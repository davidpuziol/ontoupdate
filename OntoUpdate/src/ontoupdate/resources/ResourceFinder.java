
package ontoupdate.resources;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.util.HashMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import ontoupdate.util.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * Um ResourceFinder deverá ser criado pelo ResourceManager para obtenção dos
 * paths de recursos utilizados pelo sistema (Listados em ResourcesList.xml)
 * @David Puziol Prata
 */
public class ResourceFinder {

    /** HashMap que armazenará os pares <Alias,Arquivo> */
    private HashMap<String, String> mapRecursos;

    /**
     * Constrói um ResourceFinder, inicializando um HashMap e preenchendo-o com
     * as informações de recursos presentes em ResourcesList.xml
     */
    protected ResourceFinder() {
        mapRecursos = new HashMap<String, String>();
        ObterRecursosDescritos();
    }

    /**
     * Método para tradução de Aliases para Recursos previamente mapeados. A listagem dos mapeamentos deve estar preenchida corretamente no documento XML wikiconcepts.resources.ResourcesList.xml
     * <p><u>Exemplo:</u><br>
     * alias - "ALIAS_IMAGEM_FOO" <br>
     * retorno - "C:\WikiConcepts\src\wikiconcepts\resources\Imagem_Foo.jpg"
     * </p>
     * @param alias String contendo o alias de mapeamento de um recurso 
     * @return path para o arquivo referente ao Alias ou null caso não haja tal mapeamento
     */
    protected String GetResourcePath(String alias) {
        String arquivo = mapRecursos.get(alias);
        if (arquivo != null) {
            return GetInternalResourcePath(arquivo);
        } else {
            return null;
        }
    }
    
    protected Image GetImageResource(String alias){
        String arquivo = mapRecursos.get(alias);
        if(arquivo != null){
            return GetInternalImage(arquivo);
        } else {
            return null;
        }
    }
    
    protected BufferedReader GetBufferedReader(String alias)
    {
        String arquivo = mapRecursos.get(alias);
        if(arquivo != null){
            return GetInternalBufferedReader(arquivo);
        } else {
            return null;
        }
    }

    /**
     * Método privado para obter o caminho completo no sistema para um recurso
     * existente dentro do pacote wikiconcepts.resources
     * <p><u>Exemplo:</u><br>
     * arquivo - "Imagem_Foo.jpg" <br>
     * retorno - "C:\WikiConcepts\src\wikiconcepts\resources\Imagem_Foo.jpg"
     * </p>
     * @param arquivo nome do arquivo local ao pacote wikiconcepts.resources
     * @return path completo para o arquivo
     */
    private String GetInternalResourcePath(String arquivo) {
        String pathInterno = this.getClass().getResource(arquivo).getPath();
        
        File file = new File(pathInterno);
        
        if(file.exists())
        {
            
        }
        return pathInterno;
    }
    
    private  InputStream GetInternalInputStream(String arquivo) {
        InputStream is;
        String pathInterno = this.getClass().getResource(arquivo).getPath();
        
        File file = new File(pathInterno);
        
        if(file.exists())
        {
            try{
                is = new FileInputStream(file);
            }
            catch(FileNotFoundException e)
            {
                return null;
            }
        }
        else
        {
            is = this.getClass().getResourceAsStream(arquivo);
        }
        return is;
    }
    
    private Image GetInternalImage(String arquivo){
        
        byte[] imageData = new byte[10000];

        int byteCount;
        
        InputStream is = GetInternalInputStream(arquivo);

        if(is != null)
        {
            try{
            byteCount = is.read(imageData);
            if(byteCount > 0)
            {
                return Toolkit.getDefaultToolkit().createImage(imageData);
            }
            }
            catch(IOException e)
            {
                return null;
            }
        }
        return null;
    }
    
    private BufferedReader GetInternalBufferedReader(String arquivo){
        
        InputStream is = GetInternalInputStream(arquivo);

        if(is != null)
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            return br;
        }
        return null;
    }

    /**
     * Método privado que lê o documento XML "ResourcesList.xml" e obtem todo o
     * mapeamento de recursos par os respectivos Aliases que serão utilizados no
     * sistema. Estas referências serão armzenado num HashMap para rápida
     * recuperação dos paths dos arquivos durante a execução do aplicativo.
     */
    private void ObterRecursosDescritos() {
        try {

            //Utilizar uma Factory para criar um Document para parsing de XML
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(GetInternalInputStream("ResourcesList.xml"));

            //normalizar a representação do texto
            doc.getDocumentElement().normalize();

            //Obter lista de recursos
            NodeList listaDeRecursos = doc.getElementsByTagName("Resource");

            //Para todo recurso, obter e inserir no HashMap o par <Alias,Arquivo>
            //lembrando que podem haver mais de 1 Aliases por Arquivo
            for (int i = 0; i < listaDeRecursos.getLength(); i++) {

                Node recursoNode = listaDeRecursos.item(i);
                if (recursoNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element elementoRecurso = (Element) recursoNode;

                    NodeList listaDeArquivos = elementoRecurso.getElementsByTagName("File");
                    Element elementoArquivo = (Element) listaDeArquivos.item(0);

                    NodeList listaTextoArquivos = elementoArquivo.getChildNodes();
                    //-------
                    NodeList listaDeAliases = elementoRecurso.getElementsByTagName("Alias");
                    //Para um mesmo arquivo podem haver mais de um Alias, por isso
                    //a necessidade deste loop
                    for (int j = 0; j < listaDeAliases.getLength(); j++) {
                        Element elementoAlias = (Element) listaDeAliases.item(j);
                        NodeList listaTextoAlias = elementoAlias.getChildNodes();
                        String alias = ((Node) listaTextoAlias.item(0)).getNodeValue().trim().toUpperCase();
                        String file = ((Node) listaTextoArquivos.item(0)).getNodeValue().trim();
                        mapRecursos.put(alias, file);
                        Logger.Instance().PrintLine(Logger.LEVEL_NORMAL,Logger.NOT_ERROR,this,"Mapeado Alias '" + alias + "' para o recurso '" + file + "'");
                    }
                }
            }
        } catch (SAXParseException err) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "Erro de Parsing no XML de recursos (linha "+ err.getLineNumber() + "\n Mensagem: "+ err.getMessage());
        } catch (SAXException e) {
            Exception x = e.getException();
            Logger.Instance().UnexpectedException(this, (x == null) ? e : x);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
