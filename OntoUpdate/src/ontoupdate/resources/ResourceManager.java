

package ontoupdate.resources;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.ImageIcon;
import ontoupdate.util.Logger;

/**
 *
 * @author David Puziol Prata
 */
public class ResourceManager {

    private static ResourceFinder finder;
    
    public static void Inicializar()
    {
        finder = new ResourceFinder();
    }
    
    private static String GetPathString(String aliasArquivo)
    {
        return finder.GetResourcePath(aliasArquivo);
    }
    
    public static ImageIcon GetImageIcon(String aliasImagem)
    {
        Image img = finder.GetImageResource(aliasImagem);
        if(img != null)
        {
            return new ImageIcon(img);
        }
        else
        {
            return null;
        }
    }

    public static URL GetURL(String aliasArquivo)
    {
        String path = finder.GetResourcePath(aliasArquivo);
        try{
        return new File(path).toURI().toURL();
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public static RandomAccessFile GetTempRandomAccessFile(String aliasArquivo)
    {        
        char[] buffer = new char[100];
        int bytes_read = 0;
        int offset = 0;
        
        BufferedReader br = GetBufferedReader(aliasArquivo);
        try{
            File fileToUse = GetNewMonitoredTempFile();
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileToUse));
            
            do{
            bytes_read = br.read(buffer,offset, 100);
            if(bytes_read != -1)
            {
                bw.write(buffer, offset, bytes_read);
            }
            } while (bytes_read != -1);
            
            br.close();
            bw.close();
            return new RandomAccessFile(fileToUse, "r");
        }catch(IOException ex)
        {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    public static File GetNewMonitoredTempFile()
    {
        File fileToUse = null;
        try{
            fileToUse = File.createTempFile("WikiConcepts_", ".tmp");
            fileToUse.deleteOnExit();
        }catch(IOException ex)
        {
            Logger.Instance().UnexpectedException(ResourceManager.class, ex);
        }
        return fileToUse;
    }    
    
    public static BufferedReader GetBufferedReader(String aliasArquivo)
    {
        return finder.GetBufferedReader(aliasArquivo);
        /*String path = finder.GetResourcePath(aliasArquivo);
        try{
        return new BufferedReader(new FileReader(path));
        
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }*/
    }
    
    public static BufferedWriter GetBufferedWriter(String aliasArquivo)
    {
        String path = finder.GetResourcePath(aliasArquivo);
        try{
        return new BufferedWriter(new FileWriter(path));
        
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
   
}
