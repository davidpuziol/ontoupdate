/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.specialComponents;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author David Puziol Prata
 *
 */
public class CustomIconsTreeCellEntry extends DefaultMutableTreeNode{
    String icon;
    String text;
    
    public CustomIconsTreeCellEntry(String icon, String text)
    {
        this.icon = icon;
        this.text = text;
    }
    
    public static CustomIconsTreeCellEntry BuildNewNode(String icon, String text)
    {
        return new CustomIconsTreeCellEntry(icon, text);
    }
    
    @Override
    public String toString()
    {
        return text;
    }
}
