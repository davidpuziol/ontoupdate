/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.specialComponents;

import java.awt.Component;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.*;

/**
 *
 * @author David Puziol Prata
 */
public class CustomIconsTreeCellRenderer extends JLabel implements TreeCellRenderer{
    
    HashMap<String,ImageIcon> cellImages;

    public CustomIconsTreeCellRenderer() {
        super();
        cellImages = new HashMap<String,ImageIcon>();
    }
    
    public void AddImagesToRender(Object... args)
    {
        if(args != null)
        {
            for(int i=0; i< args.length; i++)
            {
                if(args[i] instanceof String)
                {
                    for(int j=i+1; j<args.length;j++)
                    {
                        if( args[j] instanceof ImageIcon)
                        {
                            cellImages.put((String)args[i],(ImageIcon)args[j]);
                            i=j;
                            break;
                        }
                    }
                }
            }
        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if(value instanceof CustomIconsTreeCellEntry)
        {
            this.setText(((CustomIconsTreeCellEntry)value).text);
            ImageIcon icone = cellImages.get(((CustomIconsTreeCellEntry)value).icon);
            if(icone != null)
            {
                this.setIcon(icone);
            }
            else
            {
                this.setIcon(null);
            }
        }
        else
        {
            this.setText(value.toString());
        }
        return this;
    }

}
