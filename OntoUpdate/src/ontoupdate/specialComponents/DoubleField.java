/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.specialComponents;
import java.awt.event.FocusEvent;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import javax.swing.JFormattedTextField;

/**
 *
 * @author David Puziol Prata
 */
public class DoubleField extends JFormattedTextField{

    Double maxValue;
    Double minValue;
    Double myValue;
    
    public DoubleField() throws java.text.ParseException
    {   
        super(new java.text.DecimalFormat("#0.0############",DecimalFormatSymbols.getInstance(Locale.US)));
        this.setText("0.0");
        this.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.0############",DecimalFormatSymbols.getInstance(Locale.US)))));
        this.maxValue = new Double(1);
        this.minValue = new Double(0);
        this.myValue = new Double(0);
    }
    
    public void SetValue(Double value)
    {
        this.myValue = value;
        this.setText(this.myValue.toString());
        this.processFocusEvent(new FocusEvent(this,FocusEvent.FOCUS_LOST));
    }
    
    public void SetValue(String value)
    {
        try{
        this.setText(this.getFormatter().valueToString(value));
        }catch(ParseException ex)
        {
            this.setText(this.myValue.toString());
        }
        this.processFocusEvent(new FocusEvent(this,FocusEvent.FOCUS_LOST));
    }
    
    public String GetStringValue()
    {
        return this.getText();
    }
    
    public Double GetDoubleValue()
    {
        return new Double(this.getText());
    }
    
    
    @Override
    protected void processFocusEvent(FocusEvent e)
    {
        super.processFocusEvent(e);
        if (e.getID() == FocusEvent.FOCUS_LOST)
        {   
            this.myValue = new Double(this.getText());

            if(this.myValue < this.minValue){
                this.myValue = this.minValue;
            } else if (this.myValue > this.maxValue){
                this.myValue = this.maxValue;
            }
            try{
            this.setText(this.getFormatter().valueToString(myValue));
            }catch(ParseException ex)
            {
                this.setText(this.minValue.toString());
            }
        } 
    }
}
