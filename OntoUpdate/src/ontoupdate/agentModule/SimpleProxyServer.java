package ontoupdate.agentModule;
/**
 *
 * @author David Puziol Prata
 */
import java.io.*;
import java.net.*;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;

public class SimpleProxyServer extends Thread {

    private ServerSocket server = null;
    private int thisPort;
    private String fwdServer;
    private int fwdPort;
    private int ptTimeout;
    private int debugLevel;
    private PrintStream debugOut;

    public SimpleProxyServer() {
        thisPort = CurrentProfile.Instance().getServerPort();
        fwdServer = "";
        fwdPort = 0;
        ptTimeout = ProxyThread.DEFAULT_TIMEOUT;
        debugLevel = 0;
        debugOut = System.out;
        this.setPriority(SimpleProxyServer.MAX_PRIORITY);
        ServerProxyForm.Instance().IncThread();
    }

    public SimpleProxyServer(int port) {
        thisPort = port;
        fwdServer = "";
        fwdPort = 0;
        ptTimeout = ProxyThread.DEFAULT_TIMEOUT;
        debugLevel = 0;
        debugOut = System.out;
    }

    public SimpleProxyServer(int port, String proxyServer, int proxyPort) {
        thisPort = port;
        fwdServer = proxyServer;
        fwdPort = proxyPort;
        ptTimeout = ProxyThread.DEFAULT_TIMEOUT;
        debugLevel = 0;
        debugOut = System.out;
    }

    public SimpleProxyServer(int port, String proxyServer, int proxyPort, int timeout) {
        thisPort = port;
        fwdServer = proxyServer;
        fwdPort = proxyPort;
        ptTimeout = timeout;
        debugLevel = 0;
        debugOut = System.out;
    }

    public void setDebug(int level, PrintStream out) {
        debugLevel = level;
        debugOut = out;
    }

    public int getPort() {
        return thisPort;
    }

    public boolean isRunning() {
        if (server == null) {
            return false;
        } else {
            return true;
        }
    }

    public void closeSocket() {
        try {
            server.close();

        } catch (Exception e) {
            if (debugLevel > 0) {
                debugOut.println(e);
            }
        }
        server = null;
    }

    @Override
    public void run() {
        try {
            // create a server socket, and loop forever listening for
            // client connections
            server = new ServerSocket(thisPort);
            //if (debugLevel > 0)
            debugOut.println("Started jProxy on port " + thisPort);
            ServerProxyForm.Instance().SetNumServerIp(InetAddress.getLocalHost().getHostAddress());
            ServerProxyForm.Instance().SetNumPort(Integer.toString(server.getLocalPort()));

            while (true) {
                Socket client = server.accept();
                ProxyThread t = new ProxyThread(client, fwdServer, fwdPort, debugLevel, debugOut);
                //t.setDebug(debugLevel, debugOut);
                //t.setTimeout(ptTimeout);
                t.start();
            }

        } catch (Exception e) {
            if (debugLevel > 0) {
                debugOut.println("jProxy Thread error: " + e);
            }
        }finally{
            closeSocket();
            ServerProxyForm.Instance().DecThread();
            System.gc();
             
        }
    }
}
