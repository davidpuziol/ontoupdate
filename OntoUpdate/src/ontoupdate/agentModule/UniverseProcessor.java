/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

import java.util.Iterator;
import java.util.LinkedList;
import ontoupdate.agentModule.graficInterface.ConfigurationManager;
import ontoupdate.util.Logger;
import ontoupdate.util.MathExpressionProcessor;
import ontoupdate.util.notifications.QuickMessageDisplay;


/**
 *
 * @author David Puziol Prata
 */
public class UniverseProcessor {
    private static UniverseProcessor instance;

    //private static UniverseProcessor instance;
    
    /* Quantidade de vezes em que os termos aparecem no conjunto
     * total de documentos que estão sendo analisados (contando
     * os textos de discussão e de artigo
     */
    LinkedList<LinkedList<Integer>> UniversalHitLists;
    /* Quantidades de documentos (Sendo que artigo e discussão são
     * considerados documentos diferentes) aonde ocorrem cada um
     * dos termos existentes
     */
    LinkedList<LinkedList<Integer>> DocumentOccurrenceLists;
    
    /* Frequência Normalizada de termos para o Universo */
    MathExpressionProcessor fij_formula_processor;
    LinkedList<LinkedList<Double>> UniverseNormalizedFrequencyLists;
    
    /* Inverso da Frequência de Documentos calculada (idfi) */
    MathExpressionProcessor idfi_formula_processor;
    LinkedList<LinkedList<Double>> IdfiValues;
    
    /* Peso dos Termos para o Universo de Documentos (wiq) */
    MathExpressionProcessor wiq_formula_processor;
    LinkedList<LinkedList<Double>> WiqValues;
    
    /* Resultado da Análise Universal do WordCounter
     * (palavra mais frequente e total de palavras existentes)
     */
    WordCounter.WordCountResult WordCountResult_inUniverse;
    
    /* Lista que armazena referências para todos os objetos DocumentProcessor
     * que estão participando da atual recuperação de informação */
    LinkedList<DocumentProcessor> documentProcessorsList;
    
    static public UniverseProcessor Instance()
     {
        if (instance == null) {
            instance = new UniverseProcessor();
        }

        return instance;
    }

    static public void GenerateNewInstance()
    {
        instance = new UniverseProcessor();
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, UniverseProcessor.class, "Uma nova instancia do Processador de Informacoes Universais foi gerada");
    }
    
    public UniverseProcessor()
    {
        /* Reinicia o contador de Palavras Universal
         * (que faz a contagem das palavras mais frequentes
         * e também conta o total de palavras acumulado
         * para o Universo de documentos
         */
        WordCounter.Instance().Reset();
        
        // Inicializar os contadores universais 
        
        // Inicializar Listas de informação de hits e ocorrências em documentos
        UniversalHitLists = new LinkedList<LinkedList<Integer>>();
        DocumentOccurrenceLists = new LinkedList<LinkedList<Integer>>();        
        //Para todas as famílias de classe existentes na ontologia
        for(int cf=0; cf < OntologyExtractor.Instance().Get_NumberOfClassFamilies();cf++)
        {
            //Adicionar listas para cada família de classe
            UniversalHitLists.add(new LinkedList<Integer>());
            DocumentOccurrenceLists.add(new LinkedList<Integer>());
            //obter um iterator para percorrer os termos de cada familia de classe            
            Iterator<Term> it_classFamilyTerms = OntologyExtractor.Instance().Get_ClassFamily(cf).iterator();
            while(it_classFamilyTerms.hasNext())
            {
                //Preencher a contagem para todos os termos com 0 (inicializando contador universal)
                UniversalHitLists.getLast().add(new Integer(1));
                DocumentOccurrenceLists.getLast().add(new Integer(1));
                it_classFamilyTerms.next();
            }           
        }
        
        //Inicializar lista que guardará DocumentProcessors registrados no processo */
        documentProcessorsList = new LinkedList<DocumentProcessor>();
        
        //Obter os processadores de cálculos segundo o profile corrente selecionado
        idfi_formula_processor = ConfigurationManager.Instance().Get_Formula_idfi_Processor();
        fij_formula_processor = ConfigurationManager.Instance().Get_Formula_fij_Processor();
        wiq_formula_processor = ConfigurationManager.Instance().Get_Formula_wiq_Processor();
    }
    
    /**
     * Registra um DocumentProcessor no UniverseProcessor, para que sejam finalizados
     * os cálculos por documento que dependem de informações sobre o universo de documento,
     * ao fim do processo re Recuperação de Informação
     * @param documentProcessor um DocumentProcessor que participa da recuperação de informação atual
     * @return true caso seja registrado com sucesso, e false caso o parâmetro passado seja nulo
     * ou já exista o objeto registrado atualmente
     */
    public void DocumentProcessorSubscribe(DocumentProcessor documentProcessor)
    {
        if(documentProcessor != null && !documentProcessorsList.contains(documentProcessor))
        {
            documentProcessorsList.add(documentProcessor);
            int maxPercentageProgress = 10 + 2*documentProcessorsList.size();
            QuickMessageDisplay.Instance().StartProcessProgressMessage("Processando as Informações Universais", 0,maxPercentageProgress , 0);
            ProcessesUniversalInformation();
        }
    }
    
    public synchronized void FeedUniversalInformation(boolean hasArticleDoc, LinkedList<LinkedList<Integer>> articleHitLists)
    {
        if(hasArticleDoc)
        {
            //Para todas as famílias de classe existentes na ontologia
            for(int cf=0; cf < UniversalHitLists.size();cf++)
            {
                //obter um iterator para percorrer os termos de cada familia de classe            
                Iterator<Integer> it_articleCounters = (hasArticleDoc?articleHitLists.get(cf).iterator():null);
                LinkedList<Integer> classFamilyLinkedList = UniversalHitLists.get(cf);
                LinkedList<Integer> docOccurenceLinkedList = DocumentOccurrenceLists.get(cf);
                for(int t =0; t< classFamilyLinkedList.size(); t++)
                {
                    Integer articleCount = (hasArticleDoc?it_articleCounters.next():0);

                    if(articleCount> 0)
                    {
                        Integer universalCount = classFamilyLinkedList.get(t);
                        classFamilyLinkedList.set(t, universalCount+articleCount);
                    
                        //Atualizar os contadores de Ocorrência (Por documento)
                        if( articleCount > 0)
                        {
                            docOccurenceLinkedList.set(t, docOccurenceLinkedList.get(t)+2); // Considerando Artigo como um documento diferente da Discussão
                        }
                        else
                        {
                            docOccurenceLinkedList.set(t, docOccurenceLinkedList.get(t)+1);
                        }
                    }
                }

            }
            
            //Atualizar contagem de documentos
            Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Frequencia de termos no universo de documentos e contagem universal de termos atualizadas");
        }
    }
    
    /**
     * Verifica se existe algum DocumentProcessor Cadastrado para o presente
     * UniverseProcessor.
     * @return verdadeiro caso exista pelo menos um DocumentProcessor registrado
     * no universeProcessor, falso se o contrário
     */
    public boolean HasAtLeastOneDocumentProcessor()
    {
        if(documentProcessorsList != null && documentProcessorsList.size() > 0)
        {
            return true;
        }
        return false;
    }
    
    public void ProcessesUniversalInformation()
    {

        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        //Normalizar a frequência dos termos para o universo de documentos (fiq)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Normalizando a Frequencia dos termos para o universo de documentos (fiq)");
        NormalizeUniversalWordFrequency();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        //Calcular o inverso da frequência de documento para todos os termos (idfi)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Calculando o inverso da frequencia de documento para todos os termos (idfi)");
        CalculateInverseDocumentFrequency();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        //Calcular os pesos dos Termos para o Universo (wiq)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Calculando os pesos dos Termos para o universo de documentos (wiq) e montando o vetor de pesquisa que os contem ");
        CalculateUniversalWeights();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        /* Fazer com que os DocumentProcessors existentes no processo de recuperação
         * de informação façam os cálculos finais individuais, que dependiam de informações
         * obtidas de análises universais (como o idfi).
         */
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Disparando os calculos remanescentes dos Processadores de Documento (que dependiam de informações do universo");
        TriggerDocumentProcessors();
        
        //new RetrievalResultBuilder(documentProcessorsList);
        
    }
    
    private void NormalizeUniversalWordFrequency()
    {
            /* Obter as estatísticas universais do WordCounter (palavra mais frequente e total de
             * palavras existente no universo de documentos)
             */        
            WordCountResult_inUniverse = WordCounter.Instance().GetUniversalMostFrequentWord();
            
            // Criar uma nova lista de listas de fij 
            UniverseNormalizedFrequencyLists = new LinkedList<LinkedList<Double>>();
            // Para cada família de classe
            Iterator<LinkedList<Integer>> it_fc = UniversalHitLists.iterator();
            while(it_fc.hasNext())
            {
                // Adicionar uma lista para cada família de classes
                UniverseNormalizedFrequencyLists.add(new LinkedList<Double>());
                // Para cada termo da família de classes
                Iterator<Integer> it_t = it_fc.next().iterator();
                while(it_t.hasNext())
                {
                    Integer count = it_t.next();
                    Double fij_value;
                    /* Calcular a frequência normalizada para o termo corrente,
                     * lembrando que a fórmula será utilizada somente se existir
                     * ao menos um hit para o termo */
                    if(count > 0)
                    {
                        // Informar o número de hits (frequencia no documento) para o termo corrente
                        fij_formula_processor.SetVariable(ConstDeclarations.freqij,count );
                        // Informar o número total de palavras existente no universo de documentos
                        fij_formula_processor.SetVariable(ConstDeclarations.totW, WordCountResult_inUniverse.getTotalWordCount());
                        // Informar o número de ocorrências da palavra mais frequente no total de palavras existente no universo de documentos;
                        fij_formula_processor.SetVariable(ConstDeclarations.maxW, WordCountResult_inUniverse.getMostFrequentWordCount());
                        // Calcular o valor do fij para o termo corrente segundo as variáveis informadas acima
                        fij_value = fij_formula_processor.ResolveExpression();
                    }
                    else
                    {
                        //Caso não haja nenhum hit para o termo, adotar a frequência normalizada '0'
                        fij_value = new Double(0);
                    }
                    //Armazenar o valor fij recém calculado em sua respectiva posição na UniverseNormalizedFrequencyLists
                    UniverseNormalizedFrequencyLists.getLast().add(fij_value);
                }
            }
    }
    
    private void CalculateInverseDocumentFrequency()
    {   
        //Inicializar a lista que receberá o resultado dos idfis calculados
        IdfiValues = new LinkedList<LinkedList<Double>>();
        
        //Informar o número total de documentos ao processador da fórmula do idfi
        this.idfi_formula_processor.SetVariable(ConstDeclarations.N, 1);
        
        //para todas as famílias de classe
        for(int cf=0; cf< DocumentOccurrenceLists.size(); cf++)
        {
            //Incluir nova lista de idfis, para a corrente família de classes
            IdfiValues.add(new LinkedList<Double>());
            //Segunto o número de documentos em que ocorre cada termo t
            LinkedList<Integer> docOccurenceLinkedList = DocumentOccurrenceLists.get(cf);
            for(int t =0; t< docOccurenceLinkedList.size(); t++)
            {
                //Informar o número te documentos em que o termo t atual ocorre ao processador da fórmula idfi
                this.idfi_formula_processor.SetVariable(ConstDeclarations.ni, docOccurenceLinkedList.get(t));
                //Realizar o cálculo da frequencia idfi para o termo t e aramazenar na lista IdfiValues o resultado
                Double idfi_value = this.idfi_formula_processor.ResolveExpression();
                //Caso o cálculo do idfi tenha sido matematicamente inviável (divisão por 0, por exemplo), assumir 0
                if(idfi_value.isInfinite() || idfi_value.isNaN())
                {
                    idfi_value = new Double(0);
                }
                IdfiValues.getLast().add(idfi_value);
            }
        }
    }
    
    private void CalculateUniversalWeights()
    {
        //Inicializar a lista que receberá o resultado dos wics calculados
        WiqValues = new LinkedList<LinkedList<Double>>();
        
        Iterator<LinkedList<Double>> it_idfiLists = IdfiValues.iterator();
        Iterator<LinkedList<Double>> it_wicLists = OntologyExtractor.Instance().Get_OntologyTermWeights().iterator();
        Iterator<LinkedList<Double>> it_fiqLists = UniverseNormalizedFrequencyLists.iterator();
        while(it_idfiLists.hasNext())
            {
                // Adicionar uma lista de wiq para cada família de classes
                WiqValues.add(new LinkedList<Double>());
                // Para cada termo da família de classes
                Iterator<Double> it_idfiFcList = it_idfiLists.next().iterator();
                Iterator<Double> it_wicFcList  = it_wicLists.next().iterator();
                Iterator<Double> it_fiqFcList = it_fiqLists.next().iterator();
                /* Obs: Todas as listas precisam ter tamanho igual, de acordo com a 
                 * lógica através da qual são construídas (Sempre referenciando as
                 * posições originais de Família de Classes e termos obtidos da ontologia
                 */
                while(it_idfiFcList.hasNext())
                {
                    Double idfi_value = it_idfiFcList.next();
                    Double wic_value = it_wicFcList.next();
                    Double fiq_value = it_fiqFcList.next();

                    // Informar o inverso da frequência de documentos para o termo corrente
                    wiq_formula_processor.SetVariable(ConstDeclarations.idfi, idfi_value);
                    // Informar o peso na família de classes da ontologia para o termo corrente
                    wiq_formula_processor.SetVariable(ConstDeclarations.Wic, wic_value);
                    // Informar a frequência normalizada do termo corrente para o universo de documentos
                    wiq_formula_processor.SetVariable(ConstDeclarations.Fiq, fiq_value);
                    
                    // Calcular o valor wiq para o termo corrente segundo as variáveis acima
                    Double wiq_value = wiq_formula_processor.ResolveExpression();
 
                    //Armazenar o valor wiq recém calculado em sua respectiva posição na WiqValues
                    WiqValues.getLast().add(wiq_value);
                }
            }
    }
    
    private void TriggerDocumentProcessors()
    {
        //Obter um iterador para os DocumentProcessors que se registraram no atual processo
        Iterator<DocumentProcessor> it_documentProcessors = documentProcessorsList.iterator();
        //Para cada DocumentProcessor registrado
        while(it_documentProcessors.hasNext())
        {
            DocumentProcessor currentDocumentProcessor = it_documentProcessors.next();
            /* Disparar os cálculos remanescentes (aqueles que dependiam de informações universais, 
             * agora disponíveis pelo UniverseProcessor corrente). Serão calculados os pesos locais
             * e os graus de similaridade (através dos cossenos, segundo o modelo vetorial)
             */
            currentDocumentProcessor.ProcessDocumentAfterUniversePrepared();
            QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        }
    }
    
    /**
     * Retorna a Lista de Listas que contém os inversos das frequências de documentos
     * (idfi) para todas as famílias de classes e respectivos termos (ordenadas da maneira
     * padrão como foi obtida da ontologia original).
     * @return A lista de Listas contendo os valores idfi, caso já tenham sido calculados, ou
     * nulo, caso contrário.
     */
    public LinkedList<LinkedList<Double>> GetInverseDocumentFrequencyLists()
    {
        return this.IdfiValues;
    }
    
    /**
     * Retorna a Lista de Listas que contém os pesos universais dos termos (calculados
     * em relação a todos os documentos participantes da recuperação de informação). Estes
     * pesos (Wiq) correspondem aos vesos do vetor pesquisa de um modelo vetorial de recuperação
     * de informação.
     * @return A lista de Listas contendo os valores wiq, caso já tenham sido calculados, ou
     * nulo, caso contrário.
     */
    public LinkedList<LinkedList<Double>> GetUniversalQueryWeights()
    {
        return this.WiqValues;
    }

}
