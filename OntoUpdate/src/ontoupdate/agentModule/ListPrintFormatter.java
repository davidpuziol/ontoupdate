/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author David Puziol Prata
 */
public class ListPrintFormatter {

public static String printList(List list)
    {
        String answer = "";
        if(list == null)
        {
            answer = answer + "nula";
        }
        else if(list.size() == 0)
        {
            answer = answer + "vazia";
        }
        else
        {
            Iterator it_items = list.iterator();
            while(it_items.hasNext())
            {
                Object item = it_items.next();
                if(item == null)
                {
                    answer = answer + "[nulo] ";
                }
                else
                {
                    answer = answer + "["+item.toString()+"] ";
                }
            }
        }
        return answer;
    }
    
    public static String printListFollowingAnother(List list,List model)
    {
        String answer = "";
        if(list == null)
        {
            answer = answer + "nula";
        }
        else if(list.size() == 0)
        {
            answer = answer + "vazia";
        }
        else
        {
            Iterator it_items = list.iterator();
            Iterator it_model = model.iterator();
            while(it_items.hasNext() && it_model.hasNext())
            {
                String whatToPrint;
                Object item = it_items.next();
                String modelWord = it_model.next().toString();
                int modelSize = modelWord.length();
                if(item == null)
                {
                    whatToPrint = "nulo";
                }
                else
                {
                    whatToPrint = item.toString();
                }
                if(whatToPrint.length() > modelSize)
                {
                    whatToPrint = whatToPrint.substring(0, modelSize);
                }
                int whatToPrintSize = whatToPrint.length();
                while(whatToPrintSize < modelSize)
                {
                    if(modelSize - whatToPrintSize > 2)
                    { 
                        whatToPrint = " "+whatToPrint+" ";
                        whatToPrintSize += 2;
                    }
                    else
                    {
                        whatToPrint = whatToPrint+" ";
                        whatToPrintSize += 1;
                    }
                }
                answer = answer+"["+whatToPrint+"] ";
            }
        }
        return answer;
    }
    
    public static String printListOfLists(List<List> listOfLists)
    {
        int list_index;
        String answer = "";
        
        if(listOfLists == null)
        {
            answer = "(nula)";
        }
        else if(listOfLists.size() == 0)
        {
            answer = "(vazia)";
        }
        else
        {
            Iterator<List> it = listOfLists.iterator();
            list_index=0;
            while(it.hasNext())
            {
                List currentList = it.next();
                list_index++;
                answer = answer+"("+list_index+") -> ";
                
                answer = answer + printList(currentList);
                
                if(it.hasNext())
                {
                    answer = answer +"\n";
                }
            }
        }
        return answer;
    }
}
