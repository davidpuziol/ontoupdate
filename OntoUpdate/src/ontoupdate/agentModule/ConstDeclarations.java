/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

/**
 *
 * @author David Puziol Prata
 */
public class ConstDeclarations {
    
    public final static String Fij      = "Fij";
    public final static String freqij   = "freqij";
    public final static String maxW     = "maxW";
    public final static String totW     = "totW";
    public final static String idfi     = "idfi";
    public final static String Wic      = "Wic";
    public final static String Fiq      = "Fiq";
    public final static String N        = "N";
    public final static String ni       = "ni";
    public final static String rankFc   = "rankFc";
    public final static String rankProp = "rankProp";

    public final static String fij_Default_Formula = "freqij / maxW";
    public final static String wij_Default_Formula = "Fij * idfi * Wic";
    public final static String wiq_Default_Formula = "(Wic + (1-Wic) * Fiq) * idfi * Wic";
    public final static String idfi_Default_Formula = "log( (N+2) / (ni+1) )";
    public final static String rank_Default_Formula = "(0.5*rankFc)+(0.5*rankProp)";
    
    public final static int maxNotificationEntryProgress = 100;

    public final static int serverPortDefault = 5000;
    
    public final static String stop_words_Default_List = "de, o, que, e, do, da, em, um, para," +
            " com, nao, uma, os, no, se, na, por, mais, as, dos, como, mas, foi, ao, ele, das, tem," +
            " seu, sua, ou, ser, quando, muito, muitos, muita, muitas,  nos, há, a, ja, esta, eu, também, so, pelo, pela," +
            " ate, isso, ela, entre, era, depois, sem, mesmo, aos, ter, seus, quem, nas, me, esse," +
            " eles, estao, voce, tinha, foram, ter, essa, num, nem, suas, meu, as, minha, tem, numa," +
            " pelos, elas, havia, seja, qual,quais, sera, nos, tenho, lhe, deles, essas, esses, pelas, este," +
            " fosse, dele, tu, te, voces, vos, vos, lhes, meus, minhas, teu, tua, teus, tuas, nosso," +
            " nossa, nossos, nossas,dela, delas, esta, estes, estas, aquele, aquela, aqueles, aquelas," +
            " isto, aquilo,estou, esta, estamos, estive, estão, esteve, estivemos, estiveram, estava," +
            " estavamos, estavam, estivera, estiveramos, esteja, estejamos, estejam, estivesse," +
            " estivessemos, estivessem, estiver, estivermos, estiverem, hei, havemos, hao, houve," +
            " houvemos, houveram, houvera, houvéramos, haja, houver, sou, somos, sao, era, eramos," +
            " eram, fui, foi, fomos, foram, fora, foramos, seja, sejamos, sejam, fosse, fossemos," +
            " fossem, for, formos, forem, serei, será, seremos, seria, seriamos, seriam, tenho, tem," +
            " temos, tem, tinha, tinhamos, tinham, tive, teve, tivemos, tivera, tiveram, tiveramos," +
            " tenha, tenhamos, tenham, tivesse, tivessemos, tivessem, tiver, tivermos, tiverem, terei," +
            " terao, teria, teriamos, teriam, alguns, algumas, alguma, algum, neste, nesta, nestes, nestas,"+
            " agora, ontem, hoje, amanhã, antes, embora, sido, tambem, coisa, contém, assim, outro, outros, outras, outra,"+
            " enquanto, bem, portanto, entanto, entretanto, contudo, certamente, deveras, efetivamente, decerto, sim,"+
            " incontestavelmente, realmente, seguramente, porque, conseguintemente, consequentemente, eis,"+
            " acaso, certo, decerto, porventura, possivelmente, provavelmente, quiça, talvez, apenas, exclusivaemnte, salvo, senão,"+
            " simplesmente, só, somente, unicamente, ainda, até, inclusivamente, nomeadamente, também, assaz, bastante, completamente,"+
            " demais, demasiado, demasiadamente, excessivamente, extremamente, grandemente, intensamente, levemente, ligeiramente,"+
            " ligeiramente, mal, meio, menos, mui, nada, pouco, profundamente, quanto, quão, quase, tanto, tao, todo, todas, todos, tudo,"+
            " onde, porque, abaixo, acima, acola, adiante, afora, aa, alem, algures, alhures, ali, aonde, aquém, aqui, atrás, atraves,"+
            " avante, ca, debaixo, defronte, detras, dentro, diante, donte, la, longe, nenhures, nenhum, nenhuma, perto, jamais, nao,"+
            " nunca, tampouco, primeiramente, ultimamente, algo, afinal, amiude, anteontem, antigamente, breve, brevemente, cedo,"+
            " comumente, concomitantemnte, dantes, diariamente, doravante, enfim, então, entrementes, finalmente, imediatamente, ja, logo,"+
            " ora, outrora, presentemente, primeiro, raramente, sempre simultaneamente, tarde, felizmente, melhor, pior, principalmente,"+
            " propositalmente, adrede, alerta, alias, calmamente, corajosamente, debalde, depressa, devagar, dificilmente, sobre, sobremodo,"+
            " selvaticamente, sobremaneira, podem, pode, podemos, etc, fazer, feito, feitos, feita, feitas, faz, durante, normalmente"+
            " temporariamente, tempo, maior, menor, maioria, maiores, minoria, menores, tipico, tipicos, tipica, tipicas, tipicamente, "+
            " qualquer a about above after again against all am an and any are aren't as at be because been before being below between" + 
            " both but by can't cannot could couldn't did didn't do does doesn't doing don't down during each few for from further had hadn't" +
            " has hasn't have haven't having he he'd he'll he's her here here's hers herself him himself his how how's i i'd i'll i'm i've if in" +
            " into is isn't it it's its itself let's me more most mustn't my myself no nor not of off on once only or other ought our ours"+
            " ourselves out over own same shan't she she'd she'll she's should shouldn't so some such than that that's the their theirs them"+
            " themselves then there there's these they they'd they'll they're they've this those through to too under until up very was wasn't"+ 
            " we we'd we'll we're we've were weren't what what's when when's where where's which while who who's whom why why's with won't would"+
            " wouldn't you you'd you'll you're you've your yours yourself yourselves" ;
    
   
    public static final String[] css_class_hitfc = {"hitfc1",
                                                    "hitfc2",
                                                    "hitfc3",
                                                    "hitfc4",
                                                    "hitfc5",
                                                    "hitfc6",
                                                    "hitfc7",
                                                    "hitfc8",
                                                    "hitfc9",
                                                    "hitfc10"};
    public static final String css_class_propCoef = "propCoef";
    public static final String css_class_totalCoef = "totalCoef";
    public static final Double min_Default_coeficient = 0.40;
    public static final Double min_Default_Classify = 0.70;
    public static final Double min_Deafault_Clustering = 0.20;
    public static final Double min_Default_Concept = 0.25;
    public static final Double min_Deafault_Description = 0.80;
    public static final boolean ignoreAcentuacion = true;
    public static final boolean removeStopWords = true;
    public static final boolean reduceToRadical = false;
    public static final boolean onlyExactWords = false;
    //public static final boolean checkNodes = false;
    //public static final boolean checkFinalNodes = false;
    public static final boolean checkFormula = true;
    public static final int numTextosAviso = 10;
}
