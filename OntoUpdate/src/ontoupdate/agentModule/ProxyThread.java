/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.net.Socket;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;

/**
 *
 * @author David Puziol Prata
 */
class ProxyThread extends Thread {

    private Socket pSocket;
    private String fwdServer = "";
    private int fwdPort = 0;
    private int debugLevel = 0;
    private PrintStream debugOut = System.out;
    // the socketTimeout is used to time out the connection to
    // the remote server after a certain period of inactivity;
    // the value is in milliseconds -- use zero if you don't want
    // a timeout
    public static final int DEFAULT_TIMEOUT = 20 * 1000;
    private int socketTimeout = DEFAULT_TIMEOUT;

    public ProxyThread(Socket s) {
        pSocket = s;
        this.setPriority(9);
        ServerProxyForm.Instance().IncThread();
    }

    public ProxyThread(Socket s, String proxy, int port, int debugLevel, PrintStream debugOut) {
        pSocket = s;
        fwdServer = proxy;
        fwdPort = port;
        this.debugLevel = debugLevel;
        this.debugOut = debugOut;
        this.setPriority(9);
        ServerProxyForm.Instance().IncThread();
        //this.start();
    }

    public void setTimeout(int timeout) {
        // assume that the user will pass the timeout value
        // in seconds (because that's just more intuitive)
        socketTimeout = timeout * 1000;
    }

    public void setDebug(int level, PrintStream out) {
        debugLevel = level;
        debugOut = out;
    }

    @Override
    public void run() {
        try {
            long startTime = System.currentTimeMillis();

            BufferedInputStream clientIn = new BufferedInputStream(pSocket.getInputStream());
            BufferedOutputStream clientOut = new BufferedOutputStream(pSocket.getOutputStream());
            // the socket to the remote server
            Socket server = null;

            // other variables
            byte[] request = null;
            byte[] response = null;
            int requestLength = 0;
            int responseLength = 0;
            int pos = -1;
            StringBuffer host = new StringBuffer("");
            String hostName = "";
            int hostPort = 80;

            request = getHTTPData(clientIn, host, false);
            requestLength = Array.getLength(request);
   
            

           // call method to process request and get url if is pdf, doc, rft, txt and then die
            hostName = host.toString();
            pos = hostName.indexOf(":");
            if (pos > 0) {
                try {
                    hostPort = Integer.parseInt(hostName.substring(pos + 1));
                } catch (Exception e) {
                }
                hostName = hostName.substring(0, pos);
            }
            try {
                if ((fwdServer.length() > 0) && (fwdPort > 0)) {
                    server = new Socket(fwdServer, fwdPort);
                } else {
                    server = new Socket(hostName, hostPort);
                }
            } catch (Exception e) {
                // tell the client there was an error
                String errMsg = "HTTP/1.0 500\nContent Type: text/plain\n\n"
                        + "Error connecting to the server:\n" + e + "\n";
                clientOut.write(errMsg.getBytes(), 0, errMsg.length());
            }

            RequestProcess requestProcess = new RequestProcess(request, hostName, hostPort);

            if (server != null) {
                server.setSoTimeout(socketTimeout);
                BufferedInputStream serverIn = new BufferedInputStream(server.getInputStream());
                BufferedOutputStream serverOut = new BufferedOutputStream(server.getOutputStream());

                serverOut.write(request, 0, requestLength);

                serverOut.flush();

                if (debugLevel > 1) {
                    response = getHTTPData(serverIn, true);

                    responseLength = Array.getLength(response);
                } else {
                    responseLength = streamHTTPData(serverIn, clientOut, true);
                }
                serverIn.close();
                serverOut.close();
            }

            // send the response back to the client, if we haven't already
            if (debugLevel > 1) {
                clientOut.write(response, 0, responseLength);
            }

            long endTime = System.currentTimeMillis();
            debugOut.println("Request from " + pSocket.getInetAddress().getHostAddress()
                    + " on Port " + pSocket.getLocalPort()
                    + " to host " + hostName + ":" + hostPort
                    + "\n  (" + requestLength + " bytes sent, "
                    + responseLength + " bytes returned, "
                    + Long.toString(endTime - startTime) + " ms elapsed)");

            debugOut.flush();
            
            if (debugLevel > 1) {
                debugOut.println("REQUEST:\n" + (new String(request)));
                debugOut.println("RESPONSE:\n" + (new String(response)));
                debugOut.flush();
            }
            
            clientOut.close();
            clientIn.close();
            pSocket.close();
        } catch (Exception e) {
            if (debugLevel > 0) {
                debugOut.println("Error in ProxyThread: " + e);
            }
        }
        finally{
            ServerProxyForm.Instance().DecThread();
            System.gc();
        }
    }

    private byte[] getHTTPData(InputStream in, boolean waitForDisconnect) {

        StringBuffer foo = new StringBuffer("");
        return getHTTPData(in, foo, waitForDisconnect);
    }

    private byte[] getHTTPData(InputStream in, StringBuffer host, boolean waitForDisconnect) {

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        streamHTTPData(in, bs, host, waitForDisconnect);
        return bs.toByteArray();
    }

    private int streamHTTPData(InputStream in, OutputStream out, boolean waitForDisconnect) {
        StringBuffer foo = new StringBuffer("");
        return streamHTTPData(in, out, foo, waitForDisconnect);
    }

    private int streamHTTPData(InputStream in, OutputStream out,
            StringBuffer host, boolean waitForDisconnect) {

        StringBuffer header = new StringBuffer("");
        String data = "";
        int responseCode = 200;
        int contentLength = 0;
        int pos = -1;
        int byteCount = 0;

        try {

            data = readLine(in);
            if (data != null) {
                header.append(data + "\r\n");
                pos = data.indexOf(" ");
                if ((data.toLowerCase().startsWith("http"))
                        && (pos >= 0) && (data.indexOf(" ", pos + 1) >= 0)) {
                    String rcString = data.substring(pos + 1, data.indexOf(" ", pos + 1));
                    try {
                        responseCode = Integer.parseInt(rcString);
                    } catch (Exception e) {
                        if (debugLevel > 0) {
                            debugOut.println("Error parsing response code " + rcString);
                        }
                    }
                }
            }

            // pega o resto do  header info
            while ((data = readLine(in)) != null) {
                // the header ends at the first blank line
                if (data.length() == 0) {
                    break;
                }
                header.append(data + "\r\n");

                // ckecagem para o host header
                pos = data.toLowerCase().indexOf("host:");
                if (pos >= 0) {
                    host.setLength(0);
                    host.append(data.substring(pos + 5).trim());
                }

                // checagem para o Content-Length header
                pos = data.toLowerCase().indexOf("content-length:");
                if (pos >= 0) {
                    contentLength = Integer.parseInt(data.substring(pos + 15).trim());
                }
            }

            // adiciona uma blank line para terminar o header info
            header.append("\r\n");

            //convert the header uma byte array, e escreve em sua stream
            out.write(header.toString().getBytes(), 0, header.length());


            if ((responseCode != 200) && (contentLength == 0)) {
                out.flush();
                return header.length();
            }

            if (contentLength > 0) {
                waitForDisconnect = false;
            }

            if ((contentLength > 0) || (waitForDisconnect)) {
                try {
                    byte[] buf = new byte[4096];
                    int bytesIn = 0;
                    while (((byteCount < contentLength) || (waitForDisconnect))
                            && ((bytesIn = in.read(buf)) >= 0)) {
                        out.write(buf, 0, bytesIn);
                        byteCount += bytesIn;
                    }
                } catch (Exception e) {
                    String errMsg = "Error getting HTTP body: " + e;
                    if (debugLevel > 0) {
                        debugOut.println(errMsg);
                    }
                    //bs.write(errMsg.getBytes(), 0, errMsg.length());
                }
            }
        } catch (Exception e) {
            if (debugLevel > 0) {
                debugOut.println("Error getting HTTP data: " + e);
            }
        }

        //flush para o outputstream e retorna
        try {
            out.flush();
        } catch (Exception e) {
        }
        return (header.length() + byteCount);
    }

    private String readLine(InputStream in) {
        // le a linha de texto do inputstream
        StringBuffer data = new StringBuffer("");
        int c;

        try {
            // se não tem nada pra ler retorna null
            in.mark(1);
            if (in.read() == -1) {
                return null;
            } else {
                in.reset();
            }

            while ((c = in.read()) >= 0) {
                //checatem para o fim
                if ((c == 0) || (c == 10) || (c == 13)) {
                    break;
                } else {
                    data.append((char) c);
                }
            }

            // caso as linhas forem terminadas em  \r\n
            if (c == 13) {
                in.mark(1);
                if (in.read() != 10) {
                    in.reset();
                }
            }
        } catch (Exception e) {
            if (debugLevel > 0) {
//                debugOut.println("Error getting header: " + e);
            }
        }

        // retorna o que tem
        return data.toString();
    }
}