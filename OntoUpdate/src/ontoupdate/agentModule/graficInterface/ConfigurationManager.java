/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule.graficInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.ServerSocket;
import java.net.URL;
import java.util.LinkedList;
import java.util.Properties;
import java.util.logging.Level;
import ontoupdate.agentModule.ConstDeclarations;
import ontoupdate.util.Logger;
import ontoupdate.util.MathExpressionException;
import ontoupdate.util.MathExpressionProcessor;
import ontoupdate.util.Semaforo;

/**
 *
 * @author David Puziol Prata
 */
public class ConfigurationManager {

    private String _urlTeste;    // "http://www.google.com.br/"
    private String _proxy;       // URL do proxy HTTP
    private String _port;        // porta do proxy HTTP
    private String _username;    // Usuário para autenticação de Proxy
    private String _password;    // Senha para autenticação de Proxy;
    private boolean _usingProxy; //flag que marca se utiliza ou não o proxy.
    MathExpressionProcessor formula_fij;
    MathExpressionProcessor formula_idfi;
    MathExpressionProcessor formula_wiq;
    MathExpressionProcessor formula_wij;
    MathExpressionProcessor formula_rank;
    LinkedList<String> stopWordsList;
    CurrentProfile profile = CurrentProfile.Instance();
    // public DBE_Profile selectedProfile; //Profile corrente selecionado
    private static ConfigurationManager instance;


    /**
     * Obtém a Instância única do ConfigurationManager segundo Padrão Singleton
     * @return instância única do ConfigurationManager
     */
    public static ConfigurationManager Instance() {
        if (instance == null) {
            instance = new ConfigurationManager();
        }
        return instance;
    }

    protected ConfigurationManager() {
        _usingProxy = false;
        _proxy = "";
        _port = "";
        _username = "";
        _password = "";
        _urlTeste = "http://www.google.com.br/";
         Semaforo.Instance();

        PrepareFormulas();
    }

    public void SetHttpProxy(String proxy, String port, String username, String password) {

        _proxy = (proxy == null ? "" : proxy);
        _port = (port == null ? "" : port);
        _username = (username == null ? "" : username);
        _password = (password == null ? "" : password);
    }

    public void UseHttpProxy(boolean use) {
        if (!use) {
            Authenticator.setDefault(null);
            _usingProxy = false;
        } else {
            if (!_username.isEmpty() && !_password.isEmpty()) {
                Authenticator.setDefault(new SimpleAuthenticator(_username, _password));
            } else {
                Authenticator.setDefault(null);
            }
            Properties systemProperties = System.getProperties();
            systemProperties.setProperty("http.proxyHost", _proxy);
            systemProperties.setProperty("http.proxyPort", _port);
            _usingProxy = true;
        }
    }

    public boolean TestHttpProxy() {
        String downloadedLine = new String(); //String que guardará linha a linha trazida do BufferedReader

        try {
            URL urlString = new URL(_urlTeste);

            BufferedReader urlBuffer = new BufferedReader(
                    new InputStreamReader(
                    urlString.openStream()));
            downloadedLine = urlBuffer.readLine();

            urlBuffer.close();

        } catch (Exception e) {
            return false;
        }

        if (downloadedLine.length() > 0) {
            return true;
        }
        return false;
    }

    public boolean SetAndTestHttpProxy(String proxy, String port, String username, String password, boolean use) {
        try {
            ConfigurationManager.Instance().SetHttpProxy(proxy, port, username, password);
            ConfigurationManager.Instance().UseHttpProxy(use);
            if (ConfigurationManager.Instance().TestHttpProxy()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean TestServerPort(int port) {
        ServerSocket ss;
        try {
            ss = new ServerSocket(port);
            ss.close();
            return true;
        } catch (IOException ex0) {
            java.util.logging.Logger.getLogger(ConfigurationManager.class.getName()).log(Level.SEVERE, null, ex0);
            return false;
        } catch (NumberFormatException ex1) {
            return false;
        } catch (IllegalArgumentException ex2) {
            return false;
        }
    }


    /*===============================================================
    Métodos Referentes à fórmula de Frequência Normalizada (fij)
    ================================================================*/
    public MathExpressionProcessor Get_Formula_fij_Processor() {
        return this.formula_fij;
    }

    public void Check_and_Set_Formula_fij(String formula) throws MathExpressionException {
        formula_fij.Prepare(formula);
        //se passou pela preparação da linha anterior e não houve exceção, então é válida
    }
    /*=========================================================================
    Métodos Referentes à fórmula de Inverso da Frequência de Documento (idfi)
    ==========================================================================*/

    public MathExpressionProcessor Get_Formula_idfi_Processor() {
        return this.formula_idfi;
    }

    public void Check_and_Set_Formula_idfi(String formula) throws MathExpressionException {
        formula_idfi.Prepare(formula);
        //se passou pela preparação da linha anterior e não houve exceção, então é válida
    }
    /*===============================================================
    Métodos Referentes à fórmula de Peso Local do Termo (wij)
    ================================================================*/

    public MathExpressionProcessor Get_Formula_wij_Processor() {
        return this.formula_wij;
    }

    public void Check_and_Set_Formula_wij(String formula) throws MathExpressionException {
        formula_wij.Prepare(formula);
        //se passou pela preparação da linha anterior e não houve exceção, então é válida
    }
    /*===============================================================
    Métodos Referentes à fórmula de Peso Universal do Termo (wiq)
    ================================================================*/

    public MathExpressionProcessor Get_Formula_wiq_Processor() {
        return this.formula_wiq;
    }

    public void Check_and_Set_Formula_wiq(String formula) throws MathExpressionException {
        formula_wiq.Prepare(formula);
        //se passou pela preparação da linha anterior e não houve exceção, então é válida
    }
    /*===============================================================
    Métodos Referentes à fórmula de Ponderação de Rank (rank)
    ================================================================*/

    public MathExpressionProcessor Get_Formula_rank_Processor() {
        return this.formula_rank;
    }

    public void Check_and_Set_Formula_rank(String formula) throws MathExpressionException {
        formula_rank.Prepare(formula);
        //se passou pela preparação da linha anterior e não houve exceção, então é válida
    }

    private void PrepareFormulas() {
        //Frequencia Normalizada (fij)
        formula_fij = new MathExpressionProcessor();
        formula_fij.AddPossibleVariables(ConstDeclarations.freqij,
                ConstDeclarations.totW,
                ConstDeclarations.maxW);
        try {
            this.Check_and_Set_Formula_fij(profile.getFormula_fij());
        } catch (MathExpressionException ex) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula fij do Profile não é válida e será substituída pela padrão! \n(" + ex.getMessage() + ")");
            profile.setFormula_fij(ConstDeclarations.fij_Default_Formula);
            try {
                this.Check_and_Set_Formula_fij(profile.getFormula_fij());
            } catch (MathExpressionException exDefault) {
                Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula fij padrão não é válida, impossível utilizar! \n(" + ex.getMessage() + ")");
            }
        }

        //Inverso da Frequencia de Documento (idfi)
        formula_idfi = new MathExpressionProcessor();
        formula_idfi.AddPossibleVariables(ConstDeclarations.N,
                ConstDeclarations.ni);
        try {
            this.Check_and_Set_Formula_idfi(profile.getFormula_idfi());
        } catch (MathExpressionException ex) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula idfi do Profile não é válida e será substituída pela padrão! \n(" + ex.getMessage() + ")");
            profile.setFormula_idfi(ConstDeclarations.idfi_Default_Formula);
            try {
                this.Check_and_Set_Formula_idfi(profile.getFormula_idfi());
            } catch (MathExpressionException exDefault) {
                Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula idfi padrão não é válida, impossível utilizar! \n(" + ex.getMessage() + ")");
            }
        }

        //Peso do Termo no Docuemnto (wij)
        formula_wij = new MathExpressionProcessor();
        formula_wij.AddPossibleVariables(ConstDeclarations.Fij,
                ConstDeclarations.idfi,
                ConstDeclarations.Wic);
        try {
            formula_wij.Prepare(profile.getFormula_wij());
        } catch (MathExpressionException ex) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula wij do Profile não é válida e será substituída pela padrão! \n(" + ex.getMessage() + ")");
            profile.setFormula_wij(ConstDeclarations.wij_Default_Formula);
            try {
                this.Check_and_Set_Formula_wij(profile.getFormula_wij());
            } catch (MathExpressionException exDefault) {
                Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula wij padrão não é válida, impossível utilizar! \n(" + ex.getMessage() + ")");
            }
        }

        //Peso do Termo no Repositório (wiq)
        formula_wiq = new MathExpressionProcessor();
        formula_wiq.AddPossibleVariables(ConstDeclarations.Fiq,
                ConstDeclarations.idfi,
                ConstDeclarations.Wic);
        try {
            formula_wiq.Prepare(profile.getFormula_wiq());
        } catch (MathExpressionException ex) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula wiq do Profile não é válida e será substituída pela padrão! \n(" + ex.getMessage() + ")");
            profile.setFormula_wiq(ConstDeclarations.wiq_Default_Formula);
            try {
                this.Check_and_Set_Formula_wiq(profile.getFormula_wiq());
            } catch (MathExpressionException exDefault) {
                Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula wiq padrão não é válida, impossível utilizar! \n(" + ex.getMessage() + ")");
            }
        }

        //Ponderação do Rank (rank)
        formula_rank = new MathExpressionProcessor();
        formula_rank.AddPossibleVariables(ConstDeclarations.rankFc,
                ConstDeclarations.rankProp);
        try {
            formula_rank.Prepare(profile.getFormula_rank());
        } catch (MathExpressionException ex) {
            Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula rank do Profile não é válida e será substituída pela padrão! \n(" + ex.getMessage() + ")");
            profile.setFormula_rank(ConstDeclarations.rank_Default_Formula);
            try {
                this.Check_and_Set_Formula_rank(profile.getFormula_rank());
            } catch (MathExpressionException exDefault) {
                Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "A fórmula rank padrão não é válida, impossível utilizar! \n(" + ex.getMessage() + ")");
            }
        }
    }

}
