/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule.graficInterface;

import java.util.LinkedList;
import ontoupdate.agentModule.ConstDeclarations;

/**
 *
 * @author David Puziol Prata
 */
public class CurrentProfile {

    private static CurrentProfile instance;
    private boolean proxy_use;
    private String proxy_address;
    private String proxy_port;
    private String proxy_username;
    private String proxy_password;
    private int serverPort;
    private String formula_wij;
    private String formula_wiq;
    private String formula_idfi;
    private String formula_fij;
    private String formula_rank;
    private boolean obtain_verbs_radicals;
    private boolean only_exact_words;
    private boolean ignore_accentuation;
    private boolean regex_without_stop_words;
    private String stop_words;
//    private boolean checkNodes;
//    private boolean checkFinalNodes;
    private boolean checkFormula;
    private Double min_coeficient;
    private Double min_classify;
    private Double min_clustering;
    private Double min_concept;
    private Double min_description;
    private String ontology_path;
    private boolean isConnected;
    private LinkedList urlProcessada;
    private LinkedList urlInProcess;
    private int numTextosAviso;

    public static CurrentProfile Instance() {
        if (instance == null) {
            instance = new CurrentProfile();
        }
        return instance;
    }

    public CurrentProfile() {
        //last_use = new GregorianCalendar();
        proxy_use = false;
        formula_wij = ConstDeclarations.wij_Default_Formula;
        formula_wiq = ConstDeclarations.wiq_Default_Formula;
        formula_idfi = ConstDeclarations.idfi_Default_Formula;
        formula_fij = ConstDeclarations.fij_Default_Formula;
        formula_rank = ConstDeclarations.rank_Default_Formula;
        obtain_verbs_radicals = ConstDeclarations.reduceToRadical;
        only_exact_words = ConstDeclarations.onlyExactWords;
        ignore_accentuation = ConstDeclarations.ignoreAcentuacion;
        regex_without_stop_words = ConstDeclarations.removeStopWords;
//        checkNodes = ConstDeclarations.checkNodes;
//        checkFinalNodes = ConstDeclarations.checkFinalNodes;
        checkFormula = ConstDeclarations.checkFormula;
        stop_words = ConstDeclarations.stop_words_Default_List;
        min_coeficient = ConstDeclarations.min_Default_coeficient;
        min_classify = ConstDeclarations.min_Default_Classify;
        min_clustering = ConstDeclarations.min_Deafault_Clustering;
        min_description = ConstDeclarations.min_Deafault_Description;
        numTextosAviso = ConstDeclarations.numTextosAviso;

        proxy_address = "";
        proxy_port = "";
        proxy_username = "";
        proxy_password = "";
        serverPort = 5000;
        ontology_path = "";
        isConnected = false;
        urlProcessada = new LinkedList();
        urlInProcess = new LinkedList();
    }

    /**
     * @return the proxy_use
     */
    public boolean isProxy_use() {
        return proxy_use;
    }

    /**
     * @param proxy_use the proxy_use to set
     */
    public void setProxy_use(boolean proxy_use) {
        this.proxy_use = proxy_use;
    }

    /**
     * @return the proxy_address
     */
    public String getProxy_address() {
        return proxy_address;
    }

    /**
     * @param proxy_address the proxy_address to set
     */
    public void setProxy_address(String proxy_address) {
        this.proxy_address = proxy_address;
    }

    /**
     * @return the proxy_port
     */
    public String getProxy_port() {
        return proxy_port;
    }

    /**
     * @param proxy_port the proxy_port to set
     */
    public void setProxy_port(String proxy_port) {
        this.proxy_port = proxy_port;
    }

    /**
     * @return the proxy_username
     */
    public String getProxy_username() {
        return proxy_username;
    }

    /**
     * @param proxy_username the proxy_username to set
     */
    public void setProxy_username(String proxy_username) {
        this.proxy_username = proxy_username;
    }

    /**
     * @return the proxy_password
     */
    public String getProxy_password() {
        return proxy_password;
    }

    /**
     * @param proxy_password the proxy_password to set
     */
    public void setProxy_password(String proxy_password) {
        this.proxy_password = proxy_password;
    }

    /**
     * @return the formula_wij
     */
    public String getFormula_wij() {
        return formula_wij;
    }

    /**
     * @param formula_wij the formula_wij to set
     */
    public void setFormula_wij(String formula_wij) {
        this.formula_wij = formula_wij;
    }

    /**
     * @return the formula_wiq
     */
    public String getFormula_wiq() {
        return formula_wiq;
    }

    /**
     * @param formula_wiq the formula_wiq to set
     */
    public void setFormula_wiq(String formula_wiq) {
        this.formula_wiq = formula_wiq;
    }

    /**
     * @return the formula_idfi
     */
    public String getFormula_idfi() {
        return formula_idfi;
    }

    /**
     * @param formula_idfi the formula_idfi to set
     */
    public void setFormula_idfi(String formula_idfi) {
        this.formula_idfi = formula_idfi;
    }

    /**
     * @return the formula_fij
     */
    public String getFormula_fij() {
        return formula_fij;
    }

    /**
     * @param formula_fij the formula_fij to set
     */
    public void setFormula_fij(String formula_fij) {
        this.formula_fij = formula_fij;
    }

    /**
     * @return the formula_rank
     */
    public String getFormula_rank() {
        return formula_rank;
    }

    /**
     * @param formula_rank the formula_rank to set
     */
    public void setFormula_rank(String formula_rank) {
        this.formula_rank = formula_rank;
    }

    /**
     * @return the obtain_verbs_radicals
     */
    public boolean isObtain_verbs_radicals() {
        return obtain_verbs_radicals;
    }

    /**
     * @param obtain_verbs_radicals the obtain_verbs_radicals to set
     */
    public void setObtain_verbs_radicals(boolean obtain_verbs_radicals) {
        this.obtain_verbs_radicals = obtain_verbs_radicals;
    }

    /**
     * @return the only_exact_words
     */
    public boolean isOnly_exact_words() {
        return only_exact_words;
    }

    /**
     * @param only_exact_words the only_exact_words to set
     */
    public void setOnly_exact_words(boolean only_exact_words) {
        this.only_exact_words = only_exact_words;
    }

    /**
     * @return the ignore_accentuation
     */
    public boolean isIgnore_accentuation() {
        return ignore_accentuation;
    }

    /**
     * @param ignore_accentuation the ignore_accentuation to set
     */
    public void setIgnore_accentuation(boolean ignore_accentuation) {
        this.ignore_accentuation = ignore_accentuation;
    }

    /**
     * @return the regex_without_stop_words
     */
    public boolean isRegex_without_stop_words() {
        return regex_without_stop_words;
    }

    /**
     * @param regex_without_stop_words the regex_without_stop_words to set
     */
    public void setRegex_without_stop_words(boolean regex_without_stop_words) {
        this.regex_without_stop_words = regex_without_stop_words;
    }

    /**
     * @return the stop_words
     */
    public String getStop_words() {
        return stop_words;
    }

    /**
     * @param stop_words the stop_words to set
     */
    public void setStop_words(String stop_words) {
        this.stop_words = stop_words;
    }

    /**
     * @return the min_coeficient
     */
    public Double getMin_coeficient() {
        return min_coeficient;
    }

    /**
     * @param min_coeficient the min_coeficient to set
     */
    public void setMin_coeficient(Double min_coeficient) {
        this.min_coeficient = min_coeficient;
    }

    /**
     * @return the ontology_path
     */
    public String getOntology_path() {
        return ontology_path;
    }

    /**
     * @param ontology_path the ontology_path to set
     */
    public void setOntology_path(String ontology_path) {
        this.ontology_path = ontology_path;
    }

    /**
     * @return the isConnected
     */
    public boolean isIsConnected() {
        return isConnected;
    }

    /**
     * @param isConnected the isConnected to set
     */
    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    /**
     * @return the serverPort
     */
    public int getServerPort() {
        return serverPort;
    }

    /**
     * @param serverPort the serverPort to set
     */
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    /**
     * @return the min_classify
     */
    public Double getMin_classify() {
        return min_classify;
    }

    /**
     * @param min_classify the min_classify to set
     */
    public void setMin_classify(Double min_classify) {
        this.min_classify = min_classify;
    }

    /**
     * @return the min_clustering
     */
    public Double getMin_clustering() {
        return min_clustering;
    }

    /**
     * @param min_clustering the min_clustering to set
     */
    public void setMin_clustering(Double min_clustering) {
        this.min_clustering = min_clustering;
    }

    /**
     * @return the min_concept
     */
    public Double getMin_concept() {
        return min_concept;
    }

    /**
     * @param min_concept the min_concept to set
     */
    public void setMin_concept(Double min_concept) {
        this.min_concept = min_concept;
    }

    /**
     * @return the min_description
     */
    public Double getMin_description() {
        return min_description;
    }

    /**
     * @param min_description the min_description to set
     */
    public void setMin_description(Double min_description) {
        this.min_description = min_description;
    }

    synchronized public void addURLprocessed(String url) {
        this.urlProcessada.add(url);
    }

    synchronized public void clearURLprocessed() {
        this.urlProcessada = new LinkedList();
    }

    synchronized public void clearURLInprocess() {
        this.urlInProcess = new LinkedList();
    }

    synchronized public boolean isProcessedURL(String url) {
        if (this.urlProcessada.contains(url)) {
            return true;
        } else {
            return false;
        }
    }

    synchronized public void removeURLProcessed(String ur) {
        this.urlProcessada.remove(ur);
    }

    synchronized public void addInProcess(String url) {
        if (!this.urlInProcess.contains(url)) {
            this.urlInProcess.add(url);
        }
    }

    synchronized public void removeInProcess(String url) {
        if (!this.urlInProcess.contains(url)) {
            this.urlInProcess.remove(url);
        }
    }

    synchronized public boolean isInProcessedURL(String url) {
        if (this.urlInProcess.contains(url)) {
            return true;
        } else {
            return false;
        }
    }

    public int numberURLProcessed() {
        return this.urlProcessada.size();
    }

    /**
     * @return the checkNodes
     */
//    public boolean isCheckNodes() {
//        return checkNodes;
//    }
//
//    /**
//     * @param checkNodes the checkNodes to set
//     */
//    public void setCheckNodes(boolean checkNodes) {
//        this.checkNodes = checkNodes;
//    }
//
//    /**
//     * @return the checkFinalNodes
//     */
//    public boolean isCheckFinalNodes() {
//        return checkFinalNodes;
//    }
//
//    /**
//     * @param checkFinalNodes the checkFinalNodes to set
//     */
//    public void setCheckFinalNodes(boolean checkFinalNodes) {
//        this.checkFinalNodes = checkFinalNodes;
//    }

    /**
     * @return the numTextosAviso
     */
    public int getNumTextosAviso() {
        return numTextosAviso;
    }

    /**
     * @param numTextosAviso the numTextosAviso to set
     */
    public void setNumTextosAviso(int numTextosAviso) {
        this.numTextosAviso = numTextosAviso;
    }

    /**
     * @return the checkFormula
     */
    public boolean isCheckFormula() {
        return checkFormula;
    }

    /**
     * @param checkFormula the checkFormula to set
     */
    public void setCheckFormula(boolean checkFormula) {
        this.checkFormula = checkFormula;
    }
}
