/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule.graficInterface;

import java.io.File;
import javax.swing.filechooser.*;

/**
 *
 * @author David Puziol Prata
 */
public class OntologyFileFilter extends FileFilter {

    //Accept all directories and all gif, jpg, tiff, or png files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        
        String fileName = f.getName().trim().toLowerCase();
        if(fileName.endsWith(".owl"))
        {
            return true;
        }
        return false;
    }

    //The description of this filter
    public String getDescription() {
        return "Apenas Ontologias (*.owl)";
    }
}

