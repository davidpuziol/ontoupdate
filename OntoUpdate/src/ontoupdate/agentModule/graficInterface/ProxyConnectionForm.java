

package ontoupdate.agentModule.graficInterface;

import ontoupdate.resources.ResourceManager;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public class ProxyConnectionForm extends javax.swing.JFrame {
    private static ProxyConnectionForm instance;

    /** Creates new form SettingsConnectionForm */
    public  ProxyConnectionForm() {
        initComponents();
        ontoupdate.util.DisplayUtils.CenterGuiComponent(this);
    }

        public static ProxyConnectionForm Instance() {
        if (instance == null) {
            instance = new ProxyConnectionForm();
        }
        return instance;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JLabelForm = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabelProxy = new javax.swing.JLabel();
        jLabelPort = new javax.swing.JLabel();
        jLabelUser = new javax.swing.JLabel();
        jTextFieldPorta = new javax.swing.JTextField();
        jTextFieldUser = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPasswordField1 = new javax.swing.JPasswordField();
        jTextFieldProxy = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabelTest = new javax.swing.JLabel();
        jButtonTest = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jCheckBoxUseProxy = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldServerProxyPort = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("OntoUpdate: Connection Proxy");
        setResizable(false);

        JLabelForm.setFont(new java.awt.Font("Tahoma", 0, 18));
        JLabelForm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/CONFIGURATION_AGENT_MAIOR.png"))); // NOI18N
        JLabelForm.setText("   Configurações De Proxy");
        JLabelForm.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Proxy"));

        jLabelProxy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/PROXY.png"))); // NOI18N
        jLabelProxy.setText("Proxy");

        jLabelPort.setText("Porta");

        jLabelUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/USER_ICON.png"))); // NOI18N
        jLabelUser.setText("Usuário");

        jTextFieldPorta.setEnabled(false);
        jTextFieldPorta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldPortaKeyTyped(evt);
            }
        });

        jTextFieldUser.setEnabled(false);
        jTextFieldUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldUserKeyTyped(evt);
            }
        });

        jLabel4.setText("Senha");

        jPasswordField1.setEnabled(false);
        jPasswordField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPasswordField1KeyTyped(evt);
            }
        });

        jTextFieldProxy.setEnabled(false);
        jTextFieldProxy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProxyActionPerformed(evt);
            }
        });
        jTextFieldProxy.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldProxyKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUser)
                    .addComponent(jLabelProxy))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextFieldProxy)
                    .addComponent(jTextFieldUser, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabelPort, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldPorta, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                    .addComponent(jPasswordField1, 0, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelProxy)
                    .addComponent(jLabelPort)
                    .addComponent(jTextFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldProxy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelUser)
                        .addComponent(jTextFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Web Teste"));

        jLabelTest.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabelTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/WEB_DISCONNECTED.png"))); // NOI18N
        jLabelTest.setText("Não Testado");

        jButtonTest.setText("Testar Acesso á Web");
        jButtonTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTestActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabelTest, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButtonTest)
                .addGap(38, 38, 38))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTest)
                    .addComponent(jButtonTest))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/TICK_OK.png"))); // NOI18N
        jButtonSave.setText("Save");
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });

        jButtonCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/TICK_NO.png"))); // NOI18N
        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        jCheckBoxUseProxy.setText("Usar Proxy");
        jCheckBoxUseProxy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxUseProxyActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Configuração Servidor Proxy"));

        jLabel1.setText("Porta do Servidor Proxy");

        jTextFieldServerProxyPort.setText("5000");
        jTextFieldServerProxyPort.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldServerProxyPortKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jLabel1)
                .addGap(18, 66, Short.MAX_VALUE)
                .addComponent(jTextFieldServerProxyPort, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jTextFieldServerProxyPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel1))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jButtonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(jButtonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(JLabelForm, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBoxUseProxy))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(JLabelForm, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxUseProxy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxUseProxyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxUseProxyActionPerformed
        if(this.jCheckBoxUseProxy.isSelected()){
            this.jTextFieldProxy.setEnabled(true);
            this.jTextFieldPorta.setEnabled(true);
            this.jTextFieldUser.setEnabled(true);
            this.jPasswordField1.setEnabled(true);
        }
        else{
            this.jTextFieldProxy.setEnabled(false);
            this.jTextFieldPorta.setEnabled(false);
            this.jTextFieldUser.setEnabled(false);
            this.jPasswordField1.setEnabled(false);
        }
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);
        this.repaint();
    }//GEN-LAST:event_jCheckBoxUseProxyActionPerformed

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        if (this.jLabelTest.getText().equals("Conectado")) {
            try {
                if (ConfigurationManager.Instance().TestServerPort(Integer.parseInt(this.jTextFieldServerProxyPort.getText()))) {// porta disponivel
                    CurrentProfile.Instance().setProxy_use(this.jCheckBoxUseProxy.isSelected());
                    if (this.jCheckBoxUseProxy.isSelected()) {
                        CurrentProfile.Instance().setProxy_address(this.jTextFieldProxy.getText());
                        CurrentProfile.Instance().setProxy_port(this.jTextFieldPorta.getText());
                        CurrentProfile.Instance().setProxy_username(this.jTextFieldUser.getText());
                        CurrentProfile.Instance().setProxy_password(this.jPasswordField1.getText());
                    } else {
                        CurrentProfile.Instance().setProxy_address("");
                        CurrentProfile.Instance().setProxy_port("");
                        CurrentProfile.Instance().setProxy_username("");
                        CurrentProfile.Instance().setProxy_password("");
                    }
                    CurrentProfile.Instance().setIsConnected(true);
                    CurrentProfile.Instance().setServerPort(Integer.parseInt(this.jTextFieldServerProxyPort.getText()));
                    this.dispose();

                } else {
                    QuickMessageDisplay.ShowErrorMsg("Porta do Servidor Proxy não é válida. Digite novamente.", 4000);

                }
            } catch (NumberFormatException exception) {
                QuickMessageDisplay.ShowErrorMsg("Somente Inteiro para Porta. Digite novamente.", 4000);
                this.jLabelTest.setText("Não Conectado!");
                this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
                this.repaint();
            }


        } else {
            QuickMessageDisplay.ShowWarningMsg("Conexão não testada, por favor faça o execute o teste antes de salvar", 4000);
            CurrentProfile.Instance().setIsConnected(false);
        }
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.jCheckBoxUseProxy.setSelected(CurrentProfile.Instance().isProxy_use());
        if (this.jCheckBoxUseProxy.isSelected()) {
            this.jTextFieldProxy.setText(CurrentProfile.Instance().getProxy_address());
            this.jTextFieldPorta.setText(CurrentProfile.Instance().getProxy_port());
            this.jTextFieldUser.setText(CurrentProfile.Instance().getProxy_username());
            this.jPasswordField1.setText(CurrentProfile.Instance().getProxy_password());
            
            this.jTextFieldProxy.setEnabled(true);
            this.jTextFieldPorta.setEnabled(true);
            this.jTextFieldUser.setEnabled(true);
            this.jPasswordField1.setEnabled(true);
        }
        else{
            this.jTextFieldProxy.setText("");
            this.jTextFieldPorta.setText("");
            this.jTextFieldUser.setText("");
            this.jPasswordField1.setText("");
            this.jTextFieldProxy.setEnabled(false);
            this.jTextFieldPorta.setEnabled(false);
            this.jTextFieldUser.setEnabled(false);
            this.jPasswordField1.setEnabled(false);
        }
         if (CurrentProfile.Instance().isIsConnected()) {
                this.jLabelTest.setText("Conectado");
                this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_CONNECTED"));
                CurrentProfile.Instance().setIsConnected(true);

            } else {
                this.jLabelTest.setText("Não Conectado");
                this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
                CurrentProfile.Instance().setIsConnected(false);
            }
        this.jTextFieldServerProxyPort.setText(Integer.toString(CurrentProfile.Instance().getServerPort()));
        this.dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jTextFieldProxyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProxyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProxyActionPerformed

    private void jButtonTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTestActionPerformed
        if(ConfigurationManager.Instance().SetAndTestHttpProxy(this.jTextFieldProxy.getText(),
                this.jTextFieldPorta.getText(),
                this.jTextFieldUser.getText(),
                new String(this.jPasswordField1.getPassword()),
                this.jCheckBoxUseProxy.isSelected()))
        {
            this.jLabelTest.setText("Conectado");
            this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_CONNECTED")); 
        }
        else
        {
            this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
            jLabelTest.setText("Não Conectado!");
        }
        this.repaint();
    }//GEN-LAST:event_jButtonTestActionPerformed

    private void jTextFieldUserKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldUserKeyTyped
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);
    }//GEN-LAST:event_jTextFieldUserKeyTyped

    private void jTextFieldProxyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProxyKeyTyped
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);
    }//GEN-LAST:event_jTextFieldProxyKeyTyped

    private void jTextFieldPortaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldPortaKeyTyped
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPortaKeyTyped

    private void jPasswordField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordField1KeyTyped
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jPasswordField1KeyTyped

    private void jTextFieldServerProxyPortKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldServerProxyPortKeyTyped
        jLabelTest.setText("Não Conectado!");
        this.jLabelTest.setIcon(ResourceManager.GetImageIcon("IMG_WEB_DISCONNECTED"));
        CurrentProfile.Instance().setIsConnected(false);        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldServerProxyPortKeyTyped

    /**
    * @param args the command line arguments
    */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLabelForm;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonTest;
    private javax.swing.JCheckBox jCheckBoxUseProxy;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelPort;
    private javax.swing.JLabel jLabelProxy;
    private javax.swing.JLabel jLabelTest;
    private javax.swing.JLabel jLabelUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jTextFieldPorta;
    private javax.swing.JTextField jTextFieldProxy;
    private javax.swing.JTextField jTextFieldServerProxyPort;
    private javax.swing.JTextField jTextFieldUser;
    // End of variables declaration//GEN-END:variables

}
