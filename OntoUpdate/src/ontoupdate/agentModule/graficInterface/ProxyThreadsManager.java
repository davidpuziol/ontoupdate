/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule.graficInterface;

import java.util.concurrent.ExecutorService;
import ontoupdate.agentModule.SimpleProxyServer;
/**
 *
 * @author David Puziol Prata
 */
// classe controladora
public class ProxyThreadsManager {
    private static ProxyThreadsManager instance;
    private ExecutorService executor;
    private SimpleProxyServer simpleProxyServer;
//

    public void StartProxyServer(){
        simpleProxyServer = new SimpleProxyServer();
        simpleProxyServer.start();
    }

    public static ProxyThreadsManager Instance() {
        if (instance == null) {
            instance = new ProxyThreadsManager();
        }
        return instance;
    }

    public void PauseServer(){
        simpleProxyServer.suspend();
    }
    public void ContinueServer(){
        simpleProxyServer.resume();
    }
    public void FinishServer(){
       try{
           simpleProxyServer.closeSocket();
        simpleProxyServer.interrupt();
       }catch(Exception e){

       }  
    }
}
