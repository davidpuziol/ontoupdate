/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule.graficInterface;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 *
 * @author David Puziol Prata
 */
public class SimpleAuthenticator extends Authenticator {

    private String username,  password;

    public SimpleAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(
                username, password.toCharArray());
    }
}