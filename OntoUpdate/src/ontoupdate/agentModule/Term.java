/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.tree.DefaultMutableTreeNode;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.specialComponents.CustomIconsTreeCellEntry;
import ontoupdate.util.Logger;
import ontoupdate.util.PatternManager;

/**
 *
 * @author David Puziol Prata
 */
public class Term {

    RDFResource originalOntologyResource;
    String main_keyword;
    LinkedList<String> synonymns;
    LinkedList<String> verbs;
    int class_deep_level;
    int familyClass_maxLevel;
    Double weight;
    String prop_sinonimo;
    String prop_verbo;
    Double prop_ajuste;
    Pattern termRegexPattern;

    public Term(RDFResource owlResource, int class_deep_level) {
        originalOntologyResource = owlResource;

        synonymns = new LinkedList<String>();
        verbs = new LinkedList<String>();

        this.class_deep_level = class_deep_level;
        this.weight = new Double(-1);

        OWLModel owlModel = owlResource.getOWLModel();

        this.main_keyword = WordModifier.GetCleanWordLowered(owlResource.getName());

        try {
            prop_sinonimo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("Sinônimo"));
        } catch (IllegalArgumentException ex) {
            prop_sinonimo = null;
            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'Sinônimo' não existe na ontologia selecionada!");
        }
        try {
            prop_verbo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("VerboRelacionado"));
        } catch (IllegalArgumentException ex) {
            prop_verbo = null;
            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'VerboRelacionado' não existe na ontologia selecionada!");
        }
        try {
            Float ajuste = (Float) owlResource.getPropertyValue(owlModel.getOWLProperty("AjusteRelevância"));

            if (ajuste == null) {
                prop_ajuste = null;
            } else {
                prop_ajuste = ajuste.doubleValue();
            }
        } catch (IllegalArgumentException ex) {
            prop_ajuste = null;
            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'AjusteRelevância' não existe na ontologia selecionada!");
        }

        if (prop_sinonimo != null) {
            Matcher synonymMatcher = PatternManager.Instance().GetMatcher("[^,\\s]+([\\s]+[^,\\s]+)*", prop_sinonimo);
            while (synonymMatcher.find()) {
                synonymns.add(WordModifier.GetCleanWordLowered(synonymMatcher.group()));
            }
        }

        if (prop_verbo != null) {
            Matcher verbMatcher = PatternManager.Instance().GetMatcher("[^,\\s]+", prop_verbo);
            while (verbMatcher.find()) {
                verbs.add(WordModifier.GetCleanWordLowered(verbMatcher.group()));
            }
        }

        BuildTermRegexPattern();

        System.out.println("\nClasse: " + this.main_keyword);
        System.out.print("  Sinônimos: ");
        Iterator<String> it = synonymns.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " , ");
        }
        System.out.print("\n  Verbos: ");
        it = verbs.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " , ");
        }
    }

    public Pattern BuildTermRegexPattern() {
        Iterator<String> it;
        List<String> stopWordsList = null;

        boolean useStem = CurrentProfile.Instance().isObtain_verbs_radicals();
        boolean replaceAccentuatedChars = CurrentProfile.Instance().isIgnore_accentuation();
        boolean removeStopWords = CurrentProfile.Instance().isRegex_without_stop_words();

        if (removeStopWords) {
            stopWordsList = buildListOfStopWords();
        } else {
            stopWordsList = new LinkedList<String>();
        }

        String my_regex = "";
        String my_parallelRegex = "";

        if (replaceAccentuatedChars) {
            String word = WordModifier.TransformAccentuatedCharacters(main_keyword);
            if (!stopWordsList.contains(word)) {
                my_regex = word.replaceAll("[\\s]+", "[\\\\s]+");
            }
            my_parallelRegex = word.replaceAll("[\\s]+", "[\\\\s]+");
        } else {
            String word = main_keyword;
            if (!stopWordsList.contains(word)) {
                my_regex = word.replaceAll("[\\s]+", "[\\\\s]+");
            }
            my_parallelRegex = word.replaceAll("[\\s]+", "[\\\\s]+");
        }

        it = synonymns.iterator();
        while (it.hasNext()) {
            String word = it.next();
            if (replaceAccentuatedChars) {
                word = WordModifier.TransformAccentuatedCharacters(word);
            }
            if (!stopWordsList.contains(word)) {
                if (!my_regex.isEmpty()) {
                    my_regex = my_regex + "|";
                }
                my_regex = my_regex + word.replaceAll("[\\s]+", "[\\\\s]+");
            }
            if (!my_parallelRegex.isEmpty()) {
                my_parallelRegex = my_parallelRegex + "|";
            }
            my_parallelRegex = my_parallelRegex + word.replaceAll("[\\s]+", "[\\\\s]+");
        }

        it = verbs.iterator();
        while (it.hasNext()) {
            String word = it.next();
            if (replaceAccentuatedChars) {
                word = WordModifier.TransformAccentuatedCharacters(word);
            }
            if (useStem) {
                word = WordModifier.verbStem(word);
                if (!stopWordsList.contains(word)) {
                    if (!my_regex.isEmpty()) {
                        my_regex = my_regex + "|";
                    }
                    my_regex = my_regex + word.replaceAll("[\\s]+", "[\\\\s]+") + ".*?";
                }
                if (!my_parallelRegex.isEmpty()) {
                    my_parallelRegex = my_parallelRegex + "|";
                }
                my_parallelRegex = my_parallelRegex + word.replaceAll("[\\s]+", "[\\\\s]+") + ".*?";
            } else {
                if (!stopWordsList.contains(word)) {
                    if (!my_regex.isEmpty()) {
                        my_regex = my_regex + "|";
                    }
                    my_regex = my_regex + word.replaceAll("[\\s]+", "[\\\\s]+");
                }
                if (!my_parallelRegex.isEmpty()) {
                    my_parallelRegex = my_parallelRegex + "|";
                }
                my_parallelRegex = my_parallelRegex + word.replaceAll("[\\s]+", "[\\\\s]+");
            }
        }

        my_regex = my_regex.trim();

        if (my_regex.isEmpty()) {
            my_regex = my_parallelRegex.trim();
        }

        my_regex = "(" + my_regex + ")";

        //Configuração para encontrar a palavra exata
        if (CurrentProfile.Instance().isOnly_exact_words()) {
            my_regex = "(?<=[\\p{Punct}\\s]|^)" + my_regex + "[s]?+(?=[\\p{Punct}\\s]|$)";
        }
        termRegexPattern = Pattern.compile(my_regex, Pattern.CASE_INSENSITIVE);
        return termRegexPattern;
    }

    public Pattern GetTermRegexPattern() {
        return termRegexPattern;
    }

    public void CalculateWeight(int maxClassDeep) {
        familyClass_maxLevel = maxClassDeep;
        if (prop_ajuste != null) {
            if (prop_ajuste > 1) {
                weight = (double) 1;
            } else {
                weight = prop_ajuste;
            }
        } else {
            weight = class_deep_level / (double) maxClassDeep;
        }
    }

    public boolean IsInstance() {
        if (originalOntologyResource instanceof OWLIndividual) {
            return true;
        }
        return false;
    }

    public String GetMainTerm() {
        return this.main_keyword;
    }

    public int getTermDeepLevel() {
        return this.class_deep_level;
    }

    public int getFamilyClassMaxLevel() {
        return this.familyClass_maxLevel;
    }

    public Double getTermWeight() {
        return this.weight;
    }

    public DefaultMutableTreeNode BuildTreeNode() {
        Iterator<String> it;

        DefaultMutableTreeNode termRoot;
        if (this.IsInstance()) {
            termRoot = CustomIconsTreeCellEntry.BuildNewNode("INSTANCE", "Instância: " + originalOntologyResource.getName());
        } else {
            termRoot = CustomIconsTreeCellEntry.BuildNewNode("CLASS", "Classe: " + originalOntologyResource.getName());
        }

        DefaultMutableTreeNode mainKeyword = CustomIconsTreeCellEntry.BuildNewNode("MAIN_TERM", "<html>Termo Principal: <font style=\"font-weight:100\">" + this.main_keyword);
        DefaultMutableTreeNode synonymsNode = CustomIconsTreeCellEntry.BuildNewNode("SYNONYMS", "Sinônimos (" + synonymns.size() + ")");
        DefaultMutableTreeNode verbsNode = CustomIconsTreeCellEntry.BuildNewNode("VERBS", "Verbos (" + verbs.size() + ")");
        DefaultMutableTreeNode deepLevelNode = CustomIconsTreeCellEntry.BuildNewNode("DEEP", "<html>Profundidade: <font style=\"font-weight:100\">" + class_deep_level);

        DefaultMutableTreeNode weightNode;
        if (this.prop_ajuste != null) {
            weightNode = CustomIconsTreeCellEntry.BuildNewNode("WEIGHT", "<html>Peso: <font style=\"font-weight:100\">" + this.weight + "</font> (informado)");
        } else {
            weightNode = CustomIconsTreeCellEntry.BuildNewNode("WEIGHT", "<html>Peso: <font style=\"font-weight:100\">" + this.weight + "</font> (calculado)");
        }

        DefaultMutableTreeNode regexNode = CustomIconsTreeCellEntry.BuildNewNode("REGEX", "Expressão Regular");
        regexNode.add(CustomIconsTreeCellEntry.BuildNewNode("", "<html><font style=\"font-weight:100\">" + this.BuildTermRegexPattern().pattern()));

        it = synonymns.iterator();
        while (it.hasNext()) {
            synonymsNode.add(CustomIconsTreeCellEntry.BuildNewNode("", "<html><font style=\"font-weight:100\">" + it.next().toString()));
        }

        it = verbs.iterator();
        while (it.hasNext()) {
            verbsNode.add(CustomIconsTreeCellEntry.BuildNewNode("", "<html><font style=\"font-weight:100\">" + it.next().toString()));
        }

        termRoot.add(mainKeyword);
        termRoot.add(synonymsNode);
        termRoot.add(verbsNode);
        termRoot.add(deepLevelNode);
        termRoot.add(weightNode);
        termRoot.add(regexNode);

        return termRoot;
    }

    private LinkedList<String> buildListOfStopWords() {
        LinkedList<String> stopWordsList;
        stopWordsList = new LinkedList<String>();
        if (!ConstDeclarations.stop_words_Default_List.trim().isEmpty()) {
            String[] stop_words = ConstDeclarations.stop_words_Default_List.split("[\\s]*[,;][\\s]*");

            for (int i = 0; i < stop_words.length; i++) {
                String stopWord = stop_words[i].trim();
                if (!stopWord.isEmpty()) {
                    if (!stopWordsList.contains(stopWord)) {
                        stopWordsList.add(stopWord);
                    }
                }
            }
        }
        return stopWordsList;
    }

    @Override
    public String toString() {
        return main_keyword;
    }
}
