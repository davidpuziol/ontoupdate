/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.tree.DefaultMutableTreeNode;
import ontoupdate.specialComponents.CustomIconsTreeCellEntry;

/**
 *
 * @author David Puziol Prata
 */
public class Property {

    OWLObjectProperty originalOntologyObjProp;
    LinkedList<Integer> propertyFamilyClassDomain;
    LinkedList<Integer> propertyFamilyClassRange;
    LinkedList<PropertyScope> propertyScopeList;
    String propertyName;
    String inversePropertyName;

    public Property(OWLObjectProperty originalOntologyObjProp, LinkedList<Integer> propertyFamilyClassDomain, LinkedList<Integer> propertyFamilyClassRange) {
        this.propertyFamilyClassDomain = propertyFamilyClassDomain;
        this.propertyFamilyClassRange = propertyFamilyClassRange;
        this.originalOntologyObjProp = originalOntologyObjProp;
        this.propertyName = originalOntologyObjProp.getName();
        if (originalOntologyObjProp.getInverseProperty() != null) {
            inversePropertyName = originalOntologyObjProp.getInverseProperty().getName();
        } else {
            inversePropertyName = null;
        }
        BuildPropertyScopeList();
    }

    private void BuildPropertyScopeList() {
        propertyScopeList = new LinkedList<PropertyScope>();
        for (int i = 0; i < propertyFamilyClassDomain.size(); i++) {
            for (int j = 0; j < propertyFamilyClassRange.size(); j++) {
                if (propertyFamilyClassDomain.get(i) != propertyFamilyClassRange.get(j)) {
                    PropertyScope newPropertyScope = new PropertyScope(this, propertyFamilyClassDomain.get(i), propertyFamilyClassRange.get(j));
                    if (!propertyScopeList.contains(newPropertyScope)) {
                        propertyScopeList.add(newPropertyScope);
                    }
                }
            }
        }
    }

    public DefaultMutableTreeNode BuildTreeNode() {
        DefaultMutableTreeNode propertyRoot = CustomIconsTreeCellEntry.BuildNewNode("PROPERTY", "<html>Propriedade <font color=\"#0000AA\">" + this.propertyName + "</font> (Famílias de Classes envolvidas: " + this.GetNumberOfInvolvedClasses() + ")");
        if (this.inversePropertyName != null) {
            DefaultMutableTreeNode inversePropertyNode = CustomIconsTreeCellEntry.BuildNewNode("INVERSE_PROPERTY", "<html>Propriedade Inversa (ignorada): <font color=\"#0000AA\">" + this.inversePropertyName + "</font>");
            propertyRoot.add(inversePropertyNode);
        }

        DefaultMutableTreeNode domainNode = CustomIconsTreeCellEntry.BuildNewNode("DOMAIN", "Domínio (" + this.propertyFamilyClassDomain.size() + ")");
        Iterator<Integer> it_domainFc = propertyFamilyClassDomain.iterator();
        while (it_domainFc.hasNext()) {
            domainNode.add(CustomIconsTreeCellEntry.BuildNewNode("FAMILY_CLASS", "<html>Família de Classes <font color=\"#AA0000\">" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(it_domainFc.next()) + "</font>"));
        }

        DefaultMutableTreeNode rangeNode = CustomIconsTreeCellEntry.BuildNewNode("RANGE", "Âmbito (" + this.propertyFamilyClassRange.size() + ")");
        Iterator<Integer> it_rangeFc = propertyFamilyClassRange.iterator();
        while (it_rangeFc.hasNext()) {
            rangeNode.add(CustomIconsTreeCellEntry.BuildNewNode("FAMILY_CLASS", "<html>Família de Classes <font color=\"#AA0000\">" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(it_rangeFc.next()) + "</font>"));
        }

        DefaultMutableTreeNode scopeNode = CustomIconsTreeCellEntry.BuildNewNode("TOTAL_SCOPE", "Escopo Total (" + this.propertyScopeList.size() + ")");
        Iterator<PropertyScope> it_scopeFc = propertyScopeList.iterator();
        while (it_scopeFc.hasNext()) {
            PropertyScope currentPropertyScope = it_scopeFc.next();
            scopeNode.add(CustomIconsTreeCellEntry.BuildNewNode("SCOPE", "<html>Famílias de Classes ( <font color=\"#AA0000\">" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(currentPropertyScope.GetOneClassFamily()) + "</font> , "
                    + "<font color=\"#AA0000\">" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(currentPropertyScope.GetOtherClassFamily()) + "</font> )"));
        }

        propertyRoot.add(domainNode);
        propertyRoot.add(rangeNode);
        propertyRoot.add(scopeNode);

        return propertyRoot;
    }

    public LinkedList<PropertyScope> GetPropertyScopeList() {
        return this.propertyScopeList;
    }

    public String GetPropertyName() {
        return this.propertyName;
    }

    public int GetNumberOfInvolvedClasses() {
        //Obter a soma das quantidades de famílias de classes existentes no domínio e sua extensão
        int number = propertyFamilyClassDomain.size() + propertyFamilyClassRange.size();
        //Eliminar da contagem as famílias de classe repetidas (Que pertencem tanto ao range quanto ao domain
        Iterator<Integer> it_fc = propertyFamilyClassDomain.iterator();
        while (it_fc.hasNext()) {
            if (propertyFamilyClassRange.contains(it_fc.next())) {
                number--;
            }
        }
        return number;
    }

    @Override
    public String toString() {
        return this.propertyName;
    }
}
