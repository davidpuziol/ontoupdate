/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import ontoupdate.agentModule.graficInterface.ConfigurationManager;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.util.Logger;
import ontoupdate.util.MathExpressionProcessor;
import ontoupdate.util.Semaforo;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public final class DocumentProcessor {

    UniverseProcessor up;
    WordCounter.WordCountResult WordCountResult_inArticle;
    boolean hasArticleDoc;
    LinkedList<LinkedList<Integer>> ArticleHitLists;
    LinkedList<LinkedList<Double>> ArticleNormalizedFrequencyLists;    //fij
    MathExpressionProcessor fij_formula_processor;
    /* Peso dos Termos para o Documento (wij) */
    MathExpressionProcessor wij_formula_processor;
    LinkedList<LinkedList<Double>> WijValues_forArticle;
    /* Similaridades calculadas com cada Família de Classes */
    LinkedList<Double> ClassFamiliesSimilarities_forArticle;
    /* Coeficientes calculados para os escopos de propriedade existentes */
    LinkedList<Double> PropertiesScopesCoeficients_forArticle;
    /* Valores calculados para o ranking final dos documentos */
    MathExpressionProcessor rank_formula_processor;
    public Double article_rank_value;
    public Double article_total_rank = null;
    //Contagem total de hits para todos os termos
    Integer sumAllTermsHits_forArticle;
    //Textos limpos de caracteres especias,se configurado para tal, ou originais
    String articleTextConsidered;

    public DocumentProcessor(String text) {

        // Setar as flags que indicam se existe texto considerado para Artigo e Discussão
        hasArticleDoc = !(text == null || text.isEmpty()); // se tiver artivo seta true
//        articleTextConsidered = text;
        articleTextConsidered = text;
        if (WillParticipateOfRetrieval()) {
            //Obter as fórmulas customizadas de cálculo a partir do profile
            fij_formula_processor = ConfigurationManager.Instance().Get_Formula_fij_Processor();
            wij_formula_processor = ConfigurationManager.Instance().Get_Formula_wij_Processor();
            rank_formula_processor = ConfigurationManager.Instance().Get_Formula_rank_Processor();

            // Instanciar variáveis do objeto
            // se nnnnnn
            ArticleHitLists = (hasArticleDoc ? new LinkedList<LinkedList<Integer>>() : null);
            
            LinkedList <String> palavrasOntologia = OntologyExtractor.Instance().getAllWords();
            
            Semaforo.Instance().P();
            if (CurrentProfile.Instance().isIgnore_accentuation()) {
                this.articleTextConsidered = (hasArticleDoc ? WordModifier.TransformAccentuatedCharacters(text) : null);
                    for(int i=0; i<palavrasOntologia.size(); i++){
                        palavrasOntologia.set(i, WordModifier.TransformAccentuatedCharacters(palavrasOntologia.get(i)));
                    }
            }
            Semaforo.Instance().V();
                   
//        for(int i=0; i<palavrasOntologia.size(); i++){
//            System.out.println(palavrasOntologia.get(i).toString());
//        }
            // removendo caracteres nao utilizados no texto

            articleTextConsidered = articleTextConsidered.toLowerCase();

            char chars[] = {'|', '!', '@', '#', '$', '%', '¨', '&', '*', '(', ')', '+', '`', '´', '{', '}', '[', ']', 'ª', 'º', ',', '.', '<', '>', '_',
                ';', ':', '/', '?', '°', '§', '¹', '²', '³', '=', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'ˆ', '~', '¸', '˜', '-', '”', '“', ' ', '’', '–', '\n', '\r', '\t'};


            for (int i = 0; i < chars.length; i++) {
                articleTextConsidered = articleTextConsidered.replace(chars[i], ' ');
            }
            
            //AQUI VOU COLOCAR OS _ NAS PALAVRAS PARA NAO REMOVER AS STOPWORDS DELAS
            for(int i=0; i< palavrasOntologia.size(); i++){
                String[] string = palavrasOntologia.get(i).split(" ");
                //se maior do que 1 é composta
                if(string.length > 1 && articleTextConsidered.contains(palavrasOntologia.get(i)) ){
                    //formar a palavra ligando com os _
                    String palavra = palavrasOntologia.get(i).replace(" ", "_");
                   // System.out.println("PALAVRA DA ONTOLOGIA = "+palavrasOntologia.get(i));
                    //System.out.println(palavra);
                    articleTextConsidered = articleTextConsidered.replace(palavrasOntologia.get(i), palavra);       
                }   
            }
            
            //System.out.println("TEXTO COM MUDANÇA = "+articleTextConsidered);

            if (CurrentProfile.Instance().isRegex_without_stop_words()) {

                String stopWords = CurrentProfile.Instance().getStop_words().replace(",", ""); // retirando as virgulas
                String[] array = stopWords.split(" ");

                for (int j = 0; j < array.length; j++) { // loop para retirar todas as stopwords
                    // inicio e meio de linhas
                    articleTextConsidered = articleTextConsidered.replaceAll(" " + array[j] + " ", " ");
                }
            }
            String[] packetWords;

            packetWords = articleTextConsidered.split(" ");
//        List packetWordList = Arrays.asList(packetWords)
            articleTextConsidered = ""; // zera para receber as futuras palavras

            
            for (int i = 0; i < packetWords.length; i++) {
                String word = packetWords[i]; // pegando a palavra
                word = word.trim(); // retirando os espaços
                // formar a palavra a partir de agora
                char[] charArray = word.toCharArray();// array de caracteres da palavra
                String wordFinal = ""; // inicilizada com nada  palavra que será concatenada
                for (int j = 0; j < charArray.length; j++) { // percorrendo o vetor de caracteres
                    if (      charArray[j] == '_' 
                            ||charArray[j] == 'a'
                            || charArray[j] == 'b'
                            || charArray[j] == 'c'
                            || charArray[j] == 'd'
                            || charArray[j] == 'e'
                            || charArray[j] == 'f'
                            || charArray[j] == 'g'
                            || charArray[j] == 'h'
                            || charArray[j] == 'i'
                            || charArray[j] == 'j'
                            || charArray[j] == 'k'
                            || charArray[j] == 'l'
                            || charArray[j] == 'm'
                            || charArray[j] == 'n'
                            || charArray[j] == 'o'
                            || charArray[j] == 'p'
                            || charArray[j] == 'q'
                            || charArray[j] == 'r'
                            || charArray[j] == 's'
                            || charArray[j] == 't'
                            || charArray[j] == 'u'
                            || charArray[j] == 'v'
                            || charArray[j] == 'w'
                            || charArray[j] == 'x'
                            || charArray[j] == 'y'
                            || charArray[j] == 'z') {
                        wordFinal = wordFinal + charArray[j];
                    }
                }
                
                if (!wordFinal.equals("") && wordFinal.length() > 1) { // ve se nao tem nada e se nao tiver deleta no array
                    packetWords[i] = wordFinal;
                    articleTextConsidered = articleTextConsidered + packetWords[i] + " ";
                } else {
                    packetWords[i] = null;
                }
            }
            articleTextConsidered = articleTextConsidered.replace("_", " ");
            
            //System.out.println("ULTIMO TEXTO CONSIDERADO = "+articleTextConsidered);
            //Inscrever-se sob o controle do Universo de documentos atual
            Semaforo.Instance().P();
            up = new UniverseProcessor();
            ProcessDocumentBeforeUniversePrepared();
            up.DocumentProcessorSubscribe(this);
            up.ProcessesUniversalInformation();
            Semaforo.Instance().V();
        }
    }

    /**
     * Verifica se o Processador de documento irá participar da Recuperação de informação,
     * ou seja, se possui algum documento (Artigo ou Discussão) não vazio
     * @return verdadeiro caso exista algum documento para analisar, falso se o contrário
     */
    public boolean WillParticipateOfRetrieval() {
        return (hasArticleDoc);
    }

    public void ProcessDocumentBeforeUniversePrepared() {

        //Construir a lista de Frequência dos termos no documento (freqij)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Construindo a lista de Frequencia dos termos do documento (freqij)");
        //my_notificator.SetObs("Obtendo ocorrências de termos (freqij)");
        //my_notificator.IncrementProgress(5);
        BuildHitLists();

        //Normalizar a frequência dos termos para o documento (fij)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Normalizando a Frequencia dos termos para o documento (fij)");
        //my_notificator.SetObs("Normalizando Ocorrência de termos (fij)");
        //my_notificator.IncrementProgress(5);
        NormalizeWordFrequency();
    }

    public void ProcessDocumentAfterUniversePrepared() {
        //Calcular os pesos dos Termos para o os Documentos referentes ao Artigo e à Discussão (wij)
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Calculando pesos dos termos para os textos da folksonomia '" + "' (wij)");
        CalculateDocumentWeights();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        /* Calcular os graus de similaridade (cossenos) dos "vetores" de pesos locais (wij) com os "vetores"
         * de pesos universais de pesquisa (wiq). Existe um "vetor" por família de classe, portanto o número
         * de cossenos obtidos será em relação ao número de família de classes existentes na ontologia.
         */
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Calculando os Coeficientes de Similaridade para os textos da folksonomia '" + "' (cossenos vetoriais)");
        CalculateSimilarityCoeficients();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        /* Calcular os índices de similaridade para as propriedades da ontologia, segundo as similaridades
         * encontradas para o documento e cada uma das famílias de classe.
         */
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Calculando os coeficientes de Propriedades para os atributos de similaridade da folksonomia '" + "' (cossenos vetoriais)");
        CalculatePropertiesCoeficients();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        /* Com base nos coeficientes de similaridade obtidos, deve-se então calcular uma pontuação única para
         * os documentos, para que possam então serem colocados em ordem no ranking de relevância
         */
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Totalizando a pontuação de similaridade para os textos da folksonomia '" + "' (rank value)");
        TotalizeRankingCoeficient();
        QuickMessageDisplay.Instance().IncrementProgressMessage(10);
        QuickMessageDisplay.Instance().FinishProgressMessage(1000);
    }

    public void BuildHitLists() {
        Iterator<Term> it_classFamilyTerms;
        Matcher termMatcher;
        Integer numHits;

        if (hasArticleDoc) {
            //Obter número de famílias de classes presentes na ontologia selecionada
            int numCf = OntologyExtractor.Instance().Get_NumberOfClassFamilies();

            //Loop que percorrerá todas as famílias de classe da ontologia
            for (int cf = 0; cf < numCf; cf++) {
                //Cada família de classes terá uma nova HitList
                if (hasArticleDoc) {
                    ArticleHitLists.add(new LinkedList<Integer>());
                }
                //Obter a lista de termos da família de classe corrente
                LinkedList<Term> currentClassFamily = OntologyExtractor.Instance().Get_ClassFamily(cf);
                //obter um iterator para percorrer tais termos
                it_classFamilyTerms = currentClassFamily.iterator();
                while (it_classFamilyTerms.hasNext()) {
                    Term currentTerm = it_classFamilyTerms.next();
                    //Contar hits do termo corrente para o texto do Artigo
                    if (hasArticleDoc) {
                        termMatcher = currentTerm.GetTermRegexPattern().matcher(this.articleTextConsidered);
                        numHits = new Integer(0);
                        while (termMatcher.find()) {
                            numHits++;
                        }
                        ArticleHitLists.getLast().add(numHits);
                    }
                }
                Logger.Instance().PrintBlock(Logger.LEVEL_DETAILED, Logger.NOT_ERROR, this, "Termos  = " + ListPrintFormatter.printList(currentClassFamily)
                        + (hasArticleDoc ? "\nArtHits = " + ListPrintFormatter.printListFollowingAnother(ArticleHitLists.getLast(), currentClassFamily) : ""));
            }

            //Alimentar as informações sobre o universo de textos com os parâmetros atuais obtidos
            Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Listas de freqij construídas, alimentando informações universais...");
            up.FeedUniversalInformation(hasArticleDoc, ArticleHitLists);
        }
    }

    public void NormalizeWordFrequency() {
        //###########################################
        //### NORMALIZAR FREQUÊNCIA PARA O ARTIGO ###
        //###########################################

        // Se existir texto considerado para artigo
        if (hasArticleDoc) {

            sumAllTermsHits_forArticle = new Integer(0);

            //obter a palavra mais frequente no texto
            WordCountResult_inArticle = WordCounter.Instance().GetMostFrequentWord(this.articleTextConsidered);

            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.NOT_ERROR, this, "Para o texto do Artigo: total de palavras [" + WordCountResult_inArticle.getTotalWordCount()
                    + "] , palavra mais frequente ['" + WordCountResult_inArticle.getMostFrequentWord() + "' = " + WordCountResult_inArticle.getMostFrequentWordCount() + "x]");

            ArticleNormalizedFrequencyLists = new LinkedList<LinkedList<Double>>();
            Iterator<LinkedList<Integer>> it_fc = ArticleHitLists.iterator();
            while (it_fc.hasNext()) {
                ArticleNormalizedFrequencyLists.add(new LinkedList<Double>());
                Iterator<Integer> it = it_fc.next().iterator();
                while (it.hasNext()) {
                    Integer count = it.next();
                    Double fij_value;
                    if (count > 0) {
                        sumAllTermsHits_forArticle += count;
                        fij_formula_processor.SetVariable(ConstDeclarations.freqij, count);
                        fij_formula_processor.SetVariable(ConstDeclarations.totW, WordCountResult_inArticle.getTotalWordCount());
                        fij_formula_processor.SetVariable(ConstDeclarations.maxW, WordCountResult_inArticle.getMostFrequentWordCount());
                        fij_value = fij_formula_processor.ResolveExpression();
                    } else {
                        fij_value = new Double(0);
                    }

                    ArticleNormalizedFrequencyLists.getLast().add(fij_value);

                }
            }

        } else {
            WordCountResult_inArticle = null;
            ArticleNormalizedFrequencyLists = null;
            sumAllTermsHits_forArticle = null;
        }
    }

    private void CalculateDocumentWeights() {
        //Inicializar a lista que receberá o resultado dos wijs calculados
        WijValues_forArticle = (hasArticleDoc ? new LinkedList<LinkedList<Double>>() : null);

        if (hasArticleDoc) {
            Iterator<LinkedList<Double>> it_idfiLists = up.GetInverseDocumentFrequencyLists().iterator();
            Iterator<LinkedList<Double>> it_wicLists = OntologyExtractor.Instance().Get_OntologyTermWeights().iterator();
            Iterator<LinkedList<Double>> it_fijArticleLists = (hasArticleDoc ? ArticleNormalizedFrequencyLists.iterator() : null);

            while (it_idfiLists.hasNext()) {
                // Adicionar uma lista de wij para cada família de classes
                if (hasArticleDoc) {
                    WijValues_forArticle.add(new LinkedList<Double>());
                }
                // Para cada termo da família de classes
                Iterator<Double> it_idfiFcList = it_idfiLists.next().iterator();
                Iterator<Double> it_wicFcList = it_wicLists.next().iterator();
                Iterator<Double> it_fijFcArticleList = (hasArticleDoc ? it_fijArticleLists.next().iterator() : null);
                /* Obs: Todas as listas precisam ter tamanho igual, de acordo com a
                 * lógica através da qual são construídas (Sempre referenciando as
                 * posições originais de Família de Classes e termos obtidos da ontologia
                 */
                while (it_idfiFcList.hasNext()) {
                    Double idfi_value = it_idfiFcList.next();
                    Double wic_value = it_wicFcList.next();
                    Double fij_article_value = (hasArticleDoc ? it_fijFcArticleList.next() : null);

                    // Informar o inverso da frequência de documentos para o termo corrente
                    wij_formula_processor.SetVariable(ConstDeclarations.idfi, idfi_value);
                    // Informar o peso na família de classes da ontologia para o termo corrente
                    wij_formula_processor.SetVariable(ConstDeclarations.Wic, wic_value);

                    if (hasArticleDoc) {
                        // Informar a frequência normalizada do termo corrente para o documento referente ao Artigo
                        wij_formula_processor.SetVariable(ConstDeclarations.Fij, fij_article_value);
                        // Calcular o valor wij para o termo corrente para o Artigo
                        Double wij_article_value = wij_formula_processor.ResolveExpression();
                        //Armazenar os valores wij recém calculados em suas respectivas listas e posição
                        WijValues_forArticle.getLast().add(wij_article_value);
                    }

                }
            }
        }
    }

    private void CalculateSimilarityCoeficients() {
        //Inicializar as listas que armazenarão o grau de similaridade do documento com o vetor pesquisa
        ClassFamiliesSimilarities_forArticle = (hasArticleDoc ? new LinkedList<Double>() : null);

        if (hasArticleDoc) {
            // Obter a lista de Listas (uma por família de classes) de pesos de termos para pesquisa (wiq)
            Iterator<LinkedList<Double>> it_wiqLists = up.GetUniversalQueryWeights().iterator();
            // Obter a lista de Listas (uma por família de classes) de pesos de termos para o documento (wij)
            Iterator<LinkedList<Double>> it_wijArticleLists = (hasArticleDoc ? WijValues_forArticle.iterator() : null);
            //Para todas as famílias de classe
            while (it_wiqLists.hasNext()) {
                //Obter o "vetor" pesquisa contendo os pesos wiq para uma família de classes
                LinkedList<Double> familyClass_wiqList = it_wiqLists.next();

                if (hasArticleDoc) {
                    //Calcular o grau de similaridade (através do cosseno) do "vetor" pesquisa com o "vetor" documento do Artigo
                    Double article_similarity = CalculateCosine(it_wijArticleLists.next(), familyClass_wiqList);
                    //Se o cálculo da similaridade não foi matematicamente possível (divisão por 0 por exemplo), assumir 0
                    if (article_similarity.isInfinite() || article_similarity.isNaN()) {
                        article_similarity = new Double(0);
                    }
                    //Armazenar o grau de similaridade encontrado {sim(d,j)} na lista de similaridades do Artigo
                    ClassFamiliesSimilarities_forArticle.add(article_similarity);
                    Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.NOT_ERROR, this, "Similaridade do texto do artigo com a Família de Classes " + ClassFamiliesSimilarities_forArticle.size()
                            + "(" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(ClassFamiliesSimilarities_forArticle.size() - 1) + ") = " + article_similarity);
                }
            }
        }
    }

    /**
     * Calcula o cosseno entre dois "vetores" (na verdade estruturados como LinkedLists) N-dimensionais.
     * Este cosseno também é conhecido como similaridade, pois quanto mais próximo de 0, mais obtuso será 
     * o ângulo formado entre os "vetores" e, portanto, mais discrepantes as informações neles contidas, já
     * quanto mais próximo de 1, mais agudo será o ângulo formado entre estes "vetores" e, portanto, mais
     * similares são as informações neles contidas.
     * @param documentWeights A linkedList de consulta, contendo os pesos wij
     * @param queryWeights A linkedList de pesquisa, contendo os pesos wiq
     * @return O cosseno calculado através do uso dos valores existentes nas listas passadas por parâmetros
     * como se fossem dimensões de um vetor.
     */
    private Double CalculateCosine(LinkedList<Double> documentWeights, LinkedList<Double> queryWeights) {
        //Obter os iteradores para percorrer os valores wij e wiq dos termos
        Iterator<Double> it_wij = documentWeights.iterator();
        Iterator<Double> it_wiq = queryWeights.iterator();
        //Inicializar todas as somatórias a serem realizadas
        Double sum_wij_x_wiq = new Double(0);
        Double sum_wij_squared = new Double(0);
        Double sum_wiq_squared = new Double(0);
        //Realizar as somatórias necessárias
        while (it_wij.hasNext()) {
            Double wij_value = it_wij.next();
            Double wiq_value = it_wiq.next();

            sum_wij_x_wiq += wij_value * wiq_value;
            sum_wij_squared += wij_value * wij_value;
            sum_wiq_squared += wiq_value * wiq_value;
        }

        //Calcular o cosseno, efetivamente, utilizando as somatórias préviamente calculadas.
        Double cosine = sum_wij_x_wiq / (Math.sqrt(sum_wij_squared) * Math.sqrt(sum_wiq_squared));

        return cosine;
    }

    private void CalculatePropertiesCoeficients() {
        //Obter o iterados para todos os escopos de propriedades existentes na ontologia
        Iterator<PropertyScope> it_propScope = OntologyExtractor.Instance().Get_ClassFamiliesPropertyScopeList().iterator();
        //Criar a lista ligada aonde ficarão armazenados os resultados.
        PropertiesScopesCoeficients_forArticle = (hasArticleDoc ? new LinkedList<Double>() : null);
        //Para todos os escopos de Propriedade
        while (it_propScope.hasNext()) {
            PropertyScope currentPropScope = it_propScope.next();
            if (hasArticleDoc) {
                Double article_coeficient = ClassFamiliesSimilarities_forArticle.get(currentPropScope.GetOneClassFamily()) * ClassFamiliesSimilarities_forArticle.get(currentPropScope.GetOtherClassFamily());
                PropertiesScopesCoeficients_forArticle.add(article_coeficient);
                Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.NOT_ERROR, this, "Coeficiente da Propriedade " + currentPropScope.GetProperty().GetPropertyName() + "( "
                        + "(" + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(currentPropScope.GetOneClassFamily()) + " , " + OntologyExtractor.Instance().Get_ClassFamilyTopMostTermName(currentPropScope.GetOtherClassFamily()) + " ) para o texto do Artigo = " + article_coeficient);
            }
        }
    }

    private void TotalizeRankingCoeficient() {
        // Obter os iteradores para percorrer os graus de similaridade obtidos entre as famílias de classe e documentos
        Iterator<Double> it_article_sim = (hasArticleDoc ? ClassFamiliesSimilarities_forArticle.iterator() : null);
        Iterator<Double> it_article_coef = (hasArticleDoc ? PropertiesScopesCoeficients_forArticle.iterator() : null);

        // Inicicializar a totalização das similaridades obtidas
        Double article_total_sim = (hasArticleDoc ? new Double(0) : null);
        Double article_prop_coef = (hasArticleDoc ? new Double(0) : null);
        Double max_sim_fc = (hasArticleDoc ? new Double(0) : null);
        Double max_sim_prop = (hasArticleDoc ? new Double(0) : null);
        // Totalizar os valores de similaridades entre os documentos e as famílias de classe
        for (int fc = 0; fc < OntologyExtractor.Instance().Get_NumberOfClassFamilies(); fc++) {
            //adaptação pegando o maior das similaridades
            Double sim = it_article_sim.next();
            if (hasArticleDoc && sim > max_sim_fc) {
                max_sim_fc = sim;
            }
            article_total_sim = (hasArticleDoc ? article_total_sim + sim : null);
        }

        for (int p = 0; p < OntologyExtractor.Instance().Get_NumberOfPropertiesScopes(); p++) {
            //adaptação
            Double sim = it_article_coef.next();
            if (hasArticleDoc && sim > max_sim_prop) {
                max_sim_prop = sim;
            }
            article_prop_coef = (hasArticleDoc ? article_prop_coef + sim : null);
        }
        Double simFC = article_total_sim;
        Double simProp = article_prop_coef;

        if (article_total_sim > 1) {
            simFC = max_sim_fc;
        }
        if (article_prop_coef > 1) {
            simProp = max_sim_prop;
        }


        if (hasArticleDoc) {
            this.rank_formula_processor.SetVariable(ConstDeclarations.rankFc, simFC);
            this.rank_formula_processor.SetVariable(ConstDeclarations.rankProp, simProp);
            article_total_rank = this.rank_formula_processor.ResolveExpression();

            //possibilidade nao usada para calculo
            //article_total_rank = article_total_rank/Math.sqrt((article_total_sim*article_total_sim)+(article_prop_coef*article_prop_coef));
        }

        // Normalizar o ranking para um valor de 0 a 1 e armazenar o resultado
        article_rank_value = (hasArticleDoc ? article_total_rank / OntologyExtractor.Instance().Get_MaximumRankValuePossible() : null);

        Logger.Instance().PrintBlock(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, this, "Ranking para a folksonomia '" + "': "
                + (hasArticleDoc ? "\nRanking Artigo: rankFc = " + article_total_sim + " , rankProp = " + article_prop_coef + " , Geral = " + article_total_rank + " , Normalizado = " + article_rank_value : "")
                + "\nObs: formula de rank definida para calculo do Ranking 'Geral' = " + rank_formula_processor.GetOriginalInFixExpression());
    }

    /**
     * Obter o coeficiente de similaridade encontrado (normalizado para um valor
     * entre 0 a 1) para o texto considerado para o artigo.
     * @return Coeficiente de similaridade encontrado caso exista texto de artigo
     * considerado, ou nulo caso contrário.
     */
    public Double Get_ArticleNormalizedRankValue() {
        return this.article_rank_value;
    }

    /**
     * Obter o coeficiente de similaridade encontrado (normalizado para um valor
     * entre 0 a 1) para o texto considerado para a discussão.
     * @return Coeficiente de similaridade encontrado caso exista texto de discussão
     * considerado, ou nulo caso contrário.
     */
    /**
     * Obter o maior coeficiente de similaridade dentre o coeficiente para o artigo
     * e para a discussão.
     * @return Maior coeficiente de similaridade, caso ao menos exista um coeficiente não nulo
     * para o artigo ou para a discussão. Caso ambos os coeficientes sejam nulos, retorna nulo.
     */
    public Double Get_GreatestNormalizedRankValue() {
        if (article_rank_value == null) {
            return null;
        }
        Double articleRank = (article_rank_value == null ? 0 : article_rank_value);

        return articleRank;
    }

    /**
     * Obeter o componente de recuperação de Informação no qual o presente objeto de
     * DocumentProcessor tem a sua construção baseada.
     * @return O componente de recuperação de Informação do DocumentProcessor.
     */
    public Integer GetAllTermsTotalHitCount_forArticle() {
        return sumAllTermsHits_forArticle;
    }

    public Double GetOneClassFamilySimilarity_forArticle(int family_index) {
        return ClassFamiliesSimilarities_forArticle.get(family_index);
    }

    public Double GetTotalPropertyCoeficient_forArticle() {
        Double result = new Double(0);
        Iterator<Double> it_propCoef = this.PropertiesScopesCoeficients_forArticle.iterator();
        while (it_propCoef.hasNext()) {
            result = result + it_propCoef.next();
        }
        return result;
    }

    public boolean HasArticleDoc() {
        return this.hasArticleDoc;
    }
}
