/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.portugueseStemmer;


/**
 *
 * @author David Puziol Prata
 */
public class WordModifier {

    /*
     * Regex que detecta a escrita palavra a palavra em qualquer uma das seguintes formas:
     * 1) CamelCaseWord
     * 2) separado_por_sublinhados
     * 3) escrita separada por espaços
     */
    final static String WORD_DELIMITER_REGEX = "[A-Z][^A-Z_\\s]+|[^_\\s]+";
    final static Pattern Word_Delimiter_Pattern = Pattern.compile(WORD_DELIMITER_REGEX);
    
    public static String GetCleanWordLowered(String origin)
    {
        if(origin == null || origin.isEmpty())
        {
            return origin;
        }
        
        String returnWord = "";
        
        //Matcher criado sem o uso do PatternManager por causa da necessidade de ser case sensitive
        Matcher wordMatcher = Word_Delimiter_Pattern.matcher(origin);

        while(wordMatcher.find())
        {
            returnWord = returnWord+wordMatcher.group()+" ";
        }
        returnWord = returnWord.toLowerCase().trim();
        
        return returnWord;
    }
    
    public static String TransformAccentuatedCharacters(String text)
    {
        return text.replaceAll("[ç]",       "c")
                   .replaceAll("[áàãâä]",   "a")
                   .replaceAll("[éèêë]",    "e")
                   .replaceAll("[íìîï]",    "i")
                   .replaceAll("[óòõôö]",   "o")
                   .replaceAll("[úùûü]",    "u")
                   .replaceAll("[Ç]",       "C")
                   .replaceAll("[ÁÀÃÂÄ]",   "A")
                   .replaceAll("[ÉÈÊË]",    "E")
                   .replaceAll("[ÍÌÎÏ]",    "I")
                   .replaceAll("[ÓÒÕÔÖ]",   "O")
                   .replaceAll("[ÚÙÛÜ]",    "U")
                   ;
    }
    
    private static SnowballStemmer stemmer = (SnowballStemmer) new portugueseStemmer();
    
    public static synchronized String verbStem(String verb)
    {
        stemmer.setCurrent(verb);
        stemmer.stem();
        
        String stem =  stemmer.getCurrent();
        
        if(stem == null || stem.isEmpty())
            return verb;
        else
            return stem;
    }
}
