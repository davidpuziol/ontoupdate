/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

/**
 *
 * @author David Puziol Prata
 */
public class PropertyScope {

    Property my_property;
    Integer one_related_class_family;
    Integer other_related_class_family;

    public PropertyScope(Property property, Integer oneClassFamily, Integer otherClassFamily) {
        this.my_property = property;
        this.one_related_class_family = oneClassFamily;
        this.other_related_class_family = otherClassFamily;
    }

    public Property GetProperty() {
        return my_property;
    }

    public Integer GetOneClassFamily() {
        return one_related_class_family;
    }

    public Integer GetOtherClassFamily() {
        return other_related_class_family;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PropertyScope) {
            if (((PropertyScope) obj).GetProperty() == this.GetProperty()
                    && (((PropertyScope) obj).GetOneClassFamily() == this.GetOneClassFamily() && ((PropertyScope) obj).GetOtherClassFamily() == this.GetOtherClassFamily()
                    || ((PropertyScope) obj).GetOneClassFamily() == this.GetOtherClassFamily() && ((PropertyScope) obj).GetOtherClassFamily() == this.GetOneClassFamily())) {
                return true;
            } else {
                return false;
            }
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (this.my_property != null ? this.my_property.hashCode() : 0);
        hash = 41 * hash + (this.one_related_class_family != null ? this.one_related_class_family.hashCode() : 0);
        hash = 41 * hash + (this.other_related_class_family != null ? this.other_related_class_family.hashCode() : 0);
        return hash;
    }
}
