 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.rtf.RTFEditorKit;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;
import ontoupdate.extractionModule.Document;
import ontoupdate.extractionModule.OntologyConceptualClustering;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.pdfbox.pdfparser.PDFParser;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

/**
 *
 * @author David Puziol Prata
 */
class RequestProcess extends Thread {

    private String hostName; // host
    private int hostPort; // porta
    private String url = ""; // para acessar o link
    private DocumentProcessor dp = null; // documento para processamento que será gerado a partir da extração
    private String[] arrayString; // array de palavras da requisição
    private boolean flagSytem;
    // para frente tem que receber a linha da tabela para colocar dados

    public RequestProcess(byte[] request, String hostName, int hostPort) { // o array de bite recebi eh a requisição
        this.hostName = hostName;
        this.hostPort = hostPort;

        this.setPriority(RequestProcess.MIN_PRIORITY);
        ServerProxyForm.Instance().IncThread();// isso eh referente ao meu programa so pra atualizar uma jtable tem nada  ver com a classe

        char[] charArray = new String(request).toCharArray(); // pega o requisição e array de bits e transforma em string
        String requestTemp = new String(charArray);

        if (requestTemp.startsWith("GET")) // so have URL // se tiver GET na requisição eh pq tem url
        {
            arrayString = requestTemp.split(" "); // DANDO UM SPLIT A URL FICA NA SENDO A SEGUNDA STRING DO ARRAY DE STRINGS
            url = arrayString[1];
            System.out.println(url);
            urlTemp = this.url.toLowerCase();
            CheckGet(url);
        } else {
            ServerProxyForm.Instance().DecThread();
            this.interrupt();// se nao tem get NAO TEM URL entao interromp a thread
        }
    }
    //urltemp é somente utilizado para comparação
    String urlTemp;

    private void CheckGet(String url) {

        //colocando em minusculos para availiação   
        if (urlTemp.endsWith(".pdf") || urlTemp.endsWith(".rtf") || urlTemp.endsWith(".doc") || urlTemp.endsWith(".txt")) {
            urlTemp = url;

            this.start(); // se for qualquer arquivo texto entao processa
        } else {
            TableUpdate(arrayString); // chama a tabela para inserir a linha não relevante
            ServerProxyForm.Instance().DecThread();// so para atualizar a jtable nao tem nada a ver com a classe em si
            this.interrupt();  // se nao for arquivo texto interromp a thread e para
        }
    }

    @Override
    public void run() {
        try {
            while (CurrentProfile.Instance().isInProcessedURL(url)) {
                try {
                    wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (CurrentProfile.Instance().isProcessedURL(url) != true) {
                CurrentProfile.Instance().addInProcess(url);
                URL urlTemp = new URL(this.url); // cria a url a partir da string
                String[] urlStrings = this.url.split("/"); // reparte a url para pegar o nome do arquivo
                String nameArchive = urlStrings[urlStrings.length - 1]; // o ultimo é o nome do arquivo e sua extensão
                InputStream in = urlTemp.openStream(); // abre a conexão

                // se existir o arquivo pega a instancia e concatena
                // se nao exitir o arquivo cria um e concatena

                File file = new File(nameArchive);

                //if (!file.exists()) { // usuario ainda nao teve a chance de retirar a ontologia deste texto
                FileOutputStream fileOutput = new FileOutputStream(file); // direciona uma saida para o arquivo
                fileOutput.flush();
                byte[] buf = new byte[1024]; // cria o buffer
                int len; // inteiro com o tamanho do que foi escrito no buffer
                while ((len = in.read(buf)) > 0) {
                    for (int i = 0; i < len; i++) {
                        fileOutput.write(buf[i]);  // coloca no arquivo
                        fileOutput.flush();
                    }
                }
                fileOutput.flush();
                fileOutput.close();
                in.close();

                String path = file.getAbsolutePath();
                if (this.urlTemp.endsWith(".pdf")) {
                    ParsePDF(path); // ja manda o arquivo
                }
                if (this.urlTemp.endsWith(".rtf")) {
                    ParseRTF(path);
                }
                if (this.urlTemp.endsWith(".doc")) {
                    ParseDOC(path);
                }
                if (this.urlTemp.endsWith(".txt")) {
                    ParseTXT(path);
                }
                CurrentProfile.Instance().removeInProcess(url);
                try {
                    notify();
                } catch (Exception e) {
                }
            }

        } catch (MalformedURLException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            ServerProxyForm.Instance().DecThread();
        }
    }

    private void ParsePDF(String path) {
        File f = new File(path);
        FileInputStream is = null;
        try {
            is = new FileInputStream(f);
        } catch (IOException e) {
            System.out.println("ERRO: " + e.getMessage());
        }

        PDDocument pdfDocument = null;
        String bodyText = "";
        try {
            PDFParser parser = new PDFParser(is);
            parser.parse();
            pdfDocument = parser.getPDDocument();
            PDFTextStripper stripper = new PDFTextStripper();
            bodyText = stripper.getText(pdfDocument);

            pdfDocument.close();
            is.close();
            f.delete();
            ProcessText(bodyText, this.url);

        } catch (IOException e) {
            System.out.println("ERRO: Can't open stream" + e);
        } catch (Throwable e) {
            System.out.println("ERRO: An error occurred while getting contents from PDF" + e);
        } finally {
            try {
                pdfDocument.close();
            } catch (IOException ex) {
                Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            f.delete();
        }
    }

    private void ParseRTF(String path) {
        File f = new File(path);
        FileInputStream is = null;
        try {
            is = new FileInputStream(f);
        } catch (IOException e) {
            //System.out.println("ERRO: " + e.getMessage());
        }
        String bodyText = "";

        DefaultStyledDocument styledDoc = new DefaultStyledDocument();
        try {
            new RTFEditorKit().read(is, styledDoc, 0);
            bodyText = styledDoc.getText(0, styledDoc.getLength());
            is.close();
            f.delete();
            ProcessText(bodyText, this.url);

        } catch (IOException e) {
            System.out.println("ERRO: Cannot extract text from a RTF document. Exception: " + e);

        } catch (BadLocationException e) {
            System.out.println("ERRO: Cannot extract text from a RTF document. Exception: " + e);
        } finally {
            try {
                is.close();
                f.delete();
            } catch (IOException ex) {
                Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void ParseDOC(String path) {
        File f = new File(path);
        FileInputStream is = null;
        try {
            String bodyText = "";
            is = new FileInputStream(f);
            HWPFDocument wdoc = new HWPFDocument(is);
            WordExtractor extractor = new WordExtractor(wdoc);
            bodyText = extractor.getText();
            bodyText = bodyText.replace("\n", " ");
            System.out.println(bodyText);
            is.close();
            f.delete();
            ProcessText(bodyText, this.url);

        } catch (IOException e) {
            System.out.println("ERRO: " + e.getMessage());
            this.statusOLT(true);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            f.delete();
        }
    }

    private void ParseTXT(String path) {
        File f = new File(path);
        FileInputStream is = null;

        try {
            is = new FileInputStream(f);
            String bodyText = "";
            String line;
            BufferedReader data = new BufferedReader(new InputStreamReader(is));
            while ((line = data.readLine()) != null) {
                bodyText = bodyText + " " + line;
            }
            bodyText = bodyText.replaceAll("\n", " ");
            is.close();
            f.delete();
            ProcessText(bodyText, this.url);
        } catch (IOException e) {
            System.out.println("ERRO: " + e.getMessage());
            this.statusOLT(true);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            f.delete();
        }
    }

    private void ProcessText(String text, String path) {
        dp = new DocumentProcessor(text);
        CheckRelevantToExtraction(path, text);
        TableUpdate(arrayString); // atualiza a tabela depois dos parsings

    }

    private void CheckRelevantToExtraction(String path, String originalText) {
        if (dp.article_total_rank >= CurrentProfile.Instance().getMin_coeficient()) {
            // MONTAR CLASSE PARA O TEXTO PRONTO PARA EXTRAÇÃO, COM RADICAL DOS VERBOS REDUZIDOS, SEM AS STOPWORDS CASO

            boolean flag = true;
            while (flag) {
                if (!ServerProxyForm.Instance().canContinue()) {
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    CurrentProfile.Instance().addURLprocessed(url);
//            flagSytem = true;
//            SemaforoRequest.Instance().V();
                    this.statusBuild();
                    Document doc = new Document(dp.articleTextConsidered, path, originalText);
                    OntologyConceptualClustering.Instance().insertDocument(doc);
                    flag = false;
                }
            }
            this.statusOLT(false);
        } else {
            if (dp.article_total_rank == 0) { // primeira requisicao
            }
            this.statusOLT(true);
        }
    }

    private void TableUpdate(String[] arrayString) {

        String nameAndIp = null;
        try {
            nameAndIp = InetAddress.getByName(hostName).toString();
        } catch (UnknownHostException ex) {
            Logger.getLogger(RequestProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < arrayString.length; i++) {
        }
        int posToken = nameAndIp.indexOf("/");
        String ip = "";
        if (posToken > 0) {
            try {
                ip = nameAndIp.toString().substring(posToken + 1);

            } catch (Exception e) {
                System.out.println(e);
            }
        }
        Date dataAtual = new Date();
        String[] dados = new String[6];
        dados[0] = url;
        dados[1] = ip.toString();
        dados[2] = hostName;
        dados[3] = Integer.toString(hostPort);
        dados[4] = dataAtual.toLocaleString();
        if (dp == null) {
            dados[5] = "no"; // os dados não conferem com os parsing então não foi montado um documento
        } else {
            dados[5] = dp.article_total_rank.toString(); // dados conferem com os parsing e foi montado um documento que teve um coeficiente
        }

        ServerProxyForm.Instance().AddRowTable(dados); // armazena na tabela
    }

    private void statusOLT(boolean flag) {

        if (ServerProxyForm.Instance().getOlt() != null) {
            ServerProxyForm.Instance().getOlt().StatusAtualizar(flag);
        }
    }

    private void statusBuild() {
        if (ServerProxyForm.Instance().getOlt() != null) {
            ServerProxyForm.Instance().getOlt().StatusBuild();
        }
    }
}
