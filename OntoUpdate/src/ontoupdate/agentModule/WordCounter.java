/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.agentModule;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import ontoupdate.util.PatternManager;


/**
 *
 * @author David Puziol Prata
 */
public class WordCounter {
    
    final String REGEX_ONE_WORD = "[\\w-áéíóúãõàèìòùâêîôûäëïöüç]+";
    HashMap<String,Integer> UniversalWordFrequency;
    Integer UniversalTotalWordCount;
    
    static WordCounter instance;
    
    public class WordCountResult{
        protected WordCountResult(String mostFrequentWord, Integer mostFrequentWordCount, Integer totalWordCount){
            this.mostFrequentWord = mostFrequentWord;
            this.mostFrequentWordCount = mostFrequentWordCount;
            this.totalWordCount = totalWordCount;
        }
        
        String mostFrequentWord;
        Integer mostFrequentWordCount;
        Integer totalWordCount;
       
        public Integer getTotalWordCount() {
            return totalWordCount;
        }

        public Integer getMostFrequentWordCount() {
            return mostFrequentWordCount;
        }

        public String getMostFrequentWord() {
            return mostFrequentWord;
        }
    }
    
    static WordCounter Instance()
    {
        if (instance == null) {
            instance = new WordCounter();
        }

        return instance;
    }
    
    protected WordCounter()
    {
        Reset();
    }
    
    public void Reset()
    {
        UniversalTotalWordCount = 0;
        UniversalWordFrequency = new HashMap<String,Integer>();
    }

    public WordCountResult GetMostFrequentWord(String text)
    {
        HashMap<String,Integer> docWordMap = new HashMap<String,Integer>();
        Matcher wordMatcher;
        String word;
        Integer wordCount;
        Integer totalWordCount = 0;
        
        wordMatcher = PatternManager.Instance().GetMatcher(REGEX_ONE_WORD, text);
        
        while(wordMatcher.find())
        {
            totalWordCount++;
            word = wordMatcher.group().toLowerCase();
            wordCount = docWordMap.get(word);
            if(wordCount == null)
            {
                wordCount = 0;
            }
            docWordMap.put(word, wordCount+1);
        }
        
        FeedUniversalInformation(docWordMap,totalWordCount);
        
        Entry<String,Integer> mostFrequentWord= GetMostFrequentWordFromHashMap(docWordMap);
        
        return new WordCountResult(mostFrequentWord.getKey(),mostFrequentWord.getValue(),totalWordCount);
    }
    
    public WordCountResult GetUniversalMostFrequentWord()
    {
        Entry<String,Integer> mostFrequentWord= GetMostFrequentWordFromHashMap(UniversalWordFrequency);
        
        return new WordCountResult(mostFrequentWord.getKey(),mostFrequentWord.getValue(),UniversalTotalWordCount);
    }
    
    private synchronized void FeedUniversalInformation(HashMap<String,Integer> docWordMap, Integer docTotalWordCount)
    {
        Set<Entry<String,Integer>> docWordSet = docWordMap.entrySet();
        Iterator<Entry<String,Integer>> itDocWordSet = docWordSet.iterator();
        Entry<String,Integer> currentWord = null;
        Integer wordCount;
        
        UniversalTotalWordCount += docTotalWordCount;
        
        while(itDocWordSet.hasNext())
        {
            currentWord = itDocWordSet.next();
            wordCount = UniversalWordFrequency.get(currentWord.getKey());
            if(wordCount == null)
            {
                wordCount = 0;
            }
            UniversalWordFrequency.put(currentWord.getKey(), wordCount+currentWord.getValue());
        }
    }
    
    private Entry<String,Integer> GetMostFrequentWordFromHashMap(HashMap<String,Integer> docWordMap)
    {
        Set<Entry<String,Integer>> docWordSet = docWordMap.entrySet();
        Iterator<Entry<String,Integer>> itDocWordSet = docWordSet.iterator();
        Entry<String,Integer> currentWord = null;
        int maxWordCount = 0;
        Entry<String,Integer> mostFrequentWord = null;
        
        while(itDocWordSet.hasNext())
        {
            currentWord = itDocWordSet.next();
            if(currentWord.getValue() > maxWordCount)
            {
                maxWordCount = currentWord.getValue();
                mostFrequentWord = currentWord;
            }
        }
        return mostFrequentWord;
    }
}
