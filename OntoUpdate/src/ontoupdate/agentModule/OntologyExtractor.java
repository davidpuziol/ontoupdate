/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.agentModule;

import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.OWLClass;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLIntersectionClass;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import edu.stanford.smi.protegex.owl.model.OWLUnionClass;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSClass;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import ontoupdate.agentModule.graficInterface.ConfigurationManager;
import ontoupdate.specialComponents.CustomIconsTreeCellEntry;
import ontoupdate.util.Logger;
import ontoupdate.util.MathExpressionProcessor;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
@SuppressWarnings("unchecked")
public class OntologyExtractor {

    private File owl_file;
    private OWLModel owlModel;
    private JenaOWLModel jena;
    private static Collection<OWLNamedClass> userClasses;
    private static String defaultNamespace;
    private LinkedList<LinkedList<Term>> classFamilies;
    private LinkedList<LinkedList<RDFResource>> classFamiliesRdfResources;
    private LinkedList<LinkedList<Double>> wicValues;
    private LinkedList<Property> classFamiliesProperties;
    private LinkedList<PropertyScope> classFamiliesPropertiesScopes;
    private LinkedList<String> listNames;
    private static OntologyExtractor instance;

    public static void setInstance(OntologyExtractor aInstance) {
        instance = aInstance;
    }

    public static OntologyExtractor getInstance() {
        return instance;
    }
    
        /**
     * Obtém a instância da Atual ontologia configurada
     * @return extrator da ontologia atualmente configurada
     */
    static public OntologyExtractor Instance() {
        return getInstance();
    }

    /**
     * @return the userClasses
     */
    public static Collection<OWLNamedClass> getUserClasses() {
        return userClasses;
    }

    /**
     * @param aUserClasses the userClasses to set
     */
    public static void setUserClasses(Collection<OWLNamedClass> aUserClasses) {
        userClasses = aUserClasses;
    }

    /**
     * @return the defaultNamespace
     */
    public static String getDefaultNamespace() {
        return defaultNamespace;
    }

    /**
     * @param aDefaultNamespace the defaultNamespace to set
     */
    public static void setDefaultNamespace(String aDefaultNamespace) {
        defaultNamespace = aDefaultNamespace;
    }

    /**
     * Configurar uma nova ontologia para utilização
     * @param owl_file Arquivo OWL da ontologia a extrair os dados
     */
    static public void GenerateNewInstance(File owl_file) {
        setInstance(new OntologyExtractor(owl_file));
    }

    /**
     * Construtor Protegido da Classe de Extração de Ontologias
     * @param owl_file Arquivo OWL da ontologia a extrair os dados
     */
    protected OntologyExtractor(File owl_file) {
        this.owl_file = owl_file;


        QuickMessageDisplay.Instance().StartProcessProgressMessage("Lendo a Ontologia " + owl_file.getName(), 0, 100, 10);

        // Se o arquivo existe (deve existir e ser checado pela interface com o usuário)
        if (owl_file.exists()) {
            try {
                // Extrair as informações do arquivo OWL
                ExtractOntologyInfo(null);
            } catch (Exception ex) {
                Logger.Instance().UnexpectedException(this, ex);
            }
        }
    }

    public void Reload(OWLModel modelo) {
        QuickMessageDisplay.Instance().StartProcessProgressMessage("Recarregando a ontologia modificada no sistema ", 0, 100, 10);
        try {
            // Extrair as informações do arquivo OWL
            ExtractOntologyInfo(modelo);
        } catch (Exception ex) {
            Logger.Instance().UnexpectedException(this, ex);
        }
    }

    public void ExtractOntologyInfo(OWLModel modelo) throws Exception {
        //Preparar a extração, através da criação de um modelo de Ontologia JENA/Protégé
        if (modelo == null) {

            InputStream is = new FileInputStream(getOwl_file());
            this.setJena(ProtegeOWL.createJenaOWLModelFromInputStream(is));
            setOwlModel(this.getJena());
        } else {
            setOwlModel(modelo);
        }

        QuickMessageDisplay.Instance().IncrementProgressMessage(30);
//
//        /*Obter a listagem das classes criadas pelo usuário
//        (Para evitar a futura obtenção de classes não desejadas pelo sistema) */
        setUserClasses((Collection<OWLNamedClass>) getOwlModel().getUserDefinedOWLNamedClasses());
//        /*Obter a namespace padrão para a ontologia selecionada
//        (As demais namespaces serão ignoradas durante a extração)*/
        setDefaultNamespace(getOwlModel().getDefaultOWLOntology().getNamespace());
//
//        //Inicializar a lista ligada de Famílias de Classes
        setClassFamilies(new LinkedList<LinkedList<Term>>());
//
//        //Inicializar a lista ligada de RDFResource para os objetos originais da ontologia
        setClassFamiliesRdfResources(new LinkedList<LinkedList<RDFResource>>());
//
//        //Inicializar a lista ligada de Propriedades estipuladas entre as famílias de classes
        setClassFamiliesProperties(new LinkedList<Property>());
        setClassFamiliesPropertiesScopes(new LinkedList<PropertyScope>());

        setListNames(new LinkedList<String>());

        /*Disparar o procedimento recursivo que varrerá a hierarquia de classes da
        ontologia, cobrindo também as instâncias aí presentes */
        ExploreSubclasses(getOwlModel().getOWLThingClass(), 1);

        QuickMessageDisplay.Instance().IncrementProgressMessage(30);
        //Finalizada a extração, calcular e normalizar os pesos da ontologia
        CalculateWeights();

        //Obter a relação entre as famílias de classe estipuladas pelas propriedades de Objeto
        ExploreObjectProperties();

        QuickMessageDisplay.Instance().IncrementProgressMessage(30);
        getNames();
        
        QuickMessageDisplay.Instance().FinishProgressMessage(1500);
    }
    private int i = 0;

    private void ExploreSubclasses(OWLClass owlclass, int deep_level) {
        System.out.println("\n\nciclo = " + (++i));
        /* trecho em que serão obtidas as INSTÂNCIAS */
        Collection instances = owlclass.getInstances(false);
        System.out.println("numero de instancias totais = " + owlclass.getInstances(false).size());

        Iterator<OWLIndividual> i = (Iterator<OWLIndividual>) instances.iterator();

        while (i.hasNext()) {
            OWLIndividual individual = i.next();
            if (individual.getNamespace().equals(getDefaultNamespace())) {
                // Para o nível 1, estabelece uma nova família de classes 
                if (deep_level == 1) {
                    getClassFamilies().add(new LinkedList<Term>());
                    getClassFamiliesRdfResources().add(new LinkedList<RDFResource>());
                }
                /* Verificação realizada para evitar que uma ontologia OWL com ciclo fechado de
                 * herança (o que é errado e estaria semanticamente inconsistente) coloque o programa
                 * em loop infinito 
                 */
                if (!classFamiliesRdfResources.getLast().contains(individual)) {
                    System.out.println("adicionando individuo = " + individual.getName() + " com level = " + deep_level);
                    getClassFamilies().getLast().add(new Term(individual, deep_level));
                    getClassFamiliesRdfResources().getLast().add(individual);
                }
            }
        }

        /* trecho em que serão obtidas as SUBCLASSES */

        Collection subs = owlclass.getNamedSubclasses(false);
        System.out.println("numero de subclasses totais totais = " + owlclass.getNamedSubclasses(false).size());
        Iterator<OWLNamedClass> j = (Iterator<OWLNamedClass>) subs.iterator();
        while (j.hasNext()) {
            try {
                OWLNamedClass subclasse = j.next();
                if (subclasse.getNamespace().equals(getDefaultNamespace())) {
                    // Para o nível 1, estabelece uma nova família de classes
                    if (deep_level == 1) {
                        getClassFamilies().add(new LinkedList<Term>());
                        getClassFamiliesRdfResources().add(new LinkedList<RDFResource>());
                    }
                    /* Verificação realizada para evitar que uma ontologia OWL com ciclo fechado de
                     * herança (o que é errado e estaria semanticamente inconsistente) coloque o programa
                     * em loop infinito
                     */
                    if (!classFamiliesRdfResources.getLast().contains(subclasse)) {
                        System.out.println("adicionando individuo = " + subclasse.getName() + " com level = " + deep_level);
                        getClassFamilies().getLast().add(new Term(subclasse, deep_level));
                        getClassFamiliesRdfResources().getLast().add(subclasse);
                    }

                    //Recursividade para as subclasses, até alcançar as folhas.
                    ExploreSubclasses((OWLNamedClass) subclasse, deep_level + 1);
                }
            } catch (ClassCastException ex) {
                /* Uma exception relativa ao cast incorreto de classes neste ponto
                 * significa que a subclasse analisada não é uma OWLNamedClass
                 * propriamente dita e, portanto, deve ser ignorada e prosseguir para
                 * a próxima iteração do While
                 */
                continue;
            }
        }
    }

    private void CalculateWeights() {
        //Inicializar a Lista de Listas dos pesos dos termos para as Famílias de Classes
        setWicValues(new LinkedList<LinkedList<Double>>());
        //Para todas as famílias de classes
        System.out.println("\ninicio do calculo de pesos " + getClassFamilies().size());
        for (int i = 0; i < getClassFamilies().size(); i++) {
            //Obter a máxima profundidade (na hierarquia de classes) dos termos da família de Classes
            int maxLevel = 0;
            for (int j = 0; j < getClassFamilies().get(i).size(); j++) {
                int currentLevel = getClassFamilies().get(i).get(j).getTermDeepLevel();
                if (currentLevel > maxLevel) {
                    maxLevel = currentLevel;
                }
            }

            /* Adicionar uma nova lista de wic para armazenar os resultados da
             * família de classes corrente 
             */
            getWicValues().add(new LinkedList<Double>());

            //Para todos os termos da Família de Classes corrente
            for (int j = 0; j < getClassFamilies().get(i).size(); j++) {
                /* Fazer com que o Termo calcule o seu peso através da sua
                 * profundidade na hierarquia da família de classes contrastada
                 * com a máxima profundidade, aqui passada por parâmetro
                 */
                getClassFamilies().get(i).get(j).CalculateWeight(maxLevel);
                /* Adicionar o peso do termo recém calculado à sua respectiva
                 * posição nas listas de pesos armazenadas em wicValues
                 */
                getWicValues().getLast().add(getClassFamilies().get(i).get(j).getTermWeight());
            }
        }
    }

    private void ExploreObjectProperties() {
        LinkedList<OWLObjectProperty> obtainedObjProp = new LinkedList<OWLObjectProperty>();
        //Obter a lista de Propriedades de Objeto definidas pelo criador da ontologia
        Collection<OWLObjectProperty> list_PropObj = getOwlModel().getUserDefinedOWLObjectProperties();
        Iterator<OWLObjectProperty> it_ObjProp = list_PropObj.iterator();
        //Para todas as propriedades de objeto existentes nesta listagem
        while (it_ObjProp.hasNext()) {
            //Obter a Propriedade de objeto corrente na iteração
            OWLObjectProperty objProp = it_ObjProp.next();
            /* Para evitar duplicações semânticas, ignorar as propriedades inversas
             * (no entanto a propriedade inversa será citada na sua propriedade original correspondente)
             */
            if (objProp.getInverseProperty() == null || !obtainedObjProp.contains(objProp.getInverseProperty())) {
                //Obter a lista de famílias de classe pertencentes ao domínio através do qual a  propriedade se aplica
                LinkedList<Integer> domainFamilyClasses = new LinkedList<Integer>();
                RDFSClass propertyDomain = objProp.getDomain(false);
                ObtainFamilyClassesInvolved(propertyDomain, domainFamilyClasses);
                //Obter a lista de famílias de classe pertencentes à extensão sobre a qual a propriedade se aplica
                LinkedList<Integer> rangeFamilyClasses = new LinkedList<Integer>();
                RDFResource propertyRange = objProp.getRange(false);
                ObtainFamilyClassesInvolved((RDFSClass) propertyRange, rangeFamilyClasses);
                //Caso exista ao menos uma família de classe válida para domínio e extensão da propriedade
                if (domainFamilyClasses.size() > 0 && rangeFamilyClasses.size() > 0) {
                    //adicionar a nova propriedade à lista da OntologyExtractor
                    getClassFamiliesProperties().add(new Property(objProp, domainFamilyClasses, rangeFamilyClasses));
                    //adicionar a lista de escopos de propriedade local da propriedade à lista geral da ontologia
                    getClassFamiliesPropertiesScopes().addAll(getClassFamiliesProperties().getLast().GetPropertyScopeList());
                    //marcar propriedade como já obtida
                    obtainedObjProp.add(objProp);
                }
            }
        }
    }

    private void ObtainFamilyClassesInvolved(RDFSClass rdfsClass, List<Integer> familyClassesInvolved) {
        //Inicializar o iterador de operandos (de uma união ou intersecção de classes) como nulo
        Iterator<RDFSClass> it_groupOperands = null;

        /* Caso o elemento envolvido seja uma NamedClass ou um Individual, adicionar a família de 
         * classe a qual o elemento pertence (se tiver sido inserido na ontologia pelo usuário)
         */
        if (rdfsClass instanceof OWLNamedClass
                || rdfsClass instanceof OWLIndividual) {
            /* Descobrir a(s) família(s) de classes a qual o elemento atual pertence e incluir
             * (sem repetição) na lista de famílias de classes envolvidas
             */
            DiscoverResourceFamilyClass((RDFResource) rdfsClass, familyClassesInvolved);
        } /* Caso o elemento envolvido seja uma união de outros elementos, então instanciar
         * o iterador para depois percorrer os integrantes dessa união
         */ else if (rdfsClass instanceof OWLUnionClass) {
            it_groupOperands = (Iterator<RDFSClass>) ((OWLUnionClass) rdfsClass).getOperands().iterator();
        } /* Caso o elemento envolvido seja uma intersecção de outros elementos, então instanciar
         * o iterador para depois percorrer os integrantes dessa intersecção
         */ else if (rdfsClass instanceof OWLIntersectionClass) {
            it_groupOperands = (Iterator<RDFSClass>) ((OWLIntersectionClass) rdfsClass).getOperands().iterator();
        }
        /* Se o iterador de operandos não for nulo, significa que foi obtido um iterador de um grupo
         * de outros elementos (união e intersecção serão aqui tratados da mesma maneira). Aprofundar
         * a análise para cada sub-elemento deste grupo
         */
        if (it_groupOperands != null) {
            // Para todos os elementos participantes do grupo
            while (it_groupOperands.hasNext()) {
                //Chamar recursivamente este método
                ObtainFamilyClassesInvolved(it_groupOperands.next(), familyClassesInvolved);
            }
        }
    }

    private void DiscoverResourceFamilyClass(RDFResource rdfResource, List<Integer> familyClassesInvolved) {
        //Percorrer todas as famílias de classe existentes na ontologia atual
        for (Integer fc = 0; fc < getClassFamiliesRdfResources().size(); fc++) {
            //Se o elemento identificado está presente na família de classe fc
            if (getClassFamiliesRdfResources().get(fc).contains(rdfResource)) {
                // Se a família de classe fc já não está presente na listagem de famílias envolvidas
                if (!classFamiliesRdfResources.get(fc).contains(fc)) {
                    //Inserir o índice da família de classe fc na listagem de famílias envolvidas
                    familyClassesInvolved.add(fc);
                }
            }
        }
    }

    public int Get_NumberOfClassFamilies() {
        if (getClassFamilies() != null) {
            return getClassFamilies().size();
        } else {
            return 0;
        }
    }

    /**
     * Na recuperação de informação existe um valor máximo que pode-se obter para
     * a soma total das similaridades obtidas. Este valor máximo será utilizado para
     * normalizar o índice de ranking dos documentos para valores entre 0 e 1.
     * @return Máxima pontuação possível ao se somar o máximo de pontuação que pode-se
     * obter em cada uma das similaridades correspondentes às famílias de classes e 
     * respectiva propriedades de objeto presentes.
     */
    public Double Get_MaximumRankValuePossible() {
        MathExpressionProcessor rank_formula_processor = ConfigurationManager.Instance().Get_Formula_rank_Processor();

        rank_formula_processor.SetVariable(ConstDeclarations.rankFc, (getClassFamilies() != null ? getClassFamilies().size() : new Double(0)));
        rank_formula_processor.SetVariable(ConstDeclarations.rankProp, (getClassFamiliesPropertiesScopes() != null ? getClassFamiliesPropertiesScopes().size() : new Double(0)));

        return rank_formula_processor.ResolveExpression();
    }

    public LinkedList<Term> Get_ClassFamily(int class_family_index) {
        return getClassFamilies().get(class_family_index);
    }

    public String Get_ClassFamilyTopMostTermName(int class_family_index) {
        return getClassFamilies().get(class_family_index).get(0).main_keyword;
    }

    /**
     * Obtém a lista de listas (uma para cada família de classe) dos valores 
     * wic (pesos dos termos em cada família de classes) calculados para a
     * Ontologia atual.
     * @return Lista de listas de valores wic, cujo posicionamento acompanha a
     * ordem de todas as demais listas relativas a famílias de classes e atributos
     * para os termos do sistema.
     */
    public LinkedList<LinkedList<Double>> Get_OntologyTermWeights() {
        return this.getWicValues();
    }

    public LinkedList<PropertyScope> Get_ClassFamiliesPropertyScopeList() {
        return getClassFamiliesPropertiesScopes();
    }

    public int Get_NumberOfPropertiesScopes() {
        if (getClassFamiliesPropertiesScopes() != null) {
            return getClassFamiliesPropertiesScopes().size();
        } else {
            return 0;
        }
    }

    public DefaultTreeModel BuildTreeModel() {
        DefaultTreeModel treeModel;
        DefaultMutableTreeNode ontologyRoot;

        ontologyRoot = CustomIconsTreeCellEntry.BuildNewNode("ONTOLOGY", getOwl_file().getName());

        Iterator<LinkedList<Term>> it_classFamily = getClassFamilies().iterator();
        while (it_classFamily.hasNext()) {
            DefaultMutableTreeNode familyClassRoot;
            LinkedList<Term> actualFamilyClass = it_classFamily.next();
            familyClassRoot = CustomIconsTreeCellEntry.BuildNewNode("FAMILY_CLASS", "<html>Família de Classes <font color=\"#AA0000\">" + actualFamilyClass.get(0).GetMainTerm() + "</font> (Profundidade Máx: " + actualFamilyClass.get(0).getFamilyClassMaxLevel() + ")");

            Iterator<Term> it_Terms = actualFamilyClass.iterator();
            while (it_Terms.hasNext()) {
                familyClassRoot.add(it_Terms.next().BuildTreeNode());
            }
            ontologyRoot.add(familyClassRoot);
        }

        Iterator<Property> it_Property = getClassFamiliesProperties().iterator();
        while (it_Property.hasNext()) {
            ontologyRoot.add(it_Property.next().BuildTreeNode());
        }

        treeModel = new DefaultTreeModel(ontologyRoot);
        return treeModel;
    }

    
    public void getNames() {
        for (int i = 0; i < this.getClassFamilies().size(); i++) {
            LinkedList<Term> termos = getClassFamilies().get(i);
            for (int j = 0; j < termos.size(); j++) {
                String name = termos.get(j).GetMainTerm();
                getListNames().add(name);
                // pegar sinonimos, pegar verbos
                LinkedList<String> sinonimos = termos.get(j).synonymns;
                for (int k = 0; k < sinonimos.size(); k++) {
                    name = sinonimos.get(k);
                    getListNames().add(name);
                }

                LinkedList<String> verbos = termos.get(j).verbs;
                for (int k = 0; k < verbos.size(); k++) {
                    name = verbos.get(k);
                    getListNames().add(name);
                }
            }
        }
    }
    
     public LinkedList<String> getAllWords() {
        LinkedList listaPalavrasOntologia = new LinkedList();
        for (int i = 0; i < this.getClassFamilies().size(); i++) {
            LinkedList<Term> termos = getClassFamilies().get(i);
            for (int j = 0; j < termos.size(); j++) {
                String name = termos.get(j).GetMainTerm();
                listaPalavrasOntologia.add(name);
                // pegar sinonimos, pegar verbos
                LinkedList<String> sinonimos = termos.get(j).synonymns;
                for (int k = 0; k < sinonimos.size(); k++) {
                    name = sinonimos.get(k);
                    listaPalavrasOntologia.add(name);
                }

                LinkedList<String> verbos = termos.get(j).verbs;
                for (int k = 0; k < verbos.size(); k++) {
                    name = verbos.get(k);
                    listaPalavrasOntologia.add(name);
                }
            }
        }
        return listaPalavrasOntologia;
    }

    /**
     * @return the owl_file
     */
    public File getOwl_file() {
        return owl_file;
    }

    /**
     * @param owl_file the owl_file to set
     */
    public void setOwl_file(File owl_file) {
        this.owl_file = owl_file;
    }

    /**
     * @return the owlModel
     */
    public OWLModel getOwlModel() {
        return owlModel;
    }

    /**
     * @param owlModel the owlModel to set
     */
    public void setOwlModel(OWLModel owlModel) {
        this.owlModel = owlModel;
    }

    /**
     * @return the classFamilies
     */
    public LinkedList<LinkedList<Term>> getClassFamilies() {
        return classFamilies;
    }

    /**
     * @param classFamilies the classFamilies to set
     */
    public void setClassFamilies(LinkedList<LinkedList<Term>> classFamilies) {
        this.classFamilies = classFamilies;
    }

    /**
     * @return the classFamiliesRdfResources
     */
    public LinkedList<LinkedList<RDFResource>> getClassFamiliesRdfResources() {
        return classFamiliesRdfResources;
    }

    /**
     * @param classFamiliesRdfResources the classFamiliesRdfResources to set
     */
    public void setClassFamiliesRdfResources(LinkedList<LinkedList<RDFResource>> classFamiliesRdfResources) {
        this.classFamiliesRdfResources = classFamiliesRdfResources;
    }

    /**
     * @return the wicValues
     */
    public LinkedList<LinkedList<Double>> getWicValues() {
        return wicValues;
    }

    /**
     * @param wicValues the wicValues to set
     */
    public void setWicValues(LinkedList<LinkedList<Double>> wicValues) {
        this.wicValues = wicValues;
    }

    /**
     * @return the classFamiliesProperties
     */
    public LinkedList<Property> getClassFamiliesProperties() {
        return classFamiliesProperties;
    }

    /**
     * @param classFamiliesProperties the classFamiliesProperties to set
     */
    public void setClassFamiliesProperties(LinkedList<Property> classFamiliesProperties) {
        this.classFamiliesProperties = classFamiliesProperties;
    }

    /**
     * @return the classFamiliesPropertiesScopes
     */
    public LinkedList<PropertyScope> getClassFamiliesPropertiesScopes() {
        return classFamiliesPropertiesScopes;
    }

    /**
     * @param classFamiliesPropertiesScopes the classFamiliesPropertiesScopes to set
     */
    public void setClassFamiliesPropertiesScopes(LinkedList<PropertyScope> classFamiliesPropertiesScopes) {
        this.classFamiliesPropertiesScopes = classFamiliesPropertiesScopes;
    }

    /**
     * @return the listNames
     */
    public LinkedList<String> getListNames() {
        return this.listNames;
    }

    /**
     * @param listNames the listNames to set
     */
    public void setListNames(LinkedList<String> listNames) {
        this.listNames = listNames;
    }

    /**
     * @return the jena
     */
    public JenaOWLModel getJena() {
        return jena;
    }

    /**
     * @param jena the jena to set
     */
    public void setJena(JenaOWLModel jena) {
        this.jena = jena;
    }
}
