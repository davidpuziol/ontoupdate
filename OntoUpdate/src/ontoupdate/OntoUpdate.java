/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import ontoupdate.agentModule.graficInterface.AgentForm;
import ontoupdate.resources.ResourceManager;
import ontoupdate.util.notifications.QuickMessageDisplay;
/**
 *
 * @author David
 */
public class OntoUpdate {

 public static void main(String[] args) {
        // TODO code application logic here
        ResourceManager.Inicializar();

        //Configurar Logger do Protégé para apenas exibir se ouver alguma falha grave
        edu.stanford.smi.protege.util.Log.getLogger().setLevel(java.util.logging.Level.SEVERE);

        //Inicializar Mensagens Rápidas a serem utilizadas pelo sistema
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_INFORMATION, new Color(215, 247, 251), new Color(20, 1, 107), new Color(20, 1, 107), ResourceManager.GetImageIcon("IMG_QUICK_MSG_INFO"), new Font("Arial", Font.BOLD, 14));
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_SUCCESS, new Color(202, 255, 210), new Color(1, 90, 5), new Color(1, 90, 5), ResourceManager.GetImageIcon("IMG_QUICK_MSG_OK"), new Font("Arial", Font.BOLD, 14));
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_ERROR, new Color(254, 218, 219), new Color(111, 2, 5), new Color(111, 2, 5), ResourceManager.GetImageIcon("IMG_QUICK_MSG_ERROR"), new Font("Arial", Font.BOLD, 14));
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_WARNING, new Color(255, 236, 217), new Color(125, 72, 0), new Color(125, 72, 0), ResourceManager.GetImageIcon("IMG_QUICK_MSG_WARNING"), new Font("Arial", Font.BOLD, 14));
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_PROCESS, new Color(213, 213, 234), new Color(41, 41, 84), new Color(41, 41, 84), ResourceManager.GetImageIcon("IMG_QUICK_MSG_PROCESS"), new Font("Arial", Font.BOLD, 14));
        QuickMessageDisplay.Instance().CreateMessageSet(QuickMessageDisplay.MSG_QUESTION, new Color(215, 247, 251), new Color(20, 1, 107), new Color(20, 1, 107), ResourceManager.GetImageIcon("IMG_QUICK_MSG_QUESTION"), new Font("Arial", Font.BOLD, 14));

        ShowAgentForm();
    }

    public static void ShowAgentForm() {
        //Exibir tela Inicial (Configuração)
        AgentForm agenteForm = AgentForm.Instance();
        agenteForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        agenteForm.setVisible(true);
        agenteForm.requestFocus();
    }

}
