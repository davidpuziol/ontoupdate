 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.extractionModule;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author David Puziol Prata
 */
public class Concept {

    private LinkedList<Concept> listOfConceptChild;
    private LinkedList<Description> listOfTerms;
    private LinkedList<Description> listOfAllTerms;
    private LinkedList<Cluster> listOfCluster;
    private LinkedList<Document> listaOfDocRelatives;
    private String nameOfConcept;
    private Concept pai;

    public Concept(String name, Document document, Double minBeDescription) {
        nameOfConcept = name; // conceito é o root
        prepareConcept(document, minBeDescription); // chama preparador

    }

    public Concept(String name) {
        this.nameOfConcept = name;
        this.listOfCluster = new LinkedList();
        this.listOfConceptChild = new LinkedList();
        this.listOfTerms = new LinkedList();
        this.listaOfDocRelatives = new LinkedList();
        this.pai = null;
    }

    public Concept(Document document, Double minBeDescription) {
        nameOfConcept = null; // qualquer conceito que nao eh root é nulo como nome
        pai = null;
        prepareConcept(document, minBeDescription); // chama preparador
    }

    public Concept(String name, LinkedList<Cluster> clusters, Double minBeDescription, Concept pai) {
        this.nameOfConcept = name;
        this.pai = pai;
        prepareConcept(clusters, minBeDescription);
    }

    private void prepareConcept(LinkedList<Cluster> clusterVector, Double minBeDescription) {
        setVetorConceptsChild(new LinkedList());
        setListOfTerms(new LinkedList());
        setVetorListOfCluster(new LinkedList());
        this.listaOfDocRelatives = new LinkedList();
        conceptFormation(clusterVector, minBeDescription);
    }

    private void prepareConcept(Document document, Double minBeDescription) {
        setVetorConceptsChild((LinkedList<Concept>) new LinkedList());
        setListOfTerms((LinkedList<Description>) new LinkedList());
        setVetorListOfCluster((LinkedList<Cluster>) new LinkedList());
        this.listaOfDocRelatives = new LinkedList();
        setDescription(document, minBeDescription);
    }

    public boolean hasChild() {

        if (getVetorConceptsChild().isEmpty()) {
            System.out.println(this.getNameOfConcept() + " NAO TEM FILHOS");
            return false;
        } else {
            System.out.println(this.getNameOfConcept() + " TEM FILHOS");
            return true;
        }
    }

    public boolean hasClassifyInConcept(Double minClassify, Document doc) {

        Iterator iterador = getVetorConceptsChild().iterator();
        if (this.getListOfTerms() != null && !this.getListOfTerms().isEmpty()) {
            //comparar os termos do conceito filho com o do documento
            Double valor = classify(doc.getListOfWords(), this.getListOfTerms());
            System.out.println("\nINDICE DE CLASSIFICAÇÃO DO DOCUMENTO " + doc.getPath() + "NO CONCEITO " + this.getNameOfConcept() + " = " + valor);
            if (valor >= minClassify) // passa o a lista de termos dos dois para comparacao
            {

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean hasClassifyInChildren(Double minClassify, Document doc) {

        Iterator iterador = getVetorConceptsChild().iterator();
        while (iterador.hasNext()) {
            Concept child = (Concept) iterador.next();
            //comparar os termos do conceito filho com o do documento
            Double classify = classify(doc.getListOfWords(), child.getListOfTerms());
            System.out.println("INDICE DE CLASSIFICAÇÃO DO DOCUMENTO " + doc.getPath() + "NO CONCEITO " + this.getNameOfConcept() + "=" + classify);
            if (classify >= minClassify) // passa o a lista de termos dos dois para comparacao
            {
                System.out.println(this.getNameOfConcept() + " TEM PELO MENOS 1 CLASSIFICADOR VALIDO PARA O DOCUMENTO " + doc.getPath());
                return true;
            }
        }
        System.out.println(this.getNameOfConcept() + " NAO TEM CLASSIFICADOR VALIDO PARA O DOCUMENTO " + doc.getPath());
        return false;
    }

    private Double classify(LinkedList<DocumentWord> documentWords, LinkedList<Description> descriptionTerms) {
        // cria o vetor somente com as palavras que possuem em comum nos dois vetores de palavras do document
        LinkedList<DocumentWord> documentWordAux = new LinkedList();
        LinkedList<Description> descriptionTermdAux = new LinkedList();
        for (int i = 0; i < descriptionTerms.size(); i++) {
            //boolean flag = false;
            for (int j = 0; j < documentWords.size(); j++) {
                if (descriptionTerms.get(i).getWord().equals(documentWords.get(j).getWord())) {
                    descriptionTermdAux.add(descriptionTerms.get(i));
                    documentWordAux.add(documentWords.get(j));
                    break;
                    // somente tem 1 palavra em cada vetor do mesmo tipo entao so entrara uma vez aqui e os vetore aux iram ter a mesma sincronia
                }
            }
        }
        return calculateSunAll(documentWordAux, descriptionTermdAux);
    }

    private Double calculateSunAll(LinkedList<DocumentWord> documentWeights, LinkedList<Description> queryWeights) {
        //Obter os iteradores para percorrer os valores wij e wiq dos termos
        Iterator<DocumentWord> it_wij = documentWeights.iterator();
        Iterator<Description> it_wiq = queryWeights.iterator();
        //Inicializar todas as somatórias a serem realizadas
        Double sum_wij_x_wiq = new Double(0);
        //Realizar as somatórias necessárias
        while (it_wij.hasNext()) {
            Double wij_value = it_wij.next().getWeith(); // pega o peso da palavra do documento
            Double wiq_value = it_wiq.next().getWeigth(); // pega o peso do termo do conceito

            sum_wij_x_wiq += wij_value * wiq_value; // soma a a multiplicacao de ambos
        }
        return sum_wij_x_wiq; // retorna o valor do clasificador
    }

    public Concept getChildWithHigherClassify(Document document, Double minClassify) {
        // essa funcao retorna o filho de melhor classificador que será maior do que a relevancia
        // uma vez que foi checado antes a relevancia dos filhos para ser maior

        Iterator iterador = getVetorConceptsChild().iterator();
        Double betterClassify = 0.0;
        Concept child = null;
        System.out.println("PROCURANDO SE O DOCUMENTO SE ENCAIXA EM ALGUM FILHO DO " + this.getNameOfConcept());
        while (iterador.hasNext()) {
            Concept concept = (Concept) iterador.next();
            Double numberClassify = classify(document.getListOfWords(), concept.getListOfTerms());

            if (numberClassify > betterClassify) {
                child = concept;
                betterClassify = numberClassify;
                System.out.println(this.getNameOfConcept() + "TEM CLASSIFICADO PARA O DOCUMENTO " + document.getPath() + " NO FILHO " + child.getNameOfConcept() + " COM VALOR DE CLASSIFY =" + betterClassify);

            }
        }
        try {
            if (betterClassify >= minClassify) {
                System.out.println(this.getNameOfConcept() + "TEM MELHOR CLASSIFICADO PARA O DOCUMENTO " + document.getPath() + " PARA O FILHO " + child.getNameOfConcept() + " COM VALOR DE CLASSIFY =" + betterClassify);
                return child;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public Cluster KnnChooseClusterToDocument(Document document, Double minClustering) {
        // todo conceito deve ter pelo menos um cluster assim que for criado tem um documento associado a um dos seus clusters ou pelo menos 1 cluster
        System.out.println("NO CONCEITO " + this.getNameOfConcept() + " IRA PROCURAR UM CLUSTER PARA O DOCUMENTO " + document.getPath());
        int[] numberOfSimilarities;

        if (!listOfCluster.isEmpty()) { // tem pelo menos um documento no cluster
            numberOfSimilarities = new int[getListOfCluster().size()];

            for (int i = 0; i < getListOfCluster().size(); i++) {
                //pegando as descricoes que serão eliminadas
                LinkedList<Description> allDesc = new LinkedList();
                allDesc = relativeDescription(this, allDesc);
                System.out.println("\n\nDESCRICOES RELATIVAS ");
                Iterator it = allDesc.iterator();
                while (it.hasNext()) {
                    Description desc = (Description) it.next();
                    System.out.println(desc.getWord());
                }

                numberOfSimilarities[i] = getListOfCluster().get(i).getNumberOfDocumentSimilar(document, allDesc, minClustering);
                System.out.println("SIMILARIDADE COM OS DOCUMENTOS DO CLUSTER " + i + " COM  " + numberOfSimilarities[i] + " DOCUMENTOS SIMILARES");
            }
            //array position a partir de agora nao sera mais usado pela codigo anterior e sera reaproveitado para guardar a posicao do array cujo vetor tem a maior similaridade
            return getArrayPositionWithHightSimilarityOrNew(numberOfSimilarities, minClustering); // retorna o cluster de melhor similaridade
        } else {
            Cluster cluster = new Cluster(); // cria um cluster e adiciona
            getListOfCluster().add(cluster);
            System.out.println("INCLUINDO DOCUMENTO EM UM CLUSTER NOVO POIS A LISTA ESTA VAZIA");
            return cluster;
        }
    }

    private Cluster getArrayPositionWithHightSimilarityOrNew(int[] numberOfSimilarities, Double minClustering) {
        int index = 0;
        int betterSimilarity = 0;
        boolean flag = false;
        // percorrendo o vetor de similaridades para ver se tiver uma maior que zero
        for (int i = 0; i < numberOfSimilarities.length; i++) {
            if (numberOfSimilarities[i] > 0) {
                flag = true;
            }
        }

        // neste ponto ja se sabe que ira criar um cluster ou nao
        if (flag == false) { // caso ninguem tenha nenhum documento similar
            System.out.println("NAO EXISTE CLUSTER SIMILAR FILHO DO CONCEITO " + this.getNameOfConcept());
            System.out.println("CRIANDO NOVO CLUSTER NO CONCEITO " + this.getNameOfConcept());
            Cluster cluster = new Cluster(); // cria um cluster e adiciona
            getListOfCluster().add(cluster);
            return cluster; // retorna o cluster
        } else {// sabendo que tem documento similar
            for (int i = 0; i < numberOfSimilarities.length; i++) {
                if (numberOfSimilarities[i] == betterSimilarity) {// checar quem tem menor numero de documentos
                    if (getListOfCluster().get(i).getListOfDocument().size() < getListOfCluster().get(index).getListOfDocument().size()) { // se o numero de documentos no cluster for menor do que o anterior, ele passa
                        index = i;
                    }
                } else {// eh diferente e so muda o index se for maior
                    if (numberOfSimilarities[i] > betterSimilarity) {
                        betterSimilarity = numberOfSimilarities[i];
                        index = i;
                    }
                }
            }
            System.out.println("SERA ADICIONADO NO CLUSTER " + index);
            return getListOfCluster().get(index);
        }
    }

    private void setDescription(Document document, Double minBeDescription) {
        Iterator iterador = document.getListOfWords().iterator();
        while (iterador.hasNext()) {
            DocumentWord word = (DocumentWord) iterador.next();
            if (word.getWeith() >= minBeDescription) {
                Description description = new Description(word.getWord(), word.getWeith());
                this.listOfTerms.add(description);
            }
        }
    }

    public void RemakeDescription(Double minBeDescription) {
        //cria a lista para guardar as descricoes
        this.listOfTerms = new LinkedList<Description>();
        LinkedList<Description> descriptionList = new LinkedList();
        Iterator allDocsIt = listaOfDocRelatives.iterator();
        while (allDocsIt.hasNext()) {
            Document adoc = (Document) allDocsIt.next();
            Iterator iteradorDocWord = adoc.getListOfWords().iterator();
            // criar descricao nula para ser preenchida
            while (iteradorDocWord.hasNext()) {
                // comparar as palavras
                //pegar uma pelavra // obs: um documento nao pode existir sm uma palavra pois nao passaria pela relevancia
                DocumentWord word = (DocumentWord) iteradorDocWord.next();
                // preparar para rodar o vetor de descricoes criando o iterador
                // definir peso igual ao peso da palavra que estou iterando caso nao tenha nenhuma outra palavra o peso ja esta definido
                // flag ja dizendo que a palavra nao existe
                boolean flag = false; // saber se term pra adicionar ou nao
                //if (descriptionList.size() > 0) {
                Iterator iteradorVectorDescription = descriptionList.iterator();
                while (iteradorVectorDescription.hasNext()) { // olhar se a palavra tem no vetor de descricoes
                    Description descriptionAux = (Description) iteradorVectorDescription.next();

                    if (word.getWord().equals(descriptionAux.getWord())) { // se a palavra for igual adicionar o peso e setar flag igual a true
                        flag = true;
                        //Double weight = descriptionAux.getWeigth() + word.getWeith();
                        descriptionAux.centroidByIncrementHit(word.getWeith());
                    }
                }
                if (flag == false) {// se nao tiver a descricao inclui ela no vetor
                    Description description = new Description(word.getWord(), word.getWeith());
                    descriptionList.add(description);// poe o peso
                }
            }
        }
        // DELETAR TODA A LISTA DE TERMOS DO CONCEITO PARA REFORMULDAR  UMA NOVA

        Iterator iteradorDescriptions = descriptionList.iterator();

        while (iteradorDescriptions.hasNext()) {
            Description descriptionAux2 = (Description) iteradorDescriptions.next();
            if (descriptionAux2.getNumHits() / listaOfDocRelatives.size() >= minBeDescription) {
                this.listOfTerms.add(descriptionAux2);
            }
        }

         //RETIRAR AS DESCRICOES DOS PAIS
        LinkedList<Description> descRelatives = new LinkedList();
        descRelatives = this.relativeDescription(pai, descRelatives);
        Iterator iteratorRelatives = descRelatives.iterator();
        if (!this.getNameOfConcept().equals("root")) {
            while (iteratorRelatives.hasNext()) {
                Description descRel = (Description) iteratorRelatives.next();
                Iterator it = this.getListOfTerms().iterator();
                while (it.hasNext()) {
                    Description desc = (Description) it.next();
                    if (desc.getWord().equals(descRel.getWord())) {
                        this.getListOfTerms().remove(desc);
                        break;
                    }
                }
            }
        }
        // OrdenarDescription();
        CreatePosibleDescriptionsUpdate();
    }

    public void updateDescription(Document doc, Double minBeDescription) {
        //cria a lista para guardar as descricoes
        this.listOfTerms = new LinkedList<Description>();
        LinkedList<Description> descriptionList = new LinkedList();
        if (!this.listaOfDocRelatives.contains(doc)) {
            this.listaOfDocRelatives.add(doc);
            Iterator allDocsIt = listaOfDocRelatives.iterator();
            while (allDocsIt.hasNext()) {
                Document adoc = (Document) allDocsIt.next();
                Iterator iteradorDocWord = adoc.getListOfWords().iterator();
                // criar descricao nula para ser preenchida
                while (iteradorDocWord.hasNext()) {
                    // comparar as palavras
                    //pegar uma pelavra // obs: um documento nao pode existir sm uma palavra pois nao passaria pela relevancia
                    DocumentWord word = (DocumentWord) iteradorDocWord.next();
                    // preparar para rodar o vetor de descricoes criando o iterador
                    // definir peso igual ao peso da palavra que estou iterando caso nao tenha nenhuma outra palavra o peso ja esta definido
                    // flag ja dizendo que a palavra nao existe
                    boolean flag = false; // saber se term pra adicionar ou nao
                    //if (descriptionList.size() > 0) {
                    Iterator iteradorVectorDescription = descriptionList.iterator();
                    while (iteradorVectorDescription.hasNext()) { // olhar se a palavra tem no vetor de descricoes
                        Description descriptionAux = (Description) iteradorVectorDescription.next();

                        if (word.getWord().equals(descriptionAux.getWord())) { // se a palavra for igual adicionar o peso e setar flag igual a true
                            flag = true;
                            //Double weight = descriptionAux.getWeigth() + word.getWeith();
                            descriptionAux.centroidByIncrementHit(word.getWeith());
                        }
                    }
                    if (flag == false) {// se nao tiver a descricao inclui ela no vetor
                        Description description = new Description(word.getWord(), word.getWeith());
                        descriptionList.add(description);// poe o peso
                    }
                }
            }
            // DELETAR TODA A LISTA DE TERMOS DO CONCEITO PARA REFORMULDAR  UMA NOVA

            Iterator iteradorDescriptions = descriptionList.iterator();


            while (iteradorDescriptions.hasNext()) {
                Description descriptionAux2 = (Description) iteradorDescriptions.next();
                if (descriptionAux2.getNumHits() / listaOfDocRelatives.size() >= minBeDescription) {
                    this.listOfTerms.add(descriptionAux2);
                }
            }

          //RETIRAR AS DESCRICOES DOS PAIS
        LinkedList<Description> descRelatives = new LinkedList();
        descRelatives = this.relativeDescription(pai, descRelatives);
        Iterator iteratorRelatives = descRelatives.iterator();
        if (!this.getNameOfConcept().equals("root")) {
            while (iteratorRelatives.hasNext()) {
                Description descRel = (Description) iteratorRelatives.next();
                Iterator it = this.getListOfTerms().iterator();
                while (it.hasNext()) {
                    Description desc = (Description) it.next();
                    if (desc.getWord().equals(descRel.getWord())) {
                        this.getListOfTerms().remove(desc);
                        break;
                    }
                }
            }
        }

            //OrdenarDescription();
            CreatePosibleDescriptionsUpdate();
        }
    }

    public void conceptFormation(LinkedList<Cluster> clusterVector, Double minBeDescription) { // somente entrar nessa funcao se tiver cluster e documento
        // cria a lista para guardar a descricoes
        LinkedList<Description> descriptionList = new LinkedList();
        Double sumDocuments = new Double(0);
        Iterator iteradorClusterVector = clusterVector.iterator();
        while (iteradorClusterVector.hasNext()) {
            Cluster cluster = (Cluster) iteradorClusterVector.next();
            sumDocuments += cluster.getListOfDocument().size();
            Iterator iteradorDocument = cluster.getListOfDocument().iterator();
            while (iteradorDocument.hasNext()) {
                // pegando o documento do cluster
                Document doc = (Document) iteradorDocument.next();
                this.listaOfDocRelatives.add(doc);
                // preparar o iterador da lista de documento do documento
                Iterator iteradorDocWord = doc.getListOfWords().iterator();
                // criar descricao nula para ser preenchida
                while (iteradorDocWord.hasNext()) {
                    // comparar as palavras
                    //pegar uma pelavra // obs: um documento nao pode existir sm uma palavra pois nao passaria pela relevancia
                    DocumentWord word = (DocumentWord) iteradorDocWord.next();
                    // preparar para rodar o vetor de descricoes criando o iterador                   
                    // definir peso igual ao peso da palavra que estou iterando caso nao tenha nenhuma outra palavra o peso ja esta definido
                    // flag ja dizendo que a palavra nao existe
                    boolean flag = false; // saber se tem term pra adicionar ou nao
                    //if (descriptionList.size() > 0) {
                    Iterator iteradorVectorDescription = descriptionList.iterator();
                    while (iteradorVectorDescription.hasNext()) { // olhar se a palavra tem no vetor de descricoes
                        Description descriptionAux = (Description) iteradorVectorDescription.next();

                        if (word.getWord().equals(descriptionAux.getWord())) { // se a palavra for igual adicionar o peso e setar flag igual a true
                            flag = true;
                            //Double weight = descriptionAux.getWeigth() + word.getWeith();
                            descriptionAux.centroidByIncrementHit(word.getWeith());
                        }
                    }
                    if (flag == false) {// se nao tiver a descricao inclui ela no vetor
                        Description description = new Description(word.getWord(), word.getWeith());
                        descriptionList.add(description);// poe o peso
                    }
                }
            }
        }


        this.listOfTerms = new LinkedList<Description>();
        
        // fazer o loop para definir quem será ou nao descricao
        Iterator iteradorDescriptions = descriptionList.iterator();
        // DELETAR TODA A LISTA DE TERMOS DO CONCEITO PARA REFORMULDAR  UMA NOVA
        while (iteradorDescriptions.hasNext()) {
            Description descriptionAux2 = (Description) iteradorDescriptions.next();
            // numero de hits de acordo com quantos documentos que foram encontrados
            if (descriptionAux2.getNumHits() / listaOfDocRelatives.size() >= minBeDescription) {
                this.listOfTerms.add(descriptionAux2);
            }
        }
        //RETIRAR AS DESCRICOES DOS PAIS
        LinkedList<Description> descRelatives = new LinkedList();
        descRelatives = this.relativeDescription(pai, descRelatives);
        Iterator iteratorRelatives = descRelatives.iterator();
        if (!this.getNameOfConcept().equals("root")) {
            while (iteratorRelatives.hasNext()) {
                Description descRel = (Description) iteratorRelatives.next();
                Iterator ite = this.getListOfTerms().iterator();
                while (ite.hasNext()) {
                    Description desc = (Description) ite.next();
                    if (desc.getWord().equals(descRel.getWord())) {
                        this.getListOfTerms().remove(desc);
                        break;
                    }
                }
            }
        }
        //OrdenarDescription();
        // CreatePosibleDescriptionsConceptFormation(clusterVector);
    }

    public void CreatePosibleDescriptionsUpdate() {
        //Iterator iteradorClusters = this.listOfCluster.iterator();
        Iterator iteradorDocuments = this.listaOfDocRelatives.iterator();
        while (iteradorDocuments.hasNext()) {
            Document document = (Document) iteradorDocuments.next();
            Iterator iteradorDescriptons = this.listOfTerms.iterator();
            while (iteradorDescriptons.hasNext()) {
                LinkedList<String> listaGeral = new LinkedList();
                Description description = (Description) iteradorDescriptons.next();
                LinkedList lista = new LinkedList(document.getWordsSimilars(description.getWord()));

                Iterator listaIterador = lista.iterator();
                while (listaIterador.hasNext()) {
                    String word = (String) listaIterador.next();

                    Iterator geralIterador = listaGeral.iterator();
                    boolean flag = false;
                    while (geralIterador.hasNext()) {
                        String desc = (String) geralIterador.next();
                        if (desc.equals(word)) {
                            flag = true;
                        }
                    }
                    if (flag == false) {
                        listaGeral.add(word);
                    }
                }
                description.includeSimilarWords(listaGeral);
            }
        }
    }

    public void CreatePosibleDescriptionsConceptFormation(LinkedList<Cluster> listaCluster) {
        Iterator itcluster = listaCluster.iterator();
        while (itcluster.hasNext()) {
            Cluster cluster = (Cluster) itcluster.next();
            Iterator iteradorDocuments = cluster.getListOfDocument().iterator();
            while (iteradorDocuments.hasNext()) {
                Document document = (Document) iteradorDocuments.next();
                Iterator iteradorDescriptons = this.listOfTerms.iterator();
                while (iteradorDescriptons.hasNext()) {
                    LinkedList<String> listaGeral = new LinkedList();
                    Description description = (Description) iteradorDescriptons.next();
                    LinkedList lista = new LinkedList(document.getWordsSimilars(description.getWord()));

                    Iterator listaIterador = lista.iterator();
                    while (listaIterador.hasNext()) {
                        String word = (String) listaIterador.next();

                        Iterator geralIterador = listaGeral.iterator();
                        boolean flag = false;
                        while (geralIterador.hasNext()) {
                            String desc = (String) geralIterador.next();
                            if (desc.equals(word)) {
                                flag = true;
                            }
                        }
                        if (flag == false) {
                            listaGeral.add(word);
                        }
                    }
                    description.includeSimilarWords(listaGeral);
                }
            }
        }
    }

    public LinkedList<Document> getAllDocument() {
        LinkedList<Document> listOfDocuments = new LinkedList<Document>();

        Iterator iteradorCluster = this.getVetorListOfCluster().iterator();
        while (iteradorCluster.hasNext()) {
            Cluster cluster = (Cluster) iteradorCluster.next();
            Iterator iteradorDocuments = cluster.getListOfDocument().iterator();
            while (iteradorDocuments.hasNext()) {
                Document document = (Document) iteradorDocuments.next();
                listOfDocuments.add(document);
            }
        }
        return listOfDocuments;
    }

    private void OrdenarDescription() {
        LinkedList stringDescription = new LinkedList();
        if (!this.listOfTerms.isEmpty()) {
            for (int i = 0; i < this.listOfTerms.size(); i++) {
                stringDescription.add(this.listOfTerms.get(i).getWord());
            }
            String[] descWords = (String[]) stringDescription.toArray();
            List listadescription = Arrays.asList(descWords);
            Collections.sort(listadescription, String.CASE_INSENSITIVE_ORDER);
            descWords = (String[]) listadescription.toArray();

            LinkedList<Description> novaLista = new LinkedList();

            for (int i = 0; i < descWords.length; i++) {
                Iterator iterador = this.listOfTerms.iterator();
                while (iterador.hasNext()) {
                    Description desc = (Description) iterador.next();
                    if (desc.getWord().equals(descWords[i])) {
                        novaLista.add(desc);
                    }
                }
            }
            this.listOfTerms = novaLista;
        }
    }

    public void clearAllClusters() {
        this.getListOfCluster().clear();
    }

    public void clearAllDescriptions() {
        this.listOfTerms.clear();
    }

    public void removeCluster(Cluster cluster) {
        this.getListOfCluster().remove(cluster);
    }

    public void insertChild(Concept child) {
        this.getListOfConceptChild().add(child);
    }

    public void insertCluster(Cluster cluster) {
        if (!this.getListOfCluster().contains(cluster)) {
            this.getListOfCluster().add(cluster);
            Iterator it = cluster.getListOfDocument().iterator();
            while (it.hasNext()) {
                Document doc = (Document) it.next();
                this.insertDocRelative(doc);
            }
        }
    }

    public void removeDocIncluster(Document doc) {
        //procurando o documento
        if (!this.listOfCluster.isEmpty()) {
            for (int i = 0; i < this.getListOfCluster().size(); i++) {
                if (!this.listOfCluster.get(i).getListOfDocument().isEmpty()) {
                    boolean flag = false;
                    if (this.getListOfCluster().get(i).getListOfDocument().contains(doc)) {
                        this.getListOfCluster().get(i).getListOfDocument().remove(doc);
                        flag = true;
                    }
                    if (this.getListOfCluster().get(i).getListOfDocument().isEmpty()) {
                        removeCluster(this.getListOfCluster().get(i));
                    }
                    if (flag) {
                        break;
                    }
                }
            }
        }
    }

    public LinkedList<Description> getAllTerms() {

        LinkedList<Description> descriptionList = new LinkedList();
        Iterator allDocsIt = listaOfDocRelatives.iterator();
        while (allDocsIt.hasNext()) {
            Document adoc = (Document) allDocsIt.next();
            Iterator iteradorDocWord = adoc.getListOfWords().iterator();
            // criar descricao nula para ser preenchida
            while (iteradorDocWord.hasNext()) {
                // comparar as palavras
                //pegar uma pelavra // obs: um documento nao pode existir sm uma palavra pois nao passaria pela relevancia
                DocumentWord word = (DocumentWord) iteradorDocWord.next();
                // preparar para rodar o vetor de descricoes criando o iterador
                // definir peso igual ao peso da palavra que estou iterando caso nao tenha nenhuma outra palavra o peso ja esta definido
                // flag ja dizendo que a palavra nao existe
                boolean flag = false; // saber se term pra adicionar ou nao
                //if (descriptionList.size() > 0) {
                Iterator iteradorVectorDescription = descriptionList.iterator();
                while (iteradorVectorDescription.hasNext()) { // olhar se a palavra tem no vetor de descricoes
                    Description descriptionAux = (Description) iteradorVectorDescription.next();

                    if (word.getWord().equals(descriptionAux.getWord())) { // se a palavra for igual adicionar o peso e setar flag igual a true
                        flag = true;
                        //Double weight = descriptionAux.getWeigth() + word.getWeith();
                        descriptionAux.centroidByIncrementHit(word.getWeith());
                    }
                }
                if (flag == false) {// se nao tiver a descricao inclui ela no vetor
                    Description description = new Description(word.getWord(), word.getWeith());
                    descriptionList.add(description);// poe o peso
                }
            }
        }
        return OrdenarAllDescription(descriptionList);
    }

    private LinkedList<Description> OrdenarAllDescription(LinkedList descriptionList) {

        LinkedList stringDescription = new LinkedList();
        if (!descriptionList.isEmpty()) {
            for (int i = 0; i < descriptionList.size(); i++) {
                Description descAux = (Description) descriptionList.get(i);
                stringDescription.add(descAux.getWord());


            }

            //String[] descWords =  (String[]) stringDescription.toArray();
            String[] descWords = Arrays.copyOf(stringDescription.toArray(), stringDescription.toArray().length, String[].class);

            List listadescription = Arrays.asList(descWords);

            Collections.sort(listadescription, String.CASE_INSENSITIVE_ORDER);
            descWords = (String[]) listadescription.toArray();
            LinkedList<Description> novaLista = new LinkedList();
            for (int i = 0;
                    i < descWords.length;
                    i++) {
                Iterator iterador = descriptionList.iterator();
                while (iterador.hasNext()) {
                    Description desc = (Description) iterador.next();
                    if (desc.getWord().equals(descWords[i])) {
                        novaLista.add(desc);
                    }
                }
            }
            descriptionList = novaLista;
        }
        return descriptionList;
    }

    public LinkedList<Description> relativeDescription(Concept conceito, LinkedList<Description> allDesc) {
        if (conceito != null) {
            if (!conceito.getNameOfConcept().equals("root")) {
                Iterator it = conceito.getListOfTerms().iterator();
                while (it.hasNext()) {
                    Description desc = (Description) it.next();
                    System.out.println(desc.getWord());
                    allDesc.add(desc);
                }
                //procurar de quem ele é filho insere e assim por diante
                Iterator it2 = OntologyConceptualClustering.Instance().getHierarchy().iterator();
                while (it2.hasNext()) {
                    Concept conceitoAux = (Concept) it2.next();
                    boolean flag = false;
                    Iterator it3 = conceitoAux.getListOfConceptChild().iterator();
                    while (it3.hasNext()) {
                        Concept conceitoAux2 = (Concept) it3.next();
                        if (conceitoAux2 == conceito) {
                            flag = true;
                        }
                    }
                    if (flag) {
                        allDesc = relativeDescription(conceitoAux, allDesc);
                    }
                }
                return allDesc;
            } else {
                return allDesc;
            }
        } else {
            return allDesc;
        }
    }

    public void insertDocRelative(Document doc) {
        if (!this.getListOfDocumentsRelatives().contains(doc)) {
            this.listaOfDocRelatives.add(doc);
        }
    }

    public void removeDocRelative(Document doc) {
        if (this.listaOfDocRelatives.contains(doc)) {
            this.listaOfDocRelatives.remove(doc);
        }
    }

    public void setVetorListOfCluster(LinkedList<Cluster> listOfCluter) {
        this.setListOfCluster(listOfCluter);
    }

    public LinkedList<Concept> getVetorConceptsChild() {
        return getListOfConceptChild();
    }

    public void setVetorConceptsChild(LinkedList<Concept> vetorConceptsChild) {
        this.setListOfConceptChild(vetorConceptsChild);
    }

    public LinkedList<Description> getListOfTerms() {
        return listOfTerms;
    }

    public void setListOfTerms(LinkedList<Description> listOfTerms) {
        this.listOfTerms = listOfTerms;
    }

    public String getNameOfConcept() {
        return nameOfConcept;
    }

    public void setNameOfConcept(String nameOfConcept) {
        this.nameOfConcept = nameOfConcept;
    }

    public LinkedList<Cluster> getVetorListOfCluster() {
        return getListOfCluster();
    }

    public LinkedList<Document> getListOfDocumentsRelatives() {
        return listaOfDocRelatives;
    }

    public void removeChild(Concept child) {
        if (this.getListOfConceptChild().contains(child)) {
            this.getVetorConceptsChild().remove(child);
        }
    }

    public void printConcept() {
        for (int i = 0; i
                < this.listOfTerms.size(); i++) {
            System.out.print(this.listOfTerms.get(i).getWord().toString() + " ");
        }

        for (int i = 0; i
                < this.getListOfCluster().size(); i++) {
            this.getListOfCluster().get(i).printCluster();

        }
    }

    public String getStringOfTerms() {
        if (!this.getListOfTerms().isEmpty()) {
            String descricoes = "";
            Iterator it = this.getListOfTerms().iterator();
            while (it.hasNext()) {
                Description desc = (Description) it.next();
                descricoes = descricoes + desc.getWord() + " ";
            }
            return descricoes;
        }
        return null;
    }

    /**
     * @return the listOfConceptChild
     */
    public LinkedList<Concept> getListOfConceptChild() {
        return listOfConceptChild;
    }

    /**
     * @param listOfConceptChild the listOfConceptChild to set
     */
    public void setListOfConceptChild(LinkedList<Concept> listOfConceptChild) {
        this.listOfConceptChild = listOfConceptChild;
    }

    /**
     * @return the listOfCluster
     */
    public LinkedList<Cluster> getListOfCluster() {
        return listOfCluster;
    }

    /**
     * @param listOfCluster the listOfCluster to set
     */
    public void setListOfCluster(LinkedList<Cluster> listOfCluster) {
        this.listOfCluster = listOfCluster;
    }

    /**
     * @return the pai
     */
    public Concept getPai() {
        return pai;
    }

    /**
     * @param pai the pai to set
     */
    public void setPai(Concept pai) {
        this.pai = pai;
    }

    /**
     * @return the listOfAllTerms
     */
    public LinkedList<Description> getListOfAllTerms() {
        return listOfAllTerms;
    }

    /**
     * @param listOfAllTerms the listOfAllTerms to set
     */
    public void setListOfAllTerms(LinkedList<Description> listOfAllTerms) {
        this.listOfAllTerms = listOfAllTerms;
    }
}
