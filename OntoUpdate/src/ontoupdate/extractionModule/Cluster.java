/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.extractionModule;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author David Puziol Prata
 *
 * todo cluster tem uma funcao de similaridade para devolver a comparacao de um
 * documento com os documentos do cluster todo cluster deve ter um vetor de
 * documentos
 */
public class Cluster {

    private LinkedList<Document> listOfDocument; // aqui contera a lista de documento referente a um cluster

    public Cluster() {
        listOfDocument = new LinkedList();
    }

    /**
     * @return the listOfDocument
     */
    public LinkedList<Document> getListOfDocument() {
        return listOfDocument;
    }

    /**
     * @param listOfDocument the listOfDocument to set
     */
    public void setListOfDocument(LinkedList<Document> listOfDocument) {
        this.listOfDocument = listOfDocument;
    }

    public void addDocumentInCluster(Document document) {
        if (!this.listOfDocument.contains(document)) {
            listOfDocument.add(document);
        }
    }

    public void removeDoc(Document doc) {
        if (this.listOfDocument.contains(doc)) {
            this.listOfDocument.remove(doc);
        }
    }

    public int getNumberOfDocumentSimilar(Document document, LinkedList<Description> listaDesc, Double minClustering) {
        int similarity = 0;
        Iterator iterador = listOfDocument.iterator();
        while (iterador.hasNext()) {
            Document documentCluster = (Document) iterador.next();
            Double valor = cosineSimilarity(document, documentCluster, listaDesc);
            System.out.println("KNN ENCONTRADO NO VALOR DE " + valor);
            if (valor >= minClustering) {
                similarity++;
            }
        }
        return similarity;
    }

    private Double cosineSimilarity(Document doc1, Document doc2, LinkedList<Description> listaDesc) {

        LinkedList<DocumentWord> listWords = new LinkedList(); // cria o vetor somente com as palavras que possuem em comum nos dois vetores de palavras do document
        LinkedList<DocumentWord> doc1Aux = new LinkedList(); // cria o vetor somente com as palavras que possuem em comum nos dois vetores de palavras do document
        LinkedList<DocumentWord> doc2Aux = new LinkedList();
        
//        System.out.println("\n\nDOCUMENTO 1 PALAVRAS QUE VAO PARA O CALCULO DE SIMILARIDADE\n");
//        for(int i = 0; i<doc1.getListOfWords().size(); i++){
//            String word = doc1.getListOfWords().get(i).getWord();
//            System.out.println(word);
//        }

//        System.out.println("\n\nDOCUMENTO 2 PALAVRAS QUE VAO PARA O CALCULO DE SIMILARIDADE\n");
//        for(int i = 0; i<doc2.getListOfWords().size(); i++){
//            String word = doc2.getListOfWords().get(i).getWord();
//            System.out.println(word);
//        }
        
        for (int i = 0; i < doc1.getListOfWords().size(); i++) {
            //boolean flag = false;
            for (int j = 0; j < doc2.getListOfWords().size(); j++) {
                if (doc1.getListOfWords().get(i).getWord().equals(doc2.getListOfWords().get(j).getWord())) {
                    if (listaDesc != null) {
                        boolean flag = true;
                        for (int k = 0; k < listaDesc.size(); k++) {
                            String descricao = listaDesc.get(k).getWord();
                            if (doc1.getListOfWords().get(i).getWord().contains(descricao)) {
                                //System.out.println("PALAVRA QUE DEVE SER RETIRARDA = "+descricao);
                                flag = false;
                            }
                        }
                        if (flag) {
                            doc1Aux.add(doc1.getListOfWords().get(i));
                            doc2Aux.add(doc2.getListOfWords().get(j));
                        }
                    } else {
                        doc1Aux.add(doc1.getListOfWords().get(i));
                        doc2Aux.add(doc2.getListOfWords().get(j));
                    }
                    // somente tem 1 palavra em cada vetor do mesmo tipo entao so entrara uma vez aqui e os vetore aux iram ter a mesma sincronia
                }
            }
        }

        if (doc1Aux.size() > 0) { // possui elementos em comum e pode ter similaridade
            // seta no documento auxiliar que ira para o calculo dos cossenos os pecos das palavras do documento original
            return calculateCosineSimilarity(doc1Aux, doc2Aux);
        } else { // nao possui pesos em comum entao a similaridade é zero

            return 0.0;
        }
    }

    //funcoes de setagem de peso forçado somente para testes
    private void setWeightToZero(LinkedList<DocumentWord> vetor) {

        Iterator iteradorVetor = vetor.iterator();
        while (iteradorVetor.hasNext()) {
            DocumentWord word = (DocumentWord) iteradorVetor.next();
            word.setNumberOfHits(0.0);
            word.setWeith(0.0);
        }
    }

    private void changeWeight(LinkedList<DocumentWord> doc1, LinkedList<DocumentWord> docAux) {
        Iterator iteradorDocAux = docAux.iterator();
        Iterator iteradorDoc = doc1.iterator();

        while (iteradorDocAux.hasNext()) {
            DocumentWord word1 = (DocumentWord) iteradorDocAux.next();
            iteradorDoc = docAux.iterator();
            while (iteradorDoc.hasNext()) {
                DocumentWord wordDoc1 = (DocumentWord) iteradorDoc.next();
                if (word1.getWord().equals(wordDoc1.getWord())) {
                    word1.setWeith(wordDoc1.getWeith());
                    word1.setNumberOfHits(wordDoc1.getNumberOfHits());
                }
            }
        }
    }

    private Double calculateCosineSimilarity(LinkedList<DocumentWord> documentWeights, LinkedList<DocumentWord> queryWeights) {

        Iterator it_wij = documentWeights.iterator();
        Iterator it_wiq = queryWeights.iterator();
        //Inicializar todas as somatórias a serem realizadas
        Double sum_wij_x_wiq = new Double(0);
        Double sum_wij_squared = new Double(0);
        Double sum_wiq_squared = new Double(0);
        //Realizar as somatórias necessárias
        while (it_wij.hasNext()) {
            DocumentWord doc1 = (DocumentWord) it_wij.next();
            DocumentWord doc2 = (DocumentWord) it_wiq.next();

            Double wij_value = doc1.getWeith();
            Double wiq_value = doc2.getWeith();

            sum_wij_x_wiq += wij_value * wiq_value;
            sum_wij_squared += wij_value * wij_value;
            sum_wiq_squared += wiq_value * wiq_value;
        }

        //Calcular o cosseno, efetivamente, utilizando as somatórias préviamente calculadas.
        Double cosine = sum_wij_x_wiq / (Math.sqrt(sum_wij_squared) * Math.sqrt(sum_wiq_squared));

        return cosine;
    }

    public Double evaluation() {
        System.out.println("INICIANDO CALCULO DE EVALUATION NO CLUSTER ");
        Double sumSimilarity = 0.0;

        for (int i = 0; i < this.listOfDocument.size(); i++) {
            Document doc1 = this.listOfDocument.get(i);
            for (int j = i +1; j < this.listOfDocument.size(); j++) {
                Document doc2 = this.listOfDocument.get(j);
                sumSimilarity += cosineSimilarity(doc1, doc2, null);
            }
        }

        Double tam = Double.parseDouble(String.valueOf(this.listOfDocument.size()));
        sumSimilarity = sumSimilarity / (tam * tam);
        System.out.println("EVALUATION NO CLUSTER = " + sumSimilarity);
        return sumSimilarity;
    }

    public void printCluster() {
        System.out.println("cluster com " + this.listOfDocument.size());
        Iterator it = this.getListOfDocument().iterator();
        while (it.hasNext()) {
            Document doc = (Document) it.next();
            System.out.println(doc.getPath());
        }
    }
}
