/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.extractionModule;

import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;
import ontoupdate.util.Logger;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public class OntologyConceptualClustering {

    private static OntologyConceptualClustering instance;
    private static int numConcept;
    private LinkedList<Concept> hierarchy;
    private Concept root;
    private Double minClassify; //valor que o usuario irá colocar se quiser alterar o valor padrao
    private Double minClustering;
    private Double minBeConcept;
    private Double minBeDescription;
    private Concept lastConcept;

    static public OntologyConceptualClustering Instance() {
        if (getInstance() == null) {
            setInstance(new OntologyConceptualClustering());
        }

        return getInstance();
    }

    public static void setInstance(OntologyConceptualClustering aInstance) {
        instance = aInstance;
    }

    public static OntologyConceptualClustering getInstance() {
        return instance;
    }

    static public void GenerateNewInstance() {
        setInstance(new OntologyConceptualClustering());
        Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.NOT_ERROR, OntologyConceptualClustering.class, "Uma nova instancia da Arvore de Conceitos foi gerada");
    }

    public OntologyConceptualClustering() {
        hierarchy = new LinkedList(); //cria a hierarquia ou reseta uma
        minClassify = CurrentProfile.Instance().getMin_classify(); // defini o valor do patamar de classificacao sobre um conceito
        minClustering = CurrentProfile.Instance().getMin_classify(); // defini o valor do patamar de classificacao sobre um conceito
        minBeConcept = CurrentProfile.Instance().getMin_concept();
        minBeDescription = CurrentProfile.Instance().getMin_description();
        numConcept = 0;
    }

    public synchronized void insertDocument(Document doc) {
        System.out.println("\n\n\n\n INICIO DA INSERCAO DO DOCUMENTO " + doc.getPath());

        this.lastConcept = root;
        WebDCC(doc, root);
        //este ta parte era feita anes para verificar a consistencia da árvore. Não é mais necessária, mas caso alguma modificação futura que seja feita
        //precisar destes reparos a função encontra-se pronta. Algumas partes deste código fonte esta pronto para usar. somente precisa verificar se precisa ou nào ser colocada.
//        if (CurrentProfile.Instance().isCheckNodes() || CurrentProfile.Instance().isCheckFinalNodes()) {
//            checkConsistence(root);
//        }
        if (ServerProxyForm.Instance().askUpadteOntology()) {
            QuickMessageDisplay.ShowWarningMsg("A árvore de conceitos atingiu o tamanho estabelecido pelo usuário. É necessário atualizar a ontologia", 5000);
            int saida = JOptionPane.showConfirmDialog(null, "Deseja atualizar a ontologia?", null, JOptionPane.WARNING_MESSAGE);
            if (saida == 0) {
                ServerProxyForm.Instance().jButtonUpdate.doClick();

            } else {
                int saida1 = JOptionPane.showConfirmDialog(null, "O ontoUpdate já pode descartar a árvore de coceitos", null, JOptionPane.WARNING_MESSAGE);
                if (saida1 == 0) {
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                } else {
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
            }
        }
        for (int i = 0; i < hierarchy.size(); i++) {
            hierarchy.get(i).RemakeDescription(minBeDescription);
        }
    }

    // classe sincronizada pois tem varias treads tentando adicionar ao mesmo tempo
    public void WebDCC(Document document, Concept master) {
        // inicio do algoritmo WEBDCC
//        System.out.println(master.getNameOfConcept()
        System.out.println("\nDOCUMENTO " + document.getPath() + " ENTRANDO NO WEB DCC");
        if (getHierarchy().isEmpty()) {
            System.out.println("HIERARQUIA VAZIA");
            // criar raiz da arvore
            root = new Concept("root"); //cria conceito root
            System.out.println("CONCEITO ROOT CRIADO E ADICIONADO");
            getHierarchy().add(root);
            lastConcept = root;
            master = root;
        }

        Concept cParent = master;
        //System.out.println("criando uma copia parcial do documento para percorrer a arvore");
        // Document docWebDCC = new Document(document.getListOfWords());

        while (cParent.hasChild() && cParent.hasClassifyInChildren(getMinClassify(), document)) {


            // o filho com o maior classify vira Cparent
            Concept cParentChild = cParent.getChildWithHigherClassify(document, getMinClassify()); // pega o filho de maior classificador
            cParent.updateDescription(document, this.minBeDescription);
            //update das descricoes
            // if (cParentChild != null) {
            if (cParentChild != null) {
                cParent = cParentChild;
            } else {
                break;
            }
        }
        cParent.insertDocRelative(document);
        cParent.RemakeDescription(minBeDescription);
        // retorna o cluster que será colocado o documento
        Cluster clusterCparent = cParent.KnnChooseClusterToDocument(document, this.getMinClustering()); // neste ponto eh passado o documento original com todos os termos
        System.out.println("ADICIONADO NO CLUSTER DO CONCEITO " + cParent.getNameOfConcept());
        // adiciona ao cluster do conceito o documento oroginal
        clusterCparent.addDocumentInCluster(document);

        // somente vira conceito se tiver mais de 1 cluster
        if (clusterCparent.getListOfDocument().size() > 1) {
            // seria qeu ser maior que um pois a funcao evaluation precisa de 2 documentos pelo menos
            Double valor = clusterCparent.evaluation();
            if (valor >= this.getMinBeConcept()) { // se for maior que o valor vira conceito

                LinkedList<Cluster> vetorClusters = new LinkedList();
                //LinkedList<Document> vetorDocument = new LinkedList<Document>(clusterCparent.getListOfDocument());
                vetorClusters.add(clusterCparent); // ja adiciona e com todos os documentos ainda
                //concetpformation esta dentro de um conceito
                Concept newConceptByCluster = new Concept("conceito " + numConcept, vetorClusters, minBeDescription, cParent); //gera os pesos dos conceitos
                //parada se repetir o conceito
                if (newConceptByCluster.getListOfTerms().size() > 0) {
                    //if (checkLoop(newConceptByCluster, cParent)) {
                        numConcept++;
                        lastConcept = newConceptByCluster;
                        // este conceito sera filho do cparent
                        cParent.insertChild(newConceptByCluster);
                        this.getHierarchy().add(newConceptByCluster);
                        newConceptByCluster.insertCluster(clusterCparent);
                        cParent.removeCluster(clusterCparent);
                        // para todo documento de cParent tentar colocar no filho newConcept que eh o filho mais novo dele

                        // copia necessaria devido a condicao de corrida da recursao e problema do iterador presente no java
                        LinkedList<Cluster> copyCParentVetorCluster = copyListOfClusters(cParent.getVetorListOfCluster());
                        //LinkedList<Cluster> copyCParentVetorCluster = CloneListOfclusters(cParent.getVetorListOfCluster());

                        for (int i = 0; i < copyCParentVetorCluster.size(); i++) { // enquanto tiver proximo na hierarquia
                            Cluster cluster = copyCParentVetorCluster.get(i);
                            LinkedList<Document> copyClusterVetorDoc = copyListOfDocuments(cluster.getListOfDocument());
                            for (int j = 0; j < copyClusterVetorDoc.size(); j++) {
                                Document docClusterCopy = (Document) copyClusterVetorDoc.get(j);
//                            if (newConceptByCluster.getListOfDocumentsRelatives().contains(docClusterCopy)) {
//                                newConceptByCluster.removeDocRelative(docClusterCopy);
//                            }
                                if (newConceptByCluster.hasClassifyInConcept(getMinClassify(), docClusterCopy)) { // se o documento for classificado no novo cluster remove do cparent e poe no novo
                                    // documento so existe em 1 clustere, procura o documento e remove se o cluster ficar vazio remove la dentro da funcao
                                    // motivo dessa procura é os objetos cluster da copia e cluster do conceito serem diferentes devido as alteracoes
                                    cParent.removeDocIncluster(docClusterCopy);
                                    newConceptByCluster.updateDescription(document, this.minBeDescription);
                                    WebDCC(docClusterCopy, newConceptByCluster);
                                    // newConceptByCluster.RemakeDescription(minBeDescription);
                                }
                            }
                        }
                    //}
                }
                System.out.println("A LISTA DE DESCRICOES FICOU SEM NADA ENTAO DELETA");
               // cParent.RemakeDescription(minBeDescription);
            }   
        }
        //cParent.RemakeDescription(minBeDescription);
    }

    private Document removeConceptTermsOfDocument(Concept cParent, Document document) {
        // criacao dos iteradores
        Iterator iteradorDescriptions = cParent.getListOfTerms().iterator();
        Iterator iteradorWords;

        // loop para procurar rodar o vetor de descricao do conceito
        while (iteradorDescriptions.hasNext()) {
            Description description = (Description) iteradorDescriptions.next();
            // loop para rodar o vetor de palavras do documento
            iteradorWords = document.getListOfWords().iterator();
            while (iteradorWords.hasNext()) {
                DocumentWord word = (DocumentWord) iteradorWords.next();
                if (description.getWord().equals(word.getWord())) { // se a palavra for igual, tira do documento
                    document.getListOfWords().remove(word);
                }
            }
        }
        return document; // retorna documento original sem as palavras
    }

    private LinkedList copyListOfClusters(LinkedList<Cluster> listaCluster) {
        LinkedList copyCParentVetorCluster = new LinkedList();
        Iterator it = listaCluster.iterator();
        while (it.hasNext()) {
            Cluster cluster = (Cluster) it.next();
            copyCParentVetorCluster.add(cluster);
        }
        return copyCParentVetorCluster;
    }

    private LinkedList copyListOfDocuments(LinkedList<Document> listaDocument) {
        LinkedList copyCParentVetorDoc = new LinkedList();
        Iterator it = listaDocument.iterator();
        while (it.hasNext()) {
            Document doc = (Document) it.next();
            copyCParentVetorDoc.add(doc);
        }
        return copyCParentVetorDoc;
    }

    private LinkedList copyListOfChild(LinkedList<Concept> listaChild) {
        LinkedList copyCParentVetorChild = new LinkedList();
        Iterator it = listaChild.iterator();
        while (it.hasNext()) {
            Concept conceito = (Concept) it.next();
            copyCParentVetorChild.add(conceito);
        }
        return copyCParentVetorChild;
    }

    private LinkedList CloneListOfclusters(LinkedList<Cluster> listaCluster) {
        LinkedList<Cluster> cloneCParentVetorCluster = new LinkedList();
        Iterator it = listaCluster.iterator();
        while (it.hasNext()) {
            Cluster cluster = (Cluster) it.next();
            Cluster newCluster = new Cluster();
            Iterator it1 = cluster.getListOfDocument().iterator();
            while (it1.hasNext()) {
                Document doc = (Document) it1.next();
                Document newDoc = new Document(doc.getText(), doc.getPath(), doc.getOriginalText());
                newCluster.addDocumentInCluster(newDoc);
            }
            cloneCParentVetorCluster.add(newCluster);
        }
        return cloneCParentVetorCluster;
    }

    private void printHierarchy() {
        Iterator iteradorHierarchy = hierarchy.iterator();
        while (iteradorHierarchy.hasNext()) {
            Concept conceito = (Concept) iteradorHierarchy.next();
            conceito.printConcept();
        }
    }

    private boolean checkLoop(Concept conceitoNovo, Concept cParent) {
        if (!lastConcept.equals(root)) {
            if (conceitoNovo.getListOfDocumentsRelatives().size() == this.lastConcept.getListOfDocumentsRelatives().size()) {
                if (this.lastConcept.getListOfDocumentsRelatives().containsAll(conceitoNovo.getListOfDocumentsRelatives())) {
                    if (cParent.getListOfTerms().containsAll(conceitoNovo.getListOfTerms())) {
                        if (cParent.getListOfCluster().size() < 2) {
                            return false;
                        }
                    }
                    return false;

                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

//    private void checkConsistence(Concept cParent) {
//        if (cParent.hasChild()) {
//            if (CurrentProfile.Instance().isCheckNodes()) {
//                LinkedList<Concept> copyListChild = copyListOfChild(cParent.getListOfConceptChild());
//                for (int i = 0; i < copyListChild.size(); i++) {
//                    checkConsistence(copyListChild.get(i));
//                }
//                if (!cParent.equals(root)) {
//                    for (int j = 0; j < cParent.getListOfConceptChild().size(); j++) {
//                        if (cParent.getListOfConceptChild().get(j).getListOfDocumentsRelatives().size() == cParent.getListOfDocumentsRelatives().size()) {
//                            for (int k = 0; k < cParent.getListOfConceptChild().size(); k++) {
//                                cParent.getPai().insertChild(cParent.getListOfConceptChild().get(k));
//                                cParent.getListOfConceptChild().get(k).setPai(cParent.getPai());
//                            }
//                            cParent.getPai().removeChild(cParent);
//                            this.getHierarchy().remove(cParent);
//                            break;
//                        }
//                    }
//                }
//            }
//        } else {
//            if (CurrentProfile.Instance().isCheckFinalNodes()) {
//                if (cParent.getListOfCluster().isEmpty()) {
//                    if (cParent != root) {
//                        cParent.getPai().removeChild(cParent);
//                    }
//                }
//            }
//        }
//    }

    public void imprimirVetoresDocs() {

        System.out.println("\n\n\n IMPRESSAO DAS DESCRIÇÕES E DOCUMENTOS REFERENTES A CADA CONCEITO");
        Iterator iterador = root.getListOfConceptChild().iterator();

        while (iterador.hasNext()) {
            Concept conceito = (Concept) iterador.next();
            System.out.println("\n CONCEITO = " + conceito.getNameOfConcept());
            Iterator itDesc = conceito.getListOfTerms().iterator();

            while (itDesc.hasNext()) {

                System.out.println("\n AMOSTRA DE DESCRIÇÕES");
                Description desc = (Description) itDesc.next();
                System.out.println(desc.getWord() + "\t\t\t\t\t\t" + desc.getWeigth() + "\t" + desc.getNumHits() + "\t");
            }
        }

    }

    /**
     * @return the hierarchy
     */
    public LinkedList<Concept> getHierarchy() {
        return hierarchy;
    }

    /**
     * @param hierarchy the hierarchy to set
     */
    public void setHierarchy(LinkedList<Concept> hierarchy) {
        this.hierarchy = hierarchy;
    }

    /**
     * @return the root
     */
    public Concept getRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(Concept root) {
        this.root = root;
    }

    /**
     * @return the minClassify
     */
    public Double getMinClassify() {
        return minClassify;
    }

    /**
     * @param minClassify the minClassify to set
     */
    public void setMinClassify(Double minClassify) {
        this.minClassify = minClassify;
    }

    /**
     * @return the minClustering
     */
    public Double getMinClustering() {
        return minClustering;
    }

    /**
     * @param minClustering the minClustering to set
     */
    public void setMinClustering(Double minClustering) {
        this.minClustering = minClustering;
    }

    /**
     * @return the minBeConcept
     */
    public Double getMinBeConcept() {
        return minBeConcept;
    }

    /**
     * @param minBeConcept the minBeConcept to set
     */
    public void setMinBeConcept(Double minBeConcept) {
        this.minBeConcept = minBeConcept;
    }

    /**
     * @return the minBeDescription
     */
    public Double getMinBeDescription() {
        return minBeDescription;
    }

    /**
     * @param minBeDescription the minBeDescription to set
     */
    public void setMinBeDescription(Double minBeDescription) {
        this.minBeDescription = minBeDescription;
    }
}
