/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.extractionModule;
/**
 *
 * @author David Puziol Prata
 */
public class DocumentWord {

    private Double weigth;
    private String word;
    private Double numberOfHits;

    /**
     * @return the weith
     */

    public DocumentWord(String word){
        this.word = word;
        setNumberOfHits(1.0);
    }

    public DocumentWord(String word, Double weight){
        this.weigth=weight;
        this.word = word;
    }

    public Double getWeith() {
        return weigth;
    }

    /**
     * @param weith the weith to set
     */
    public void setWeith(Double weith) {
        this.weigth = weith;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }


    public void incrementHit(){
        setNumberOfHits(getNumberOfHits()+1.0);
    }

    /**
     * @return the numberOfHits
     */
    public Double getNumberOfHits() {
        return numberOfHits;
    }

    /**
     * @param numberOfHits the numberOfHits to set
     */
    public void setNumberOfHits(Double numberOfHits) {
        this.numberOfHits = numberOfHits;
    }

}
