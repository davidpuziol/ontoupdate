/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontoupdate.extractionModule;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import ontoupdate.agentModule.OntologyExtractor;
import ontoupdate.agentModule.WordModifier;
import ontoupdate.agentModule.graficInterface.CurrentProfile;
import ontoupdate.util.Semaforo;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.portugueseStemmer;

/**
 *
 * @author David Puziol Prata
 */
public class Document {

    private LinkedList<DocumentWord> listOfWords;
    private String text;
    private String path;
    private String originalText;

    public Document(String text, String path, String originalText) {
        this.text = text;
        this.path = path;
        this.originalText = originalText;
        listOfWords = new LinkedList();
        
        LinkedList<String> palavrasOntologia = OntologyExtractor.Instance().getAllWords();

        Semaforo.Instance().P();
        if (CurrentProfile.Instance().isIgnore_accentuation()) {
            for (int i = 0; i < palavrasOntologia.size(); i++) {
                palavrasOntologia.set(i, WordModifier.TransformAccentuatedCharacters(palavrasOntologia.get(i)));
            }
        }
        Semaforo.Instance().V();

        for (int i = 0; i < palavrasOntologia.size(); i++) {
            String[] string = palavrasOntologia.get(i).split(" ");
            //se maior do que 1 é composta
            if (string.length > 1 && this.text.contains(palavrasOntologia.get(i))) {
                //formar a palavra ligando com os _
                String palavra = palavrasOntologia.get(i).replace(' ', '_');
                this.text = this.text.replace(palavrasOntologia.get(i), palavra);
            }
        }

        System.out.println("\nTEXTO = " + text);
        
        
        
        String[] allWords = makeAllWords();
        

        
        allWords = reduceAllWords(allWords);
        //ordenando
        List listaPalavras = Arrays.asList(allWords);

        Collections.sort(listaPalavras, String.CASE_INSENSITIVE_ORDER);
        allWords = (String[]) listaPalavras.toArray();

        updateOfCreateListOfWords(allWords);
        
        for(int i =0; i < listOfWords.size(); i++){
            DocumentWord dw = listOfWords.get(i);
            dw.setWord(dw.getWord().replace('_', ' '));
        }
        if(CurrentProfile.Instance().isCheckFormula()){
           calculateNomalizedFrequencyAmandiGodoy(); 
        }else{
            calculateNomalizedFrequency();
        }
        //printAllterms();
    }

    public Document(String text, String path, String originalText, LinkedList<DocumentWord> listOfWords) {
        this.text = text;
        this.path = path;
        this.originalText = originalText;
        this.listOfWords = listOfWords;

    }

    private String[] makeAllWords() {
        String[] packetOfWord = getText().split(" ");
        String nova = "";
        for (int i = 0; i < packetOfWord.length; i++) {
            if ((packetOfWord[i] != null) || (!packetOfWord[i].equals(" ")) || !packetOfWord[i].isEmpty()) {
                nova = nova + packetOfWord[i] + " ";
            }
        }
        this.setText(nova);
        packetOfWord = nova.split(" ");

        return packetOfWord;
    }

    private void printAllterms() {
        int i;
        for (i = 0; i < getListOfWords().size(); i++) {
            System.out.println(getListOfWords().get(i).getWord().toString() + " peso = " + getListOfWords().get(i).getWeith() + " Hits = " + getListOfWords().get(i).getNumberOfHits());
        }
    }

    private String[] reduceAllWords(String[] allWords) {
        SnowballStemmer stemmer = (SnowballStemmer) new portugueseStemmer();
        for (int i = 0; i < allWords.length; i++) {
            allWords[i] = wordStemmer(allWords[i], stemmer);
        }
        return allWords;
    }

    private synchronized String wordStemmer(String word, SnowballStemmer stemmer) {
        stemmer.setCurrent(word);
        stemmer.stem();

        String stem = stemmer.getCurrent();
        if (stem == null || stem.isEmpty()) {
            return word;
        } else {
            return stem;
        }
    }

    private void updateOfCreateListOfWords(String[] allWords) {
        boolean flag;
        for (int i = 0; i < allWords.length; i++) {// percorrendo o vetor de palavras

            // procurando a palavra no vetor de frequencias
            flag = false; // seta como se nao tivesse a palavra e tenta procurar
            for (int j = 0; j < getListOfWords().size(); j++) { // loop para procurar
                // DocumentWord wop = getListOfWords().get(j); // pegando a palavra
                if (getListOfWords().get(j).getWord().equals(allWords[i])) { // se for identica aumenta o numero de hit
                    getListOfWords().get(j).incrementHit();
                    flag = true; // seta flag para nao adidionar
                    j = getListOfWords().size(); // sai fora do loop pois se já achou nao tem duas vezes
                }
            }
            if (flag == false) { // nao tem o termo tem que colocar
                DocumentWord word = new DocumentWord(allWords[i]);
                getListOfWords().add(word);
            }
        }
    }

    private void calculateNomalizedFrequencyAmandiGodoy() {

        Double sumAllTermsSquare = 0.0;
        // pegando a soma de dos quadrados de todos os termos
        Iterator iterator = getListOfWords().iterator();
        while (iterator.hasNext()) {
            DocumentWord wop = (DocumentWord) iterator.next();
            //System.out.println("palavra "+wop.getWord()+" hits "+wop.getNumberOfHits()+" hits² = ");
            Double hitsSquare = wop.getNumberOfHits() * wop.getNumberOfHits();
            /// System.out.print(hitsSquare.toString());
            sumAllTermsSquare = sumAllTermsSquare + hitsSquare;  //colocando termo ao quadrado e somando
            // System.out.println("somatoria total = "+sumAllTermsSquare.toString());
        }
        sumAllTermsSquare = Math.sqrt(sumAllTermsSquare);

        //resetando o iterador e percorrendo dinovo
        iterator = getListOfWords().iterator();
        while (iterator.hasNext()) {
            DocumentWord wop = (DocumentWord) iterator.next();
            wop.setWeith(wop.getNumberOfHits() / (sumAllTermsSquare));
        }
    }

    private void calculateNomalizedFrequency() {
        // iniciando com 1 pois existe texto e garante que nao sera divisivel por zero
        Double max = 1.0;
        DocumentWord word = null;
        // pegando a soma de dos quadrados de todos os termos
        Iterator iterator = getListOfWords().iterator();
        while (iterator.hasNext()) {
            DocumentWord wop = (DocumentWord) iterator.next();
            if (wop.getNumberOfHits() > max) {
                max = wop.getNumberOfHits();
                word = wop;
            }
        }

        //System.out.println("max frequencia =" +max+" da palavra "+word.getWord());
        //resetando o iterador e percorrendo dinovo
        iterator = getListOfWords().iterator();
        while (iterator.hasNext()) {
            DocumentWord wop = (DocumentWord) iterator.next();
            wop.setWeith(wop.getNumberOfHits() / max);
        }
    }

    public LinkedList getWordsSimilars(String word) {
        LinkedList<String> words = new LinkedList();
        String[] listaString = this.getText().split(" ");
        for (int i = 0; i < listaString.length; i++) {
            if (listaString[i].startsWith(word)) {
                Iterator iteradorWords = words.iterator();
                boolean flag = false;
                while (iteradorWords.hasNext()) {
                    String name = (String) iteradorWords.next();
                    if (name.equals(listaString[i])) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    words.add(listaString[i]);
                }
            }
        }
        if (words.isEmpty()) {
            words.add(word);
        }
        return words;
    }

    /**
     * @return the listOfWords
     */
    public LinkedList<DocumentWord> getListOfWords() {
        return listOfWords;
    }

    /**
     * @param listOfWords the listOfWords to set
     */
    private void setListOfWords(LinkedList<DocumentWord> listOfWords) {
        this.listOfWords.addAll(listOfWords);
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the originalText
     */
    public String getOriginalText() {
        return originalText;
    }

    /**
     * @param originalText the originalText to set
     */
    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }
}
