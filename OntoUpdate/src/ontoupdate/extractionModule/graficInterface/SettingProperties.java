package ontoupdate.extractionModule.graficInterface;

import com.hp.hpl.jena.util.FileUtils;
import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import ontoupdate.agentModule.OntologyExtractor;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;
import ontoupdate.extractionModule.OntologyConceptualClustering;
import ontoupdate.resources.ResourceManager;
import ontoupdate.specialComponents.CustomIconsTreeCellRenderer;
import ontoupdate.util.Semaforo;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public class SettingProperties extends javax.swing.JFrame {

    private Stack pilha = new Stack();
    private String[] classNames;
    private String[] classeProperties;
    private String nameProperties = null;
    private OWLModel owlModel = null;
    private DefaultListModel modeloList1;
    private DefaultListModel modeloList2;
    private OntologyLearningTree ontologyLearningTree;

    /** Creates new form SettingVerb */
    public SettingProperties(String nameProperties, OWLModel model, OntologyLearningTree ontologyLearningTree) {
        initComponents();
        ontoupdate.util.DisplayUtils.CenterGuiComponent(this);
        if (model != null) {

            CustomIconsTreeCellRenderer customIconsTreeCellRenderer = new CustomIconsTreeCellRenderer();
            customIconsTreeCellRenderer.AddImagesToRender("ONTOLOGY", ResourceManager.GetImageIcon("IMG_TREE_ONTOLOGY"),
                    "FAMILY_CLASS", ResourceManager.GetImageIcon("IMG_TREE_FAMILY_CLASS"),
                    "REGEX", ResourceManager.GetImageIcon("IMG_TREE_REGEX"),
                    "WEIGHT", ResourceManager.GetImageIcon("IMG_TREE_WEIGHT"),
                    "DEEP", ResourceManager.GetImageIcon("IMG_TREE_DEEP"),
                    "MAIN_TERM", ResourceManager.GetImageIcon("IMG_TREE_MAIN_TERM"),
                    "SYNONYMS", ResourceManager.GetImageIcon("IMG_TREE_SYNONYMS"),
                    "VERBS", ResourceManager.GetImageIcon("IMG_TREE_VERBS"),
                    "INSTANCE", ResourceManager.GetImageIcon("IMG_TREE_INSTANCE"),
                    "CLASS", ResourceManager.GetImageIcon("IMG_TREE_CLASS"),
                    "PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_PROPERTY"),
                    "INVERSE_PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_INVERSE_PROPERTY"),
                    "DOMAIN", ResourceManager.GetImageIcon("IMG_TREE_DOMAIN"),
                    "RANGE", ResourceManager.GetImageIcon("IMG_TREE_RANGE"),
                    "TOTAL_SCOPE", ResourceManager.GetImageIcon("IMG_TREE_TOTAL_SCOPE"),
                    "SCOPE", ResourceManager.GetImageIcon("IMG_TREE_SCOPE"),
                    "CONCEPT", ResourceManager.GetImageIcon("IMG_TREE_CONCEPT"),
                    "DESCRIPTION", ResourceManager.GetImageIcon("IMG_TREE_DESCRIPTION"),
                    "CLUSTER", ResourceManager.GetImageIcon("IMG_TREE_CLUSTER"),
                    "LISTCONCEPTS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CONCEPTS"),
                    "LISTCLUSTERS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CLUSTERS"));

            this.jTreeOntologia.setCellRenderer(customIconsTreeCellRenderer);
            this.jTreeOntologia.setCellRenderer(customIconsTreeCellRenderer);
            if (nameProperties != null) {
                this.nameProperties = nameProperties;
                this.jTextFieldNameProperties.setText(nameProperties);
            }
            this.owlModel = model;
            this.jList1.removeAll();
            this.jList2.removeAll();
            this.jList1.setModel(new DefaultListModel());
            this.jList2.setModel(new DefaultListModel());
            this.ontologyLearningTree = ontologyLearningTree;
            modeloList1 = (DefaultListModel) this.jList1.getModel();
            modeloList2 = (DefaultListModel) this.jList2.getModel();

            this.jTreeOntologia.setModel(this.ontologyLearningTree.jTreeOntologyCarregada.getModel());
            montarLista();
            montarPropriedades();


        } else {
            QuickMessageDisplay.ShowErrorMsg("Não possui Ontologia carregada no sistema!", 2000);
            this.dispose();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jTextFieldNameProperties = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jButtonCancelar = new javax.swing.JButton();
        jButtonInserir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pb_help_fij1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTreeOntologia = new javax.swing.JTree();
        jButtonDesfazer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Escolher Âmbito"));

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jScrollPane1.setViewportView(jList1);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 182, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 264, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );

        jTextFieldNameProperties.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNamePropertiesActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome da Propriedade");

        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/TICK_NO.png"))); // NOI18N
        jButtonCancelar.setText("Sair");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonInserir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INSERT.png"))); // NOI18N
        jButtonInserir.setText("Inserir");
        jButtonInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInserirActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2)));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/PROPERTIES.png"))); // NOI18N
        jLabel4.setText("              Configurações De Propriedades");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jLabel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 537, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(86, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Escolhe Domínio"));

        jList2.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jScrollPane2.setViewportView(jList2);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
        );

        jLabel2.setText("Âmbito da Propriedade");

        jLabel3.setText("Domínio da Propriedade");

        pb_help_fij1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INFO_MENOR.png"))); // NOI18N
        pb_help_fij1.setToolTipText("Mais Informações");
        pb_help_fij1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pb_help_fij1MouseClicked(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ontologia Carregada"));

        jScrollPane3.setViewportView(jTreeOntologia);

        jButtonDesfazer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/UNDO.png"))); // NOI18N
        jButtonDesfazer.setText("Desfazer");
        jButtonDesfazer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDesfazerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
            .add(jPanel4Layout.createSequentialGroup()
                .add(21, 21, 21)
                .add(jButtonDesfazer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 170, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 264, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 7, Short.MAX_VALUE)
                .add(jButtonDesfazer))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pb_help_fij1)
                .addContainerGap(459, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createSequentialGroup()
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jButtonInserir, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(layout.createSequentialGroup()
                            .addContainerGap()
                            .add(jLabel2)))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jLabel3)
                        .add(layout.createSequentialGroup()
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jButtonCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 193, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jTextFieldNameProperties, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 581, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(pb_help_fij1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextFieldNameProperties, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(7, 7, 7)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButtonInserir)
                            .add(jButtonCancelar)))
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldNamePropertiesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNamePropertiesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNamePropertiesActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        int saida = JOptionPane.showConfirmDialog(this, "Recarregar as mudanças no sistema?");
        if (saida == 0) {
            try {
                Semaforo.Instance().P();
                OntologyExtractor.Instance().Reload(owlModel);
                Semaforo.Instance().V();
                 int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);;
                if(saida1 == 0){
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }else{
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                 ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } catch (Exception ex) {
                Semaforo.Instance().V();
                java.util.logging.Logger.getLogger(SettingClass.class.getName()).log(Level.SEVERE, null, ex);
                this.dispose();
            }
        } else {
            if (saida == 1) {
                 int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);
                if (saida1 == 0) {
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                } else {
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } else {
                this.dispose();

            }
        }

    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInserirActionPerformed
        pilha.push(this.jTextFieldNameProperties.getText());
        JenaOWLModel jena = OntologyExtractor.Instance().getJena();
        String fileName = "propriedade.owl";
        String fileNameRep = "propriedade.repository";

        Collection erros = new ArrayList();
        File file = new File(fileName);
        jena.save(file.toURI(), FileUtils.langXMLAbbrev, erros);

        InputStream is;
        try {
            is = new FileInputStream(file);
            try {
                jena = ProtegeOWL.createJenaOWLModelFromInputStream(is);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
        }
        OWLModel modelo = jena;
        pilha.push(modelo);
        file.delete();
        File fileRepository = new File(fileNameRep);
        fileRepository.delete();

        if (!this.jTextFieldNameProperties.getText().equals("")) {
            if (this.hasProperties(this.jTextFieldNameProperties.getText()) == false) {
                Semaforo.Instance().P();
                OWLObjectProperty propriedade = owlModel.createOWLObjectProperty(this.jTextFieldNameProperties.getText());

                Object[] ranges = this.jList1.getSelectedValues();
                if (ranges != null) {
                    for (int i = 0; i < ranges.length; i++) {
                        OWLNamedClass rangeClass = owlModel.getOWLNamedClass((String) ranges[i]);
                        propriedade.addUnionRangeClass(rangeClass);
                    }
                }

                Object[] dommains = this.jList2.getSelectedValues();
                if (dommains != null) {
                    for (int i = 0; i < dommains.length; i++) {
                        OWLNamedClass domaminClass = owlModel.getOWLNamedClass((String) dommains[i]);
                        propriedade.addUnionDomainClass(domaminClass);
                    }
                }
                Semaforo.Instance().V();
                this.jTextFieldNameProperties.setText("");
                if (ranges.length > 0 && dommains.length > 0) {
                    QuickMessageDisplay.ShowSuccessMsg("Propriedade Inserida com Âmbito e Domínio!", 1200);
                } else {
                    if (ranges.length > 0 && dommains.length == 0) {
                        QuickMessageDisplay.ShowInformationMsg("Propriedade Inserida Sem Domínio!", 1200);
                    } else {
                        QuickMessageDisplay.ShowInformationMsg("Propriedade Inserida Sem Âmbito!", 1200);
                    }
                }
            } else {
                QuickMessageDisplay.ShowErrorMsg("Já Existe Esta Propriedade!", 1200);
                pilha.pop();
                pilha.pop();
            }
        } else {
            QuickMessageDisplay.ShowErrorMsg("Valor para a propriedade inválido!", 1200);
            pilha.pop();
            pilha.pop();
        }
        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        recarregarOntologia();
        this.carregarSistema();

    }//GEN-LAST:event_jButtonInserirActionPerformed

    private void pb_help_fij1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pb_help_fij1MouseClicked
        QuickMessageDisplay.ShowOkDialog(QuickMessageDisplay.MSG_INFORMATION,
                "<html><b> Propriedade: </b><font style=\"font-weight:100\"><br>"
                + "<br>As propriedades definem as relações possíveis de serem "
                + "<br>criadas com uma determinada classe ou um grupo de classe. "
                + "<br>O âmbito é onde a propriedade irá atuar."
                + "<br>O domínio defini quais classes pertencem esta propriedade."
                + "<br>Segurando Ctrl é possível escolher uma ou mais classes"
                + "<br>dentro de ambas as listas. Por convenção inicia-se a "
                + "<br>palavra da propriedade com letra minúscula");
    }//GEN-LAST:event_pb_help_fij1MouseClicked

    private void jButtonDesfazerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDesfazerActionPerformed
        if (pilha.isEmpty()) {
        } else {
            //desempilhando modelo
            //desempilhando nome
            OWLModel model = (OWLModel) pilha.pop();
            String nome = (String) pilha.pop();

            this.jTextFieldNameProperties.setText(nome);
            //colocando o modelo pra quem chamou
            this.ontologyLearningTree.setModel(model);
            //carregando o modelo no sistema
            this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
            //colocando no sistema o modelo
            this.owlModel = model;

            recarregarOntologia();
            this.carregarSistema();
        }
    }//GEN-LAST:event_jButtonDesfazerActionPerformed

    /**
     * @param args the command line arguments
     */
    private void montarLista() {
        Collection<OWLNamedClass> listaNamedClasses = owlModel.getUserDefinedOWLNamedClasses();
        OWLNamedClass thing = owlModel.getOWLThingClass();
        Iterator iterador = listaNamedClasses.iterator();
        String str = "";
        while (iterador.hasNext()) {
            OWLNamedClass classe = (OWLNamedClass) iterador.next();
            modeloList1.addElement(classe.getName());
            modeloList2.addElement(classe.getName());
            str = classe.getName() + " " + str;
        }
        this.classNames = str.split(" ");
    }

    private void montarPropriedades() {
        LinkedList<OWLObjectProperty> obtainedObjProp = new LinkedList<OWLObjectProperty>();
        Collection<OWLObjectProperty> list_PropObj = owlModel.getUserDefinedOWLObjectProperties();
        Iterator<OWLObjectProperty> it_ObjProp = list_PropObj.iterator();
        String str = "";
        while (it_ObjProp.hasNext()) {
            //Obter a Propriedade de objeto corrente na iteração
            OWLObjectProperty objProp = it_ObjProp.next();
            str = objProp.getName() + " " + str;
        }
        this.classeProperties = str.split(" ");
    }

    private boolean hasProperties(String nameProperties) {
        for (int i = 0; i < this.classeProperties.length; i++) {
            if (nameProperties.equals(classeProperties[i])) {
                return true;
            }
        }
        return false;
    }

    private void recarregarOntologia() {
        DefaultTreeModel modelTreeCarregada = OntologyExtractor.Instance().BuildTreeModel();
        this.jTreeOntologia.setModel(modelTreeCarregada);
    }

    private void carregarSistema() {
        this.jList1.setModel(new DefaultListModel());
        this.modeloList1 = (DefaultListModel) this.jList1.getModel();
        this.jList2.setModel(new DefaultListModel());
        this.modeloList2 = (DefaultListModel) this.jList2.getModel();
        montarLista();
        montarPropriedades();

        recarregarOntologia();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonDesfazer;
    private javax.swing.JButton jButtonInserir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JList jList1;
    private javax.swing.JList jList2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextFieldNameProperties;
    private javax.swing.JTree jTreeOntologia;
    private javax.swing.JLabel pb_help_fij1;
    // End of variables declaration//GEN-END:variables
}
