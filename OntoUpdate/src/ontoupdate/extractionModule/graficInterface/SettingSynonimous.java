package ontoupdate.extractionModule.graficInterface;

import com.hp.hpl.jena.util.FileUtils;
import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.RDFIndividual;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Stack;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import ontoupdate.agentModule.OntologyExtractor;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;
import ontoupdate.extractionModule.OntologyConceptualClustering;
import ontoupdate.resources.ResourceManager;
import ontoupdate.specialComponents.CustomIconsTreeCellRenderer;
import ontoupdate.util.Logger;
import ontoupdate.util.Semaforo;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public class SettingSynonimous extends javax.swing.JFrame {

    private Stack pilha = new Stack();
    private OWLModel owlModel = null;
    private DefaultListModel modeloListClasse;
    private DefaultListModel modeloListIndividual;
    private OntologyLearningTree ontologyLearningTree;
    private String nameSinonimo = null;

    /** Creates new form SettingSynonimous */
    public SettingSynonimous(String nameSinonimo, OWLModel model, OntologyLearningTree ontologyLearningTree) {

        initComponents();
        ontoupdate.util.DisplayUtils.CenterGuiComponent(this);
        if (model != null) {

            CustomIconsTreeCellRenderer customIconsTreeCellRenderer = new CustomIconsTreeCellRenderer();
            customIconsTreeCellRenderer.AddImagesToRender("ONTOLOGY", ResourceManager.GetImageIcon("IMG_TREE_ONTOLOGY"),
                    "FAMILY_CLASS", ResourceManager.GetImageIcon("IMG_TREE_FAMILY_CLASS"),
                    "REGEX", ResourceManager.GetImageIcon("IMG_TREE_REGEX"),
                    "WEIGHT", ResourceManager.GetImageIcon("IMG_TREE_WEIGHT"),
                    "DEEP", ResourceManager.GetImageIcon("IMG_TREE_DEEP"),
                    "MAIN_TERM", ResourceManager.GetImageIcon("IMG_TREE_MAIN_TERM"),
                    "SYNONYMS", ResourceManager.GetImageIcon("IMG_TREE_SYNONYMS"),
                    "VERBS", ResourceManager.GetImageIcon("IMG_TREE_VERBS"),
                    "INSTANCE", ResourceManager.GetImageIcon("IMG_TREE_INSTANCE"),
                    "CLASS", ResourceManager.GetImageIcon("IMG_TREE_CLASS"),
                    "PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_PROPERTY"),
                    "INVERSE_PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_INVERSE_PROPERTY"),
                    "DOMAIN", ResourceManager.GetImageIcon("IMG_TREE_DOMAIN"),
                    "RANGE", ResourceManager.GetImageIcon("IMG_TREE_RANGE"),
                    "TOTAL_SCOPE", ResourceManager.GetImageIcon("IMG_TREE_TOTAL_SCOPE"),
                    "SCOPE", ResourceManager.GetImageIcon("IMG_TREE_SCOPE"),
                    "CONCEPT", ResourceManager.GetImageIcon("IMG_TREE_CONCEPT"),
                    "DESCRIPTION", ResourceManager.GetImageIcon("IMG_TREE_DESCRIPTION"),
                    "CLUSTER", ResourceManager.GetImageIcon("IMG_TREE_CLUSTER"),
                    "LISTCONCEPTS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CONCEPTS"),
                    "LISTCLUSTERS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CLUSTERS"));

            this.jTreeOntologia.setCellRenderer(customIconsTreeCellRenderer);
            if (nameSinonimo != null) {
                this.nameSinonimo = nameSinonimo;
                this.jTextArea1.setText(nameSinonimo);
            }
            this.owlModel = model;
            this.ontologyLearningTree = ontologyLearningTree;
            this.jListClasse.removeAll();
            this.jListIndividuos.removeAll();
            this.jListClasse.setModel(new DefaultListModel());
            this.jListIndividuos.setModel(new DefaultListModel());
            modeloListClasse = (DefaultListModel) this.jListClasse.getModel();
            modeloListIndividual = (DefaultListModel) this.jListIndividuos.getModel();
            this.jListClasse.setEnabled(false);
            this.jListIndividuos.setEnabled(false);

            this.jTreeOntologia.setModel(this.ontologyLearningTree.jTreeOntologyCarregada.getModel());
            this.montarListClass();
            this.montarListIndividual();

        } else {
            QuickMessageDisplay.ShowErrorMsg("Não possui Ontologia carregada no sistema!", 2000);
            this.dispose();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListClasse = new javax.swing.JList();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListIndividuos = new javax.swing.JList();
        jButtonInserir = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        pb_help_fij = new javax.swing.JLabel();
        jRadioButtonClasse = new javax.swing.JRadioButton();
        jRadioButtonIndividuo = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTreeOntologia = new javax.swing.JTree();
        jButtonDesfazer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/SAME.png"))); // NOI18N
        jLabel1.setText("            Inserir Sinônimos");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextArea1FocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(jTextArea1);

        jLabel2.setText("Sinônimos (Separados por vírgula)");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Escolha Classe"));

        jListClasse.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jListClasse);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Escolha Indivíduo"));

        jListIndividuos.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jListIndividuos);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 327, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );

        jButtonInserir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INSERT.png"))); // NOI18N
        jButtonInserir.setText("Inserir");
        jButtonInserir.setEnabled(false);
        jButtonInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInserirActionPerformed(evt);
            }
        });

        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/TICK_NO.png"))); // NOI18N
        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        pb_help_fij.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INFO_MENOR.png"))); // NOI18N
        pb_help_fij.setToolTipText("Mais Informações");
        pb_help_fij.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pb_help_fijMouseClicked(evt);
            }
        });

        jRadioButtonClasse.setText("Classe");
        jRadioButtonClasse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonClasseActionPerformed(evt);
            }
        });

        jRadioButtonIndividuo.setText("Indivíduo");
        jRadioButtonIndividuo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonIndividuoActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ontologia Carregada"));

        jScrollPane4.setViewportView(jTreeOntologia);

        jButtonDesfazer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/UNDO.png"))); // NOI18N
        jButtonDesfazer.setText("Desfazer");
        jButtonDesfazer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDesfazerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(17, 17, 17)
                .add(jButtonDesfazer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 140, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonDesfazer))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabel2)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(pb_help_fij))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jRadioButtonClasse)
                        .add(33, 33, 33)
                        .add(jRadioButtonIndividuo)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 334, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(3, 3, 3)
                                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(4, 4, 4)
                                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .addContainerGap()
                                .add(jButtonInserir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jButtonCancelar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(7, 7, 7))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 461, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(pb_help_fij))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(10, 10, 10)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jRadioButtonClasse)
                    .add(jRadioButtonIndividuo))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButtonCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jButtonInserir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInserirActionPerformed

        pilha.push(this.jTextArea1.getText());


        JenaOWLModel jena = OntologyExtractor.Instance().getJena();
        String fileName = "sinonimo.owl";
        String fileNameRep = "sinonimo.repository";

        Collection erros = new ArrayList();
        File file = new File(fileName);
        jena.save(file.toURI(), FileUtils.langXMLAbbrev, erros);

        InputStream is;
        try {
            is = new FileInputStream(file);
            try {
                jena = ProtegeOWL.createJenaOWLModelFromInputStream(is);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
        }

        OWLModel modelo = jena;
        pilha.push(modelo);
        file.delete();
        File fileRepository = new File(fileNameRep);
        fileRepository.delete();


        if (this.jRadioButtonClasse.isSelected()) {

            if (this.jListClasse.getSelectedValue() != null) {
                String cls = (String) jListClasse.getSelectedValue();
                OWLNamedClass classe = owlModel.getOWLNamedClass(cls);
                RDFResource owlResource = classe;
                String prop_sinonimo = "";
                try {
                    prop_sinonimo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("Sinônimo"));
                    if (prop_sinonimo == null) {
                        prop_sinonimo = "";
                    }
                    String[] novosSin = this.jTextArea1.getText().split(",");
                    prop_sinonimo = mesclarSinonimos(novosSin, prop_sinonimo);
                    owlResource.setPropertyValue(owlModel.getRDFProperty("Sinônimo"), prop_sinonimo);
                    QuickMessageDisplay.ShowSuccessMsg("Sinônimos adicionado a classe com sucesso", 1200);
                    setInsered();
                } catch (IllegalArgumentException ex) {
                    QuickMessageDisplay.ShowErrorMsg("Ontologia carregada no sistema não possui propriedade Sinônimo", 1200);
                    Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'Sinônimo' não existe na ontologia selecionada!");
                    pilha.pop();
                    pilha.pop();
                }
            } else {
                QuickMessageDisplay.ShowErrorMsg("Não possui valor selecionado na lista", 1200);
                pilha.pop();
                pilha.pop();
            }
        } else {
            if (this.jRadioButtonIndividuo.isSelected()) {
                if (this.jListIndividuos.getSelectedValue() != null) {
                    String cls = (String) jListIndividuos.getSelectedValue();
                    RDFIndividual individual = owlModel.getRDFIndividual(cls);
                    RDFResource owlResource = individual;
                    String prop_sinonimo = "";
                    try {
                        prop_sinonimo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("Sinônimo"));
                        if (prop_sinonimo == null) {
                            prop_sinonimo = "";
                        }
                        String[] novosSin = this.jTextArea1.getText().split(",");
                        prop_sinonimo = mesclarSinonimos(novosSin, prop_sinonimo);
                        owlResource.setPropertyValue(owlModel.getRDFProperty("Sinônimo"), prop_sinonimo);
                        QuickMessageDisplay.ShowSuccessMsg("Sinônimos adicionado ao indivíduo com sucesso", 1200);
                        setInsered();
                    } catch (IllegalArgumentException ex) {
                        QuickMessageDisplay.ShowErrorMsg("Ontologia carregada no sistema não possui propriedade Sinônimo", 1200);
                        Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'Sinônimo' não existe na ontologia selecionada!");
                        pilha.pop();
                        pilha.pop();
                    }
                } else {
                    QuickMessageDisplay.ShowErrorMsg("Não possui valor selecionado na lista", 1200);
                    pilha.pop();
                    pilha.pop();
                }
            } else {
                QuickMessageDisplay.ShowErrorMsg("Não possui valor selecionado na lista ou lista ativada", 1200);
                pilha.pop();
                pilha.pop();
            }
        }

        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        recarregarOntologia();
        this.carregarSistema();
    }//GEN-LAST:event_jButtonInserirActionPerformed

    private void pb_help_fijMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pb_help_fijMouseClicked
        QuickMessageDisplay.ShowOkDialog(QuickMessageDisplay.MSG_INFORMATION,
                "<html><b> Sinônimo de uma Classe ou Indivíduo </b><font style=\"font-weight:100\"><br>"
                + "<br>Para evitar a criação de classes ou indivíduos que fazem "
                + "<br>a mesma referência, usa-se sinônimos. Para a padronização"
                + "<br>é recomendável, uso de letras minúsculas. Adicionando "
                + "<br>sinônimos que ja existem, estes serão ignorados ");
}//GEN-LAST:event_pb_help_fijMouseClicked

    private void jRadioButtonClasseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonClasseActionPerformed
        if (this.jRadioButtonClasse.isSelected()) {
            this.jRadioButtonIndividuo.setSelected(false);
            this.jListIndividuos.setEnabled(false);
            this.jListClasse.setEnabled(true);
        }
    }//GEN-LAST:event_jRadioButtonClasseActionPerformed

    private void jRadioButtonIndividuoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonIndividuoActionPerformed
        if (this.jRadioButtonIndividuo.isSelected()) {
            this.jRadioButtonClasse.setSelected(false);
            this.jListClasse.setEnabled(false);
            this.jListIndividuos.setEnabled(true);
        }
    }//GEN-LAST:event_jRadioButtonIndividuoActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        int saida = JOptionPane.showConfirmDialog(this, "Recarregar as mudanças no sistema?");
        if (saida == 0) {
            try {
                Semaforo.Instance().P();
                OntologyExtractor.Instance().Reload(owlModel);
                Semaforo.Instance().V();
                int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);;
                if(saida1 == 0){
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }else{
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } catch (Exception ex) {
                Semaforo.Instance().V();
                java.util.logging.Logger.getLogger(SettingClass.class.getName()).log(Level.SEVERE, null, ex);
                this.dispose();
            }
        } else {
            if (saida == 1) {
                 int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);
                if (saida1 == 0) {
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                } else {
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } else {
                this.dispose();

            }
        }

    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jTextArea1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextArea1FocusLost
        if (!this.jTextArea1.getText().equals("")) {
            this.jButtonInserir.setEnabled(true);
        } else {
            this.jButtonInserir.setEnabled(false);
        }
    }//GEN-LAST:event_jTextArea1FocusLost

    private void jButtonDesfazerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDesfazerActionPerformed
        if (pilha.isEmpty()) {
        } else {
            //desempilhando modelo
            //desempilhando nome
            OWLModel model = (OWLModel) pilha.pop();
            String nome = (String) pilha.pop();

            this.jTextArea1.setText(nome);
            //colocando o modelo pra quem chamou
            this.ontologyLearningTree.setModel(model);
            //carregando o modelo no sistema
            this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
            //colocando no sistema o modelo
            this.owlModel = model;

            recarregarOntologia();
            this.carregarSistema();
        }
}//GEN-LAST:event_jButtonDesfazerActionPerformed

    private void montarListIndividual() {

        Collection<RDFIndividual> listaNamedIndividuos = owlModel.getUserDefinedRDFIndividuals(false);
        Iterator iterador = listaNamedIndividuos.iterator();
        while (iterador.hasNext()) {
            RDFIndividual classe = (RDFIndividual) iterador.next();
            this.modeloListIndividual.addElement(classe.getName());
        }
    }

    private void montarListClass() {
        Collection<OWLNamedClass> listaNamedClasses = owlModel.getUserDefinedOWLNamedClasses();
        Iterator iterador = listaNamedClasses.iterator();
        while (iterador.hasNext()) {
            OWLNamedClass classe = (OWLNamedClass) iterador.next();
            this.modeloListClasse.addElement(classe.getName());
        }
    }

    private String mesclarSinonimos(String[] lista1, String lista2) {
        String[] wordsLista2 = lista2.split(",");
        for (int i = 0; i < lista1.length; i++) {
            String string1 = lista1[i].toLowerCase().trim();
            boolean flag = false;
            for (int j = 0; j < wordsLista2.length; j++) {
                String string2 = wordsLista2[j].toLowerCase().trim();
                if (string1.equals(string2)) {
                    flag = true;
                }
            }
            if (flag == false) {
                lista2 = lista2 + ", " + string1;
            }
        }
        return lista2;
    }

    private void setInsered() {

        this.jTextArea1.setText("");
        this.jRadioButtonClasse.setSelected(false);
        this.jRadioButtonIndividuo.setSelected(false);
        this.jListClasse.setEnabled(false);
        this.jListIndividuos.setEnabled(false);
        this.jButtonInserir.setEnabled(false);
    }

    private void recarregarOntologia() {
        DefaultTreeModel modelTreeCarregada = OntologyExtractor.Instance().BuildTreeModel();
        this.jTreeOntologia.setModel(modelTreeCarregada);
    }

    private void carregarSistema() {
        this.jListClasse.setModel(new DefaultListModel());
        this.modeloListClasse = (DefaultListModel) this.jListClasse.getModel();
        montarListClass();
        this.jListIndividuos.setModel(new DefaultListModel());
        this.modeloListClasse = (DefaultListModel) this.jListIndividuos.getModel();
        montarListIndividual();

        recarregarOntologia();
    }
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonDesfazer;
    private javax.swing.JButton jButtonInserir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jListClasse;
    private javax.swing.JList jListIndividuos;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButton jRadioButtonClasse;
    private javax.swing.JRadioButton jRadioButtonIndividuo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTree jTreeOntologia;
    private javax.swing.JLabel pb_help_fij;
    // End of variables declaration//GEN-END:variables
}
