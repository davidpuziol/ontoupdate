package ontoupdate.extractionModule.graficInterface;

import com.hp.hpl.jena.util.FileUtils;
import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import edu.stanford.smi.protegex.owl.model.RDFIndividual;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import ontoupdate.agentModule.OntologyExtractor;
import ontoupdate.agentModule.graficInterface.ServerProxyForm;
import ontoupdate.extractionModule.OntologyConceptualClustering;
import ontoupdate.resources.ResourceManager;
import ontoupdate.specialComponents.CustomIconsTreeCellRenderer;
import ontoupdate.util.Logger;
import ontoupdate.util.Semaforo;
import ontoupdate.util.notifications.QuickMessageDisplay;

/**
 *
 * @author David Puziol Prata
 */
public class SettingInstances extends javax.swing.JFrame {

    private Stack pilha = new Stack();
    private String[] classNames = null;
    private String[] individualsNames = null;
    private String[] individuoProperties;
    private String nameIndividuo = null;
    private String nameParent = null;
    private OWLModel owlModel = null;
    private DefaultListModel modeloList;
    private DefaultComboBoxModel modeloCombo = null;
    private LinkedList<LinkedList<String>> relacionamentoClasse = new LinkedList();
    private LinkedList<LinkedList<String>> relacionamentoInstance = new LinkedList();
    private RDFIndividual novoIndividuo = null;
    private OntologyLearningTree ontologyLearningTree;

    /** Creates new form SettingInstances */
    public SettingInstances(String instance, OWLModel model, OntologyLearningTree ontologyLearningTree) {
        initComponents();
        ontoupdate.util.DisplayUtils.CenterGuiComponent(this);
        if (model != null) {

            CustomIconsTreeCellRenderer customIconsTreeCellRenderer = new CustomIconsTreeCellRenderer();
            customIconsTreeCellRenderer.AddImagesToRender("ONTOLOGY", ResourceManager.GetImageIcon("IMG_TREE_ONTOLOGY"),
                    "FAMILY_CLASS", ResourceManager.GetImageIcon("IMG_TREE_FAMILY_CLASS"),
                    "REGEX", ResourceManager.GetImageIcon("IMG_TREE_REGEX"),
                    "WEIGHT", ResourceManager.GetImageIcon("IMG_TREE_WEIGHT"),
                    "DEEP", ResourceManager.GetImageIcon("IMG_TREE_DEEP"),
                    "MAIN_TERM", ResourceManager.GetImageIcon("IMG_TREE_MAIN_TERM"),
                    "SYNONYMS", ResourceManager.GetImageIcon("IMG_TREE_SYNONYMS"),
                    "VERBS", ResourceManager.GetImageIcon("IMG_TREE_VERBS"),
                    "INSTANCE", ResourceManager.GetImageIcon("IMG_TREE_INSTANCE"),
                    "CLASS", ResourceManager.GetImageIcon("IMG_TREE_CLASS"),
                    "PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_PROPERTY"),
                    "INVERSE_PROPERTY", ResourceManager.GetImageIcon("IMG_TREE_INVERSE_PROPERTY"),
                    "DOMAIN", ResourceManager.GetImageIcon("IMG_TREE_DOMAIN"),
                    "RANGE", ResourceManager.GetImageIcon("IMG_TREE_RANGE"),
                    "TOTAL_SCOPE", ResourceManager.GetImageIcon("IMG_TREE_TOTAL_SCOPE"),
                    "SCOPE", ResourceManager.GetImageIcon("IMG_TREE_SCOPE"),
                    "CONCEPT", ResourceManager.GetImageIcon("IMG_TREE_CONCEPT"),
                    "DESCRIPTION", ResourceManager.GetImageIcon("IMG_TREE_DESCRIPTION"),
                    "CLUSTER", ResourceManager.GetImageIcon("IMG_TREE_CLUSTER"),
                    "LISTCONCEPTS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CONCEPTS"),
                    "LISTCLUSTERS", ResourceManager.GetImageIcon("IMG_TREE_LIST_CLUSTERS"));

            this.jTreeOntologia.setCellRenderer(customIconsTreeCellRenderer);
            this.jTreeOntologia.setCellRenderer(customIconsTreeCellRenderer);
            if (instance != null) {
                this.nameIndividuo = instance;

                if (nameIndividuo != null) {
                    this.jTextFieldNamedIndividuo.setText(instance);
                    this.jButtonInserir.setEnabled(true);
                } else {
                    this.jButtonInserir.setEnabled(false);
                }
                // colocar a expressao regular no local dela
            }
            this.owlModel = model;
            this.ontologyLearningTree = ontologyLearningTree;
            this.jList1.removeAll();
            this.jList1.setModel(new DefaultListModel());
            modeloList = (DefaultListModel) this.jList1.getModel();
            montarLista();
            montarIndividuos();

            tree.setModel(new DefaultTreeModel(montarArvoreClasses()));
            tree.setCellRenderer(new CheckRenderer("IMG_TREE_CLASS"));
            tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            tree.putClientProperty("JTree.lineStyle", "Angled");
            tree.addMouseListener(new NodeSelectionListener(tree));

            tree1.setModel(new DefaultTreeModel(montarArvoreIndividuo()));
            tree1.setCellRenderer(new CheckRenderer("IMG_TREE_INSTANCE"));
            tree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            tree1.putClientProperty("JTree.lineStyle", "Angled");
            tree1.addMouseListener(new NodeSelectionListener(tree1));

            tree.repaint();
            tree1.repaint();

            this.jComboBoxPropriedades.removeAllItems();
            montarCombo();
            modeloCombo = new DefaultComboBoxModel(this.individuoProperties);
            this.jComboBoxPropriedades.setModel(modeloCombo);
            this.jComboBoxPropriedades.setVisible(true);

            montarRelacionamentosClasse();
            montarRelacionamentosIndividuos();

            this.jButtonRelacionar.setEnabled(false);
            this.jComboBoxPropriedades.setEnabled(false);
            this.jTreeOntologia.setModel(this.ontologyLearningTree.jTreeOntologyCarregada.getModel());

        } else {
            QuickMessageDisplay.ShowErrorMsg("Não possui Ontologia carregada no sistema!", 2000);
            this.dispose();
        }
    }

    class NodeSelectionListener extends MouseAdapter {

        JTree tree;

        NodeSelectionListener(JTree tree) {
            this.tree = tree;
        }

        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            int row = tree.getRowForLocation(x, y);
            TreePath path = tree.getPathForRow(row);
            //TreePath  path = tree.getSelectionPath();
            if (path.getParentPath() != null) {
                if (path != null) {
                    CheckNode node = (CheckNode) path.getLastPathComponent();
                    boolean isSelected = !(node.isSelected());
                    node.setSelected(isSelected);
                    CheckNode root = (CheckNode) node.getParent();
                    System.out.println(root);
                    if (root.toString().equals("Classes")) {
                        String str = relacionamentoClasse.get(jComboBoxPropriedades.getSelectedIndex()).get(row);
                        if (isSelected == true) {
                            relacionamentoClasse.get(jComboBoxPropriedades.getSelectedIndex()).set(row, "yes");
                        } else {
                            relacionamentoClasse.get(jComboBoxPropriedades.getSelectedIndex()).set(row, "no");
                        }
                        if (node.getSelectionMode() == CheckNode.DIG_IN_SELECTION) {
                            if (isSelected) {
                                tree.expandPath(path);
                            } else {
                                tree.collapsePath(path);
                            }
                        }
                    } else {
                        String str = relacionamentoInstance.get(jComboBoxPropriedades.getSelectedIndex()).get(row);
                        if (isSelected == true) {
                            relacionamentoInstance.get(jComboBoxPropriedades.getSelectedIndex()).set(row, "yes");
                        } else {
                            relacionamentoInstance.get(jComboBoxPropriedades.getSelectedIndex()).set(row, "no");
                        }
                        if (node.getSelectionMode() == CheckNode.DIG_IN_SELECTION) {
                            if (isSelected) {
                                tree.expandPath(path);
                            } else {
                                tree.collapsePath(path);
                            }
                        }
                    }
                    ((DefaultTreeModel) tree.getModel()).nodeChanged(node);
                    if (row == 0) {
                        tree.revalidate();
                        tree.repaint();
                    }
                }
            }
        }
    }

    class ButtonActionListener implements ActionListener {

        CheckNode root;
        JTextArea textArea;

        ButtonActionListener(final CheckNode root,
                final JTextArea textArea) {
            this.root = root;
            this.textArea = textArea;
        }

        public void actionPerformed(ActionEvent e) {
            Enumeration enume = root.breadthFirstEnumeration();
            while (enume.hasMoreElements()) {
                CheckNode node = (CheckNode) enume.nextElement();
                if (node.isSelected()) {
                    TreeNode[] nodes = node.getPath();
                    textArea.append("\n" + nodes[0].toString());
                    for (int i = 1; i < nodes.length; i++) {
                        textArea.append("/" + nodes[i].toString());
                    }
                }
            }
        }
    }

    private void montarLista() {
        Collection<OWLNamedClass> listaNamedClasses = owlModel.getUserDefinedOWLNamedClasses();
        Iterator iterador = listaNamedClasses.iterator();
        String str = "";
        while (iterador.hasNext()) {
            OWLNamedClass classe = (OWLNamedClass) iterador.next();
            modeloList.addElement(classe.getName());
            str = classe.getName() + " " + str;
        }
        this.classNames = str.split(" ");
    }

    private DefaultMutableTreeNode montarArvoreIndividuo() {

        CheckNode[] nodes = new CheckNode[this.individualsNames.length];
        CheckNode thing = new CheckNode("Indivíduos");
        for (int i = 0; i < individualsNames.length; i++) {
            nodes[i] = new CheckNode(individualsNames[i]);
            thing.add(nodes[i]);
        }
        return thing;
    }

    private DefaultMutableTreeNode montarArvoreClasses() {
        CheckNode[] nodes = new CheckNode[this.classNames.length];
        CheckNode thing = new CheckNode("Classes");
        for (int i = 0; i < classNames.length; i++) {
            nodes[i] = new CheckNode(classNames[i]);
            thing.add(nodes[i]);
        }
        return thing;
    }

    private void montarIndividuos() {
        Collection<RDFIndividual> listaNamedIndividuos = owlModel.getUserDefinedRDFIndividuals(false);
        Iterator iterador = listaNamedIndividuos.iterator();
        String str = "";
        while (iterador.hasNext()) {
            RDFIndividual individuo = (RDFIndividual) iterador.next();
            if (individuo != null) {
                str = individuo.getName() + " " + str;
            }
        }
        if (str != null) {
            this.individualsNames = str.split(" ");
        }
    }

    private void montarCombo() {
        LinkedList<OWLObjectProperty> obtainedObjProp = new LinkedList<OWLObjectProperty>();
        Collection<OWLObjectProperty> list_PropObj = owlModel.getUserDefinedOWLObjectProperties();
        Iterator<OWLObjectProperty> it_ObjProp = list_PropObj.iterator();
        String str = "Disjoin ";
        while (it_ObjProp.hasNext()) {
            //Obter a Propriedade de objeto corrente na iteração
            OWLObjectProperty objProp = it_ObjProp.next();
            str = objProp.getName() + " " + str;
        }
        this.individuoProperties = str.split(" ");
    }

    private void montarRelacionamentosIndividuos() {
        this.relacionamentoInstance.clear();
        for (int i = 0; i < this.individuoProperties.length; i++) {
            LinkedList relacao = new LinkedList();
            CheckNode root = (CheckNode) tree1.getModel().getRoot();
            for (int j = 0; j < this.individualsNames.length; j++) {
                String str;
                CheckNode checkNode = (CheckNode) root.getChildAt(j);
                if (checkNode.isSelected) {
                    str = "yes";
                } else {
                    str = "no";
                }
                relacao.add(str);
            }
            this.relacionamentoInstance.add(relacao);
        }
    }

    private void montarRelacionamentosClasse() {
        this.relacionamentoClasse.clear();
        ;
        for (int i = 0; i < this.individuoProperties.length; i++) {
            LinkedList relacao = new LinkedList();
            CheckNode root = (CheckNode) tree.getModel().getRoot();
            for (int j = 0; j < this.classNames.length; j++) {
                String str;
                CheckNode checkNode = (CheckNode) root.getChildAt(j);
                if (checkNode.isSelected) {
                    str = "yes";
                } else {
                    str = "no";
                }
                relacao.add(str);
            }
            this.relacionamentoClasse.add(relacao);
        }
    }

    private boolean checkIndividuoInOntology() {
        Collection<RDFIndividual> listaNamedIndividuos = owlModel.getUserDefinedRDFIndividuals(false);
        String nameindividuo = this.nameIndividuo.toLowerCase();
        Iterator iterador = listaNamedIndividuos.iterator();
        while (iterador.hasNext()) {
            RDFIndividual individuo = (RDFIndividual) iterador.next();
            String individuoUser = individuo.getName().toLowerCase();
            if (nameindividuo.equals(individuoUser)) {
                return true;
            }
        }
        return false;
    }

    private void editIndividuo() {
        RDFIndividual individuo = owlModel.getRDFIndividual(this.jTextFieldNamedIndividuo.getText());
        RDFResource owlResource = owlModel.getRDFIndividual(this.jTextFieldNamedIndividuo.getText());
        String prop_sinonimo = null;
        String prop_verbo = null;
        try {
            prop_sinonimo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("Sinônimo"));
            this.jLabelErroSin.setText("Erro: Sem Erro");
            this.jLabelErroSin.setIcon(ResourceManager.GetImageIcon("IMG_NOWARNING"));
        } catch (IllegalArgumentException ex) {
            prop_sinonimo = null;
            this.jLabelErroSin.setText("Ërro: Propriedade não criada");
            this.jLabelErroSin.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
            this.jLabelErroSin.setForeground(Color.RED);
            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'Sinônimo' não existe na ontologia selecionada!");
        }
        try {
            prop_verbo = (String) owlResource.getPropertyValue(owlModel.getOWLProperty("VerboRelacionado"));
            this.jLabelPropVerb.setText("Ërro: Sem Erro");
            this.jLabelPropVerb.setIcon(ResourceManager.GetImageIcon("IMG_NOWARNING"));
        } catch (IllegalArgumentException ex) {
            prop_verbo = null;
            this.jLabelPropVerb.setText("Ërro: Ontologia não possui propriedade VerboRelacionado");
            this.jLabelPropVerb.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
            this.jLabelPropVerb.setForeground(Color.RED);
            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.ERROR, this, "Annotation Property 'VerboRelacionado' não existe na ontologia selecionada!");
        }

        if (prop_sinonimo != null) {
            this.jTextAreaSinonimos.setText(prop_sinonimo);
        }

        if (prop_verbo != null) {
            this.jTextAreaVerbos.setText(prop_verbo);
        }

        //montar relacao

    }

    private void recarregarOntologia() {
        DefaultTreeModel modelTreeCarregada = OntologyExtractor.Instance().BuildTreeModel();
        this.jTreeOntologia.setModel(modelTreeCarregada);
    }

    private void carregarSistema() {
        this.jList1.setModel(new DefaultListModel());
        modeloList = (DefaultListModel) this.jList1.getModel();
        this.montarLista();

        this.montarIndividuos();
        tree.setModel(new DefaultTreeModel(montarArvoreClasses()));
        tree1.setModel(new DefaultTreeModel(this.montarArvoreIndividuo()));


        modeloCombo = new DefaultComboBoxModel(this.individuoProperties);
        this.jComboBoxPropriedades.setModel(modeloCombo);
        montarCombo();
        modeloCombo = new DefaultComboBoxModel(this.individuoProperties);


        montarRelacionamentosClasse();
        montarRelacionamentosIndividuos();
        recarregarOntologia();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();
        jButtonRelacionar = new javax.swing.JButton();
        jComboBoxPropriedades = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tree1 = new javax.swing.JTree();
        jButtonCancelar = new javax.swing.JButton();
        jLabelPropVerb = new javax.swing.JLabel();
        jButtonInserir = new javax.swing.JButton();
        jLabelErroSin = new javax.swing.JLabel();
        pb_help_fij2 = new javax.swing.JLabel();
        pb_help_fij1 = new javax.swing.JLabel();
        pb_help_fij = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldNamedIndividuo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaSinonimos = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaVerbos = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTreeOntologia = new javax.swing.JTree();
        jButtonDesfazer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Relacionamentos"));

        tree.setAutoscrolls(true);
        jScrollPane3.setViewportView(tree);

        jButtonRelacionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/LINK.png"))); // NOI18N
        jButtonRelacionar.setText("Relacionar");
        jButtonRelacionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRelacionarActionPerformed(evt);
            }
        });

        jComboBoxPropriedades.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxPropriedades.setSelectedIndex(1);
        jComboBoxPropriedades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPropriedadesActionPerformed(evt);
            }
        });

        jLabel4.setText("Inserir Propriedades Relacionais");

        jScrollPane5.setViewportView(tree1);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 142, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jScrollPane5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE))
            .add(jPanel1Layout.createSequentialGroup()
                .add(74, 74, 74)
                .add(jButtonRelacionar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 123, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(105, Short.MAX_VALUE))
            .add(jPanel1Layout.createSequentialGroup()
                .add(53, 53, 53)
                .add(jComboBoxPropriedades, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(83, Short.MAX_VALUE))
            .add(jPanel1Layout.createSequentialGroup()
                .add(32, 32, 32)
                .add(jLabel4)
                .addContainerGap(117, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                    .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE))
                .add(18, 18, 18)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jComboBoxPropriedades, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jButtonRelacionar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 44, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/TICK_NO.png"))); // NOI18N
        jButtonCancelar.setText("Sair");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jLabelPropVerb.setText("Erro:");

        jButtonInserir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INSERT.png"))); // NOI18N
        jButtonInserir.setText("Inserir");
        jButtonInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInserirActionPerformed(evt);
            }
        });

        jLabelErroSin.setText("Erro: ");

        pb_help_fij2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INFO_MENOR.png"))); // NOI18N
        pb_help_fij2.setToolTipText("Mais Informações");
        pb_help_fij2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pb_help_fij2MouseClicked(evt);
            }
        });

        pb_help_fij1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INFO_MENOR.png"))); // NOI18N
        pb_help_fij1.setToolTipText("Mais Informações");
        pb_help_fij1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pb_help_fij1MouseClicked(evt);
            }
        });

        pb_help_fij.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/INFO_MENOR.png"))); // NOI18N
        pb_help_fij.setToolTipText("Mais Informações");
        pb_help_fij.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pb_help_fijMouseClicked(evt);
            }
        });

        jLabel1.setText("Nome do Indivíduo");

        jTextFieldNamedIndividuo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldNamedIndividuoFocusLost(evt);
            }
        });
        jTextFieldNamedIndividuo.addHierarchyListener(new java.awt.event.HierarchyListener() {
            public void hierarchyChanged(java.awt.event.HierarchyEvent evt) {
                jTextFieldNamedIndividuoHierarchyChanged(evt);
            }
        });

        jLabel2.setText("Sinônimos ( separados por vírgulas)");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Classe Superior"));

        jScrollPane4.setViewportView(jList1);

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
        );

        jTextAreaSinonimos.setColumns(20);
        jTextAreaSinonimos.setRows(5);
        jTextAreaSinonimos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaSinonimosFocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(jTextAreaSinonimos);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true)));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/OBJECT.png"))); // NOI18N
        jLabel5.setText("                                         Configurações De Novo Indivíduo");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 784, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(244, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel5)
        );

        jLabel3.setText("Verbo ( separacos por vírgulas )");

        jLabel6.setText("Recomendado iniciar com letra minúscula");

        jTextAreaVerbos.setColumns(20);
        jTextAreaVerbos.setRows(5);
        jTextAreaVerbos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaVerbosFocusLost(evt);
            }
        });
        jScrollPane2.setViewportView(jTextAreaVerbos);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ontologia Carregada"));

        jScrollPane6.setViewportView(jTreeOntologia);

        jButtonDesfazer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ontoupdate/resources/UNDO.png"))); // NOI18N
        jButtonDesfazer.setText("Desfazer");
        jButtonDesfazer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDesfazerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(52, Short.MAX_VALUE)
                .add(jButtonDesfazer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(48, 48, 48))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 349, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 15, Short.MAX_VALUE)
                .add(jButtonDesfazer)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(11, 11, 11)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jButtonInserir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 140, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 140, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(layout.createSequentialGroup()
                            .add(jLabel1)
                            .add(28, 28, 28)
                            .add(pb_help_fij))
                        .add(layout.createSequentialGroup()
                            .add(jLabel3)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(pb_help_fij2))
                        .add(jScrollPane2)
                        .add(layout.createSequentialGroup()
                            .add(jLabel2)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(pb_help_fij1))
                        .add(jScrollPane1)
                        .add(jLabelPropVerb, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jLabelErroSin, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jLabel6)
                        .add(jTextFieldNamedIndividuo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 284, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(18, 18, 18)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(pb_help_fij)
                            .add(jLabel1))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel6)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextFieldNamedIndividuo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(21, 21, 21)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(pb_help_fij1))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelErroSin)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel3)
                            .add(pb_help_fij2))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelPropVerb, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButtonCancelar)
                            .add(jButtonInserir))
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonRelacionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRelacionarActionPerformed
        // percorrendo cada posicao da lista de relacionamento
        for (int i = 0; i < this.relacionamentoClasse.size(); i++) {
            LinkedList lista = relacionamentoClasse.get(i);
            String str = this.individuoProperties[i];

            OWLObjectProperty propriedade = owlModel.getOWLObjectProperty(str);

            for (int j = 0; j < this.classNames.length; j++) {
                if (lista.get(j).equals("yes")) {
                    OWLNamedClass classe = owlModel.getOWLNamedClass(classNames[i]);
                    propriedade.addUnionRangeClass(classe);
                    novoIndividuo.setPropertyValue(propriedade, classe);
                }
            }
        }

        for (int i = 0; i < this.relacionamentoClasse.size(); i++) {
            LinkedList lista1 = relacionamentoClasse.get(i);
            String str = this.individuoProperties[i];
            OWLObjectProperty propriedade = owlModel.getOWLObjectProperty(str);
            for (int j = 0; j < this.individualsNames.length; j++) {
                if (lista1.get(j).equals("yes")) {
                    RDFIndividual individuo = owlModel.getRDFIndividual(individualsNames[i]);
                    propriedade.addUnionRangeClass(owlModel.getOWLNamedClass(this.nameParent));
                    novoIndividuo.setPropertyValue(propriedade, individuo);
                }
            }
        }
        QuickMessageDisplay.ShowSuccessMsg("As Relaçoes entre os indivíduos foram processadas com sucesso", 1220);
        this.jTextFieldNamedIndividuo.setText("");
        this.jTextAreaSinonimos.setText("");
        this.jTextAreaVerbos.setText("");
        this.novoIndividuo = null;
        this.jButtonRelacionar.setEnabled(false);
        this.jButtonInserir.setEnabled(true);
        this.jList1.setEnabled(true);
        this.jTextAreaSinonimos.setEnabled(true);
        this.jTextAreaVerbos.setEnabled(true);
        this.jTextFieldNamedIndividuo.setEnabled(true);
        this.jComboBoxPropriedades.setEnabled(false);
        this.tree.setEnabled(false);
        this.tree1.setEnabled(false);
        this.jLabelErroSin.setText("Ërror: ");
        this.jLabelPropVerb.setText("Ërror: ");
        this.jLabelErroSin.setForeground(Color.BLACK);
        this.jLabelPropVerb.setForeground(Color.BLACK);
        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        recarregarOntologia();
        this.carregarSistema();
        this.jList1.setSelectedIndex(0);
}//GEN-LAST:event_jButtonRelacionarActionPerformed

    private void jComboBoxPropriedadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPropriedadesActionPerformed
        try {
            int relacaoInt = this.jComboBoxPropriedades.getSelectedIndex();

            LinkedList relacao = this.relacionamentoClasse.get(relacaoInt);
            CheckNode root = (CheckNode) tree.getModel().getRoot();
            for (int i = 0; i < this.classNames.length; i++) {
                CheckNode checkNode = (CheckNode) root.getChildAt(i);
                String str = (String) relacao.get(i);
                if (str.endsWith("yes")) {
                    checkNode.setSelected(true);
                } else {
                    checkNode.setSelected(false);
                }
            }

            System.out.println("acertando os individuos");
            LinkedList relacao1 = this.relacionamentoInstance.get(relacaoInt);
            CheckNode root1 = (CheckNode) tree1.getModel().getRoot();
            for (int k = 0; k < this.individualsNames.length; k++) {
                CheckNode checkNode1 = (CheckNode) root1.getChildAt(k);
                String str1 = (String) relacao1.get(k);
                if (str1.endsWith("yes")) {
                    checkNode1.setSelected(true);
                } else {
                    checkNode1.setSelected(false);
                }
            }
            tree.repaint();
            tree1.repaint();
        } catch (Exception e) {
        }
}//GEN-LAST:event_jComboBoxPropriedadesActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
        int saida = JOptionPane.showConfirmDialog(this, "Recarregar as mudanças no sistema?");
        if (saida == 0) {
            try {
                Semaforo.Instance().P();
                OntologyExtractor.Instance().Reload(owlModel);
                Semaforo.Instance().V();
                int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);
                if (saida1 == 0) {
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                } else {
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } catch (Exception ex) {
                Semaforo.Instance().V();
                java.util.logging.Logger.getLogger(SettingClass.class.getName()).log(Level.SEVERE, null, ex);
                this.dispose();
            }
        } else {
            if (saida == 1) {
                 int saida1 = JOptionPane.showConfirmDialog(this, "O ontoUpdate já pode descartar a árvore de atualização", null, JOptionPane.WARNING_MESSAGE);
                if (saida1 == 0) {
                    OntologyConceptualClustering.GenerateNewInstance();
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                } else {
                    ServerProxyForm.Instance().setNumTextosLidos(0);
                }
                ontologyLearningTree.jButtonRefreshAprendida.doClick();
                this.dispose();
            } else {
                this.dispose();

            }
        }

}//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInserirActionPerformed
        pilha.push(this.jTextFieldNamedIndividuo.getText());
        pilha.push(this.jTextAreaSinonimos.getText());
        pilha.push(this.jTextAreaVerbos.getText());
        JenaOWLModel jena = OntologyExtractor.Instance().getJena();
        String fileName = "classe.owl";
        String fileNameRep = "classe.repository";

        Collection erros = new ArrayList();
        File file = new File(fileName);
        jena.save(file.toURI(), FileUtils.langXMLAbbrev, erros);

        InputStream is;
        try {
            is = new FileInputStream(file);
            try {
                jena = ProtegeOWL.createJenaOWLModelFromInputStream(is);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(SettingSynonimous.class.getName()).log(Level.SEVERE, null, ex);
        }

        OWLModel modelo = jena;
        pilha.push(modelo);
        file.delete();
        File fileRepository = new File(fileNameRep);
        fileRepository.delete();



        String nameParent = null;
        if (this.jList1.getSelectedValue() != null) {
            nameParent = (String) this.jList1.getSelectedValue();
        }
        RDFIndividual checkName = owlModel.getRDFIndividual(nameIndividuo);
        if (checkName != null) {
            QuickMessageDisplay.ShowErrorMsg("Indivíduo ja existente na ontologia", 1200);
            pilha.pop();
            pilha.pop();
            pilha.pop();
            pilha.pop();
        } else {
            if (this.jList1.getSelectedValue() != null) {
                OWLNamedClass parentClasse = owlModel.getOWLNamedClass(nameParent);
                this.nameParent = nameParent;
                this.novoIndividuo = parentClasse.createRDFIndividual(nameIndividuo);


                RDFResource owlResource = owlModel.getRDFIndividual(nameIndividuo);
                try {
                    if (!this.jTextAreaSinonimos.getText().equals("")) {
                        owlResource.setPropertyValue(owlModel.getRDFProperty("Sinônimo"), this.jTextAreaSinonimos.getText());

                        this.jLabelErroSin.setText("Erro: Sem Erro");
                        this.jLabelErroSin.setIcon(ResourceManager.GetImageIcon("IMG_NOWARNING"));
                    } else {
                        this.jLabelErroSin.setText("Ërro: Propriedade não criada");
                        this.jLabelErroSin.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
                        this.jLabelErroSin.setForeground(Color.RED);
                    }
                } catch (Exception e) {
                    this.jLabelErroSin.setText("Ërro: Ontologia não possui propriedade Sinônimo");
                    this.jLabelErroSin.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
                    this.jLabelErroSin.setForeground(Color.RED);
                }

                try {
                    if (!this.jTextAreaVerbos.getText().equals("")) {
                        owlResource.setPropertyValue(owlModel.getRDFProperty("VerboRelacionado"), this.jTextAreaVerbos.getText());
                        this.jLabelPropVerb.setText("Ërro: Sem Erro");
                        this.jLabelPropVerb.setIcon(ResourceManager.GetImageIcon("IMG_NOWARNING"));
                    } else {
                        this.jLabelPropVerb.setText("Ërro: Propriedade não criada");
                        this.jLabelPropVerb.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
                        this.jLabelPropVerb.setForeground(Color.RED);
                    }
                } catch (Exception e) {
                    this.jLabelPropVerb.setText("Ërro: Ontologia não possui propriedade VerboRelacionado");
                    this.jLabelPropVerb.setIcon(ResourceManager.GetImageIcon("IMG_WARNING"));
                    this.jLabelPropVerb.setForeground(Color.RED);
                }
                this.jButtonRelacionar.setEnabled(true);
                this.tree.setEnabled(true);
                this.tree1.setEnabled(true);
                this.jComboBoxPropriedades.setEnabled(true);

                QuickMessageDisplay.ShowSuccessMsg("Indivíduo inserido com sucesso", 1200);
                this.jButtonInserir.setEnabled(false);
                this.jList1.setEnabled(false);
                this.jTextAreaSinonimos.setEnabled(false);
                this.jTextAreaVerbos.setEnabled(false);
                this.jTextFieldNamedIndividuo.setEnabled(false);

                this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
                recarregarOntologia();
                this.carregarSistema();
            } else {
                QuickMessageDisplay.ShowWarningMsg("Não possui classe pai selecionada", 1200);
            }
        }
}//GEN-LAST:event_jButtonInserirActionPerformed

    private void pb_help_fij2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pb_help_fij2MouseClicked
        QuickMessageDisplay.ShowOkDialog(QuickMessageDisplay.MSG_INFORMATION,
                "<html><b> Verbos do Indivíduo: </b><font style=\"font-weight:100\"><br>"
                + "<br>Os verbos do Indivíduo implicam no relacionamento do indivíduo"
                + "<br>com o restante dos indivíduos na ontologia.");
}//GEN-LAST:event_pb_help_fij2MouseClicked

    private void pb_help_fij1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pb_help_fij1MouseClicked
        QuickMessageDisplay.ShowOkDialog(QuickMessageDisplay.MSG_INFORMATION,
                "<html><b> Sinônimos do Indivíduo: </b><font style=\"font-weight:100\"><br>"
                + "<br>Para evitar a criação de indivíduos parecidos usa-se sinônimos"
                + "<br>para este indivíduo, afim de evitar possíveis intâncias idênticas. "
                + "<br>na ontologia");
}//GEN-LAST:event_pb_help_fij1MouseClicked

    private void pb_help_fijMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pb_help_fijMouseClicked
        QuickMessageDisplay.ShowOkDialog(QuickMessageDisplay.MSG_INFORMATION,
                "<html><b> Nome do Indivíduo: </b><font style=\"font-weight:100\"><br>"
                + "<br>Para uma convenção é utilizado o nome do Indivíduo iniciando"
                + "<br>com letra maiúscula para uma melhor diferenciação. "
                + "<br>É recomendável, mas não é obrigatório.");        // TODO add your handling code here:
}//GEN-LAST:event_pb_help_fijMouseClicked

    private void jTextFieldNamedIndividuoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldNamedIndividuoFocusLost
        this.nameIndividuo = this.jTextFieldNamedIndividuo.getText();
        if (nameIndividuo.equals("")) {
            QuickMessageDisplay.ShowErrorMsg("Nome do indivíduo não escolhido", 1500);
            this.jButtonInserir.setEnabled(false);
        } else {
            if (checkIndividuoInOntology()) {
                QuickMessageDisplay.ShowErrorMsg("Esse indivíduo já existe na ontologia", 1500);
                this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
                int saida = JOptionPane.showConfirmDialog(this, "Deseja Editar?");
                if (saida == 0) {
                    editIndividuo();
                }
                this.jButtonInserir.setEnabled(false);
            } else {
                this.jButtonInserir.setEnabled(true);
            }
        }
}//GEN-LAST:event_jTextFieldNamedIndividuoFocusLost

    private void jTextFieldNamedIndividuoHierarchyChanged(java.awt.event.HierarchyEvent evt) {//GEN-FIRST:event_jTextFieldNamedIndividuoHierarchyChanged
        // TODO add your handling code here:
}//GEN-LAST:event_jTextFieldNamedIndividuoHierarchyChanged

    private void jTextAreaSinonimosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaSinonimosFocusLost
}//GEN-LAST:event_jTextAreaSinonimosFocusLost

    private void jTextAreaVerbosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaVerbosFocusLost
}//GEN-LAST:event_jTextAreaVerbosFocusLost

    private void jButtonDesfazerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDesfazerActionPerformed
        if (pilha.isEmpty()) {
        } else {
            //desempilhando modelo
            //desempilhando nome
            OWLModel model = (OWLModel) pilha.pop();
            String verbos = (String) pilha.pop();
            String sinonimos = (String) pilha.pop();
            String nome = (String) pilha.pop();

            this.jTextFieldNamedIndividuo.setText(nome);
            this.jTextAreaSinonimos.setText(sinonimos);
            this.jTextAreaVerbos.setText(verbos);
            //colocando o modelo pra quem chamou
            this.ontologyLearningTree.setModel(model);
            //carregando o modelo no sistema
            this.ontologyLearningTree.jButtonRefreshCarregada.doClick();
            //colocando no sistema o modelo
            this.owlModel = model;

            recarregarOntologia();
            this.carregarSistema();
        }
    }//GEN-LAST:event_jButtonDesfazerActionPerformed
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonDesfazer;
    private javax.swing.JButton jButtonInserir;
    private javax.swing.JButton jButtonRelacionar;
    private javax.swing.JComboBox jComboBoxPropriedades;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelErroSin;
    private javax.swing.JLabel jLabelPropVerb;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextArea jTextAreaSinonimos;
    private javax.swing.JTextArea jTextAreaVerbos;
    private javax.swing.JTextField jTextFieldNamedIndividuo;
    private javax.swing.JTree jTreeOntologia;
    private javax.swing.JLabel pb_help_fij;
    private javax.swing.JLabel pb_help_fij1;
    private javax.swing.JLabel pb_help_fij2;
    private javax.swing.JTree tree;
    private javax.swing.JTree tree1;
    // End of variables declaration//GEN-END:variables
}
