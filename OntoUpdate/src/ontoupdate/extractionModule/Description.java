/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.extractionModule;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author David Puziol Prata
 */
public class Description {

    private Double weigth;
    private String word;
    private Double numHits;
    private LinkedList<String> similarWords;
    /**
     * @return the weigth
     */
    public Description(String word, Double Weight){
        this.weigth = Weight;
        this.word = word;
        this.numHits = new Double(1);
        similarWords= new LinkedList<String> ();
    }

    public void includeSimilarWords(LinkedList<String> lista){
        Iterator iteradorList = lista.iterator();
        while(iteradorList.hasNext()){
            String newword = (String) iteradorList.next();
            Iterator iteradorSim = this.similarWords.iterator();
            boolean flag = false;
            while(iteradorSim.hasNext()){
                String word = (String) iteradorSim.next();
                if(newword.equals(word)){
                    flag = true;
                }
            }
            if(flag==false){
                this.similarWords.add(newword);
            }
        }
    }

    public Double getWeigth() {
        return weigth;
    }

    /**
     * @param weigth the weigth to set
     */
    public void setWeigth(Double weigth) {
        this.weigth = weigth;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    public void centroidByIncrementHit(Double newWeight){
        Double weightBefore = this.getNumHits()*this.weigth;
        this.setNumHits((Double) (this.getNumHits() + 1));
        this.weigth = (weightBefore+newWeight)/this.getNumHits();
    }
    

    /**
     * @return the similarWords
     */
    public LinkedList<String> getSimilarWords() {
        return similarWords;
    }

    /**
     * @param similarWords the similarWords to set
     */
    public void setSimilarWords(LinkedList<String> similarWords) {
        this.similarWords = new LinkedList(similarWords);
    }

    /**
     * @return the numHits
     */
    public Double getNumHits() {
        return numHits;
    }

    /**
     * @param numHits the numHits to set
     */
    public void setNumHits(Double numHits) {
        this.numHits = numHits;
    }
}
