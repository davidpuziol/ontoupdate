/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package ontoupdate.databaseModule;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.sql.CallableStatement;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.sql.ResultSet;
//import java.sql.Timestamp;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Vector;
//import java.util.regex.Matcher;
//import javax.swing.JOptionPane;
//import ontoupdate.agentModule.graficInterface.DataBaseForm;
//import ontoupdate.databaseModule.EntityTransactionException;
//import ontoupdate.util.Crypto;
//import ontoupdate.util.Logger;
//import ontoupdate.util.PatternManager;
//
/**
 *
 * @author David Puziol Prata
 */
//@SuppressWarnings("unchecked")
//public class DataBaseConnection {
//
//    static public final int DB_ERROR_DUPLICATED_ROW = 1;
//    static public final int DB_ERROR_NO_DATA_FOUND = 1;
//    static public final int DB_ERROR_DELETE_FAILED = 1;
//    static public final int DB_ERROR_UNKNOWN_OPERATION = 2;
//    static public final int DB_ERROR_WRONG_PARAMETER = 2;
//    static public final int DB_ERROR_DEPENDENCE_NEEDED = 3;
//    static public final int DB_ERROR_SYSTEM_FAULT = 4;
//
//    private Connection connectionDB; //Conexão ao banco de dados
//    private Statement SQLstatement;
//
//    private static DataBaseConnection instance; // Instância Singleton da Classe
//    private HashMap<String, PreparedStatement> mapQueries; //HashMap que armazenará os pares <NomeQuery,QuerySQL>
//
//    Crypto crypto;
//
//    public static DataBaseConnection Instance()
//    {
//        if (instance == null)
//        {
//            instance = new DataBaseConnection();
//        }
//
//        return instance;
//    }
//
//    protected DataBaseConnection() {
//
//        crypto = new Crypto("OntoUpdate por David Prata");
//
//        //solicita novo JDBC para MySQL
//        try{
//        Class.forName("com.mysql.jdbc.Driver").newInstance();
//        }
//        catch(Exception ex)
//        {
//            Logger.Instance().UnexpectedException(this, ex);
//        }
//
//        //Instancia un Dialog de configuração de banco de dados
//        DataBaseForm form = new DataBaseForm(true);
//
//        boolean not_logged = true;
//        try{
//        do{
//            try {
//                //connectar no banco de dados
//                connectionDB = DriverManager.getConnection("jdbc:mysql://"+form.db_server+"/"+
//                                                                           form.db_database+
//                                                                           "?useUnicode=true&characterEncoding=UTF-8",
//                                                                           form.db_username, form.db_password);
//
//
//                //cria Statement SQL para ser usado nas futuras queries
//                SQLstatement = connectionDB.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
//                                 ResultSet.CONCUR_READ_ONLY);
//                LoadSqlQueries();
//
//                PreparedStatement ps = mapQueries.get("RECUPERA_PROFILES");
//                ps.executeQuery();
//
//                if(connectionDB.isClosed())
//                {
//                    not_logged = true;
//                }
//                else
//                {
//                    not_logged = false;
//                }
//
//
//               } catch (SQLException e) {
//
//                if (!form.ShowConfigDialog(e.getLocalizedMessage(),"Conectar"))
//                {
//                    JOptionPane.showMessageDialog(form, "O Sistema será fechado, pois não foi possível estabelecer conexão com o Banco de Dados.","Sistema Finalizado",JOptionPane.WARNING_MESSAGE);
//                    System.exit(0);
//                }
//            }
//        } while(not_logged);
//
//        if (!connectionDB.isClosed()) {
//            Logger.Instance().PrintLine(Logger.LEVEL_NORMAL, Logger.NOT_ERROR, this, "Conectado com sucesso ao Banco de Dados!");
//        }
//        } catch (SQLException ex) {
//            Logger.Instance().UnexpectedException(this, ex);
//        }
//
//    }
//
//    private boolean LoadSqlQueries() throws SQLException
//    {
//        String linha;
//        String ConteudoArquivo = "";
//        Matcher matcher;
//        mapQueries = new HashMap<String, PreparedStatement>();
//
//        // Obter o arquivo onde devem estar as Queries utilizadas pelo sistema
//        BufferedReader arquivo = ResourceManager.GetBufferedReader("SQL_QUERIES_FILE");
//        // Ler todo o arquivo
//        try {
//            while ((linha = arquivo.readLine()) != null) {
//                ConteudoArquivo = ConteudoArquivo + linha + ' ';
//            }
//            arquivo.close();
//        } catch (IOException ex) {
//           Logger.Instance().UnexpectedException(this, ex);
//           return false;
//        }
//
//        matcher = PatternManager.Instance().GetMatcher("\\s*(?>/\\*)\\s*([^\\s]+).*?(?>\\*/)(?:\\s*(?>/\\*).*?(?>\\*/))*\\s*(.*?)(?=(?>\\s*/\\*))", ConteudoArquivo);
//        while(matcher.find())
//        {
//            mapQueries.put(matcher.group(1), connectionDB.prepareStatement(matcher.group(2)));
//        }
//        return true;
//    }
//
//    public Vector Query_Profiles()
//    {
//        ResultSet result;
//        PreparedStatement ps = mapQueries.get("RECUPERA_PROFILES");
//
//        Vector retorno = new Vector();
//        try{
//            result = ps.executeQuery();
//            while(result.next())
//            {
//                retorno.add(result.getString("nome"));
//            }
//            return retorno;
//        } catch(SQLException e)
//        {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    private GregorianCalendar DateToGregorianCalendar(Timestamp timestamp)
//    {
//        if(timestamp == null) return null;
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        calendar.setTimeInMillis(timestamp.getTime());
//
//        return calendar;
//    }
//
//    public LinkedList<LinkedList<Object>> Query_RetrievalsFromProfile(int profileId)
//    {
//        ResultSet result;
//        PreparedStatement ps = mapQueries.get("LISTA_RETRIEVALS_PROFILE");
//        try{
//            ps.setInt(1, profileId);
//            result = ps.executeQuery();
//
//            LinkedList<LinkedList<Object>> resultList = new LinkedList<LinkedList<Object>>();
//            while(result.next())
//            {
//                resultList.add(new LinkedList<Object>());
//                resultList.getLast().add(result.getInt("id"));
//                resultList.getLast().add(DateToGregorianCalendar(result.getTimestamp("storage_date")));
//                resultList.getLast().add(result.getInt("mode"));
//                resultList.getLast().add(DateToGregorianCalendar(result.getTimestamp("from_date")));
//                resultList.getLast().add(DateToGregorianCalendar(result.getTimestamp("to_date")));
//            }
//            return resultList;
//        } catch(SQLException e)
//        {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public String Query_RetrievalResult(int retrievalId)
//    {
//        ResultSet result;
//        PreparedStatement ps = mapQueries.get("OBTEM_RESULTADO_RETRIEVAL");
//        try{
//            ps.setInt(1, retrievalId);
//            result = ps.executeQuery();
//
//            if(result.next())
//            {
//                return result.getString("result_report");
//            }
//            return "";
//        } catch(SQLException e)
//        {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public LinkedList<String> Query_OriginalLinksFromRetrieval(int retrievalId)
//    {
//        ResultSet result;
//        LinkedList resultList = new LinkedList<String>();
//        PreparedStatement ps = mapQueries.get("OBTER_LINKS_ANTERIORES");
//        try{
//            ps.setInt(1, retrievalId);
//            result = ps.executeQuery();
//
//            if(result.next())
//            {
//                resultList.add(result.getString("url_received"));
//            }
//            return resultList;
//        } catch(SQLException e)
//        {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public boolean fnc_removeRetrieval(int retrievalId)
//    {
//        ResultSet result;
//        PreparedStatement ps = mapQueries.get("REMOVER_RETRIEVAL");
//        try{
//            ps.setInt(1, retrievalId);
//            result = ps.executeQuery();
//
//            if(result.next())
//            {
//                return result.getBoolean("sucesso");
//            }
//            return false;
//        } catch(SQLException e)
//        {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    public synchronized Row_Profile prc_profile(String operation, Row_Profile row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_profile(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_name", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_description", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_last_use", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_proxy_use", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_proxy_address", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_proxy_port", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_proxy_username", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_proxy_password", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_formula_fij", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_formula_idfi", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_formula_wij", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_formula_wiq", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_formula_rank", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_num_max_threads", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_follow_links_on_hit", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_follow_links_max_deep", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_links_max_deep", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_obtain_verbs_radicals", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_only_exact_words", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_ignore_accentuation", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_regex_without_stop_words", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_stop_words", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_min_coeficient", java.sql.Types.DOUBLE);
//        cs.registerOutParameter("pio_has_results_limit", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_num_results_limit", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_hide_extra_text", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_ajust_hour", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_ajust_minute", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_ontology_path", java.sql.Types.VARCHAR);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setInt("pio_id", row.id);
//        SetNullableString(cs,"pio_name", row.name );
//        SetNullableString(cs,"pio_description", row.description );
//        SetCalendarTimestamp(cs,"pio_last_use",row.last_use);
//        cs.setBoolean("pio_proxy_use", row.proxy_use);
//        SetNullableString(cs,"pio_proxy_address", row.proxy_address );
//        SetNullableString(cs,"pio_proxy_port", row.proxy_port );
//        SetNullableString(cs,"pio_proxy_username", row.proxy_username );
//        SetNullableString(cs,"pio_proxy_password", crypto.encrypt(row.proxy_password) );
//        SetNullableString(cs,"pio_formula_fij", row.formula_fij);
//        SetNullableString(cs,"pio_formula_idfi", row.formula_idfi);
//        SetNullableString(cs,"pio_formula_wij", row.formula_wij);
//        SetNullableString(cs,"pio_formula_wiq", row.formula_wiq);
//        SetNullableString(cs,"pio_formula_rank", row.formula_rank);
//        cs.setInt("pio_num_max_threads", row.num_max_threads);
//        cs.setBoolean("pio_follow_links_on_hit", row.follow_links_on_hit);
//        cs.setBoolean("pio_follow_links_max_deep", row.follow_links_max_deep);
//        cs.setInt("pio_links_max_deep", row.links_max_deep);
//        cs.setBoolean("pio_obtain_verbs_radicals", row.obtain_verbs_radicals);
//        cs.setBoolean("pio_only_exact_words", row.only_exact_words);
//        cs.setBoolean("pio_ignore_accentuation", row.ignore_accentuation);
//        cs.setBoolean("pio_regex_without_stop_words", row.regex_without_stop_words);
//        SetNullableString(cs,"pio_stop_words", row.stop_words);
//        cs.setDouble("pio_min_coeficient", row.min_coeficient);
//        cs.setBoolean("pio_has_results_limit", row.has_results_limit);
//        cs.setInt("pio_num_results_limit", row.num_results_limit);
//        cs.setBoolean("pio_hide_extra_text", row.hide_extra_text);
//        cs.setInt("pio_ajust_hour", row.ajust_hour);
//        cs.setInt("pio_ajust_minute", row.ajust_minute);
//        SetNullableString(cs,"pio_ontology_path", row.ontology_path);
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.id =  cs.getInt("pio_id");
//        row.name = cs.getString("pio_name");
//        row.description = cs.getString("pio_description");
//        row.last_use = GetCalendarTimestamp(cs,"pio_last_use");
//        row.proxy_use = cs.getBoolean("pio_proxy_use");
//        row.proxy_address = cs.getString("pio_proxy_address");
//        row.proxy_port = cs.getString("pio_proxy_port");
//        row.proxy_username = cs.getString("pio_proxy_username");
//        row.proxy_password = crypto.decrypt(cs.getString("pio_proxy_password"));
//        row.formula_fij = cs.getString("pio_formula_fij");
//        row.formula_idfi = cs.getString("pio_formula_idfi");
//        row.formula_wij = cs.getString("pio_formula_wij");
//        row.formula_wiq = cs.getString("pio_formula_wiq");
//        row.formula_rank = cs.getString("pio_formula_rank");
//        row.num_max_threads = cs.getInt("pio_num_max_threads");
//        row.follow_links_on_hit = cs.getBoolean("pio_follow_links_on_hit");
//        row.follow_links_max_deep = cs.getBoolean("pio_follow_links_max_deep");
//        row.links_max_deep = cs.getInt("pio_links_max_deep");
//        row.obtain_verbs_radicals = cs.getBoolean("pio_obtain_verbs_radicals");
//        row.only_exact_words = cs.getBoolean("pio_only_exact_words");
//        row.ignore_accentuation = cs.getBoolean("pio_ignore_accentuation");
//        row.regex_without_stop_words = cs.getBoolean("pio_regex_without_stop_words");
//        row.stop_words = cs.getString("pio_stop_words");
//        row.min_coeficient = cs.getDouble("pio_min_coeficient");
//        row.has_results_limit = cs.getBoolean("pio_has_results_limit");
//        row.num_results_limit = cs.getInt("pio_num_results_limit");
//        row.hide_extra_text = cs.getBoolean("pio_hide_extra_text");
//        row.ajust_hour = cs.getInt("pio_ajust_hour");
//        row.ajust_minute =  cs.getInt("pio_ajust_minute");
//        row.ontology_path = cs.getString("pio_ontology_path");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(DB_ERROR_SYSTEM_FAULT,ex.getMessage());
//        }
//
//        return row;
//    }
//
//    public synchronized Row_Retrieval prc_retrieval(String operation, Row_Retrieval row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_retrieval(?,?,?,?,?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_profile_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_mode", java.sql.Types.TINYINT);
//        cs.registerOutParameter("pio_from_date", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_to_date", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_storage_date", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_result_report", java.sql.Types.VARCHAR);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setInt("pio_id", row.id);
//        cs.setInt("pio_profile_id", row.profile_id);
//        cs.setInt("pio_mode", row.mode);
//        SetCalendarTimestamp(cs,"pio_from_date",row.from_date);
//        SetCalendarTimestamp(cs,"pio_to_date",row.to_date);
//        SetCalendarTimestamp(cs,"pio_storage_date",row.storage_date);
//        SetNullableString(cs,"pio_result_report", row.result_report );
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.id =  cs.getInt("pio_id");
//        row.profile_id = cs.getInt("pio_profile_id");
//        row.mode = cs.getInt("pio_mode");
//        row.from_date = GetCalendarTimestamp(cs,"pio_from_date");
//        row.to_date = GetCalendarTimestamp(cs,"pio_to_date");
//        row.storage_date = GetCalendarTimestamp(cs,"pio_storage_date");
//        row.result_report = cs.getString("pio_result_report");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(DB_ERROR_SYSTEM_FAULT,ex.getMessage());
//        }
//
//        return row;
//    }
//
//    public synchronized Row_Wiki prc_wiki(String operation, Row_Wiki row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_wiki(?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_root_url", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_storage_date", java.sql.Types.TIMESTAMP);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setInt("pio_id", row.id);
//        SetNullableString(cs,"pio_root_url", row.root_url );
//        SetCalendarTimestamp(cs,"pio_storage_date",row.storage_date);
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.id =  cs.getInt("pio_id");
//        row.root_url = cs.getString("pio_root_url");
//        row.storage_date = GetCalendarTimestamp(cs,"pio_storage_date");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(ex.getErrorCode(),ex.getMessage());
//        }
//
//        return row;
//    }
//
//    public synchronized Row_Folksonomy prc_folksonomy(String operation, Row_Folksonomy row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_folksonomy(?,?,?,?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_wiki_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_name", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_article_url", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_discussion_url", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_is_article", java.sql.Types.BOOLEAN);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setInt("pio_id", row.id);
//        cs.setInt("pio_wiki_id", row.wiki_id);
//        SetNullableString(cs,"pio_name", row.name );
//        SetNullableString(cs,"pio_article_url", row.article_url );
//        SetNullableString(cs,"pio_discussion_url", row.discussion_url );
//        cs.setBoolean("pio_is_article", row.is_article);
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.id =  cs.getInt("pio_id");
//        row.wiki_id = cs.getInt("pio_wiki_id");
//        row.name = cs.getString("pio_name");
//        row.article_url = cs.getString("pio_article_url");
//        row.discussion_url = cs.getString("pio_discussion_url");
//        row.is_article = cs.getBoolean("pio_is_article");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(DB_ERROR_SYSTEM_FAULT,ex.getMessage());
//        }
//
//        return row;
//    }
//
//    public synchronized Row_RetrievalComponent prc_retrieval_component(String operation, Row_RetrievalComponent row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_retrieval_component(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_retrieval_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_folksonomy_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_actually_exists", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_has_discussion", java.sql.Types.BOOLEAN);
//        cs.registerOutParameter("pio_article_revision_from", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_article_revision_to", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_article_text_considered", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_discussion_revision_from", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_discussion_revision_to", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_discussion_text_considered", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_origin_id", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_followed_link_deep", java.sql.Types.INTEGER);
//        cs.registerOutParameter("pio_url_received", java.sql.Types.VARCHAR);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setInt("pio_id", row.id);
//        cs.setInt("pio_retrieval_id", row.retrieval_id);
//        cs.setInt("pio_folksonomy_id", row.folksonomy_id);
//        cs.setBoolean("pio_actually_exists", row.actually_exists);
//        cs.setBoolean("pio_has_discussion", row.has_discussion);
//        SetCalendarTimestamp(cs,"pio_article_revision_from",row.article_revision_from);
//        SetCalendarTimestamp(cs,"pio_article_revision_to",row.article_revision_to);
//        SetNullableString(cs,"pio_article_text_considered", row.article_text_considered );
//        SetCalendarTimestamp(cs,"pio_discussion_revision_from",row.discussion_revision_from);
//        SetCalendarTimestamp(cs,"pio_discussion_revision_to",row.discussion_revision_to);
//        SetNullableString(cs,"pio_discussion_text_considered", row.discussion_text_considered );
//        cs.setInt("pio_origin_id", row.origin_id);
//        cs.setInt("pio_followed_link_deep", row.followed_link_deep);
//        SetNullableString(cs,"pio_url_received", row.url_received );
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.id =  cs.getInt("pio_id");
//        row.retrieval_id = cs.getInt("pio_retrieval_id");
//        row.folksonomy_id = cs.getInt("pio_folksonomy_id");
//        row.actually_exists = cs.getBoolean("pio_actually_exists");
//        row.has_discussion = cs.getBoolean("pio_has_discussion");
//        row.article_revision_from = GetCalendarTimestamp(cs,"pio_article_revision_from");
//        row.article_revision_to = GetCalendarTimestamp(cs,"pio_article_revision_to");
//        row.article_text_considered = cs.getString("pio_article_text_considered");
//        row.discussion_revision_from = GetCalendarTimestamp(cs,"pio_discussion_revision_from");
//        row.discussion_revision_to = GetCalendarTimestamp(cs,"pio_discussion_revision_to");
//        row.discussion_text_considered = cs.getString("pio_discussion_text_considered");
//        row.origin_id = cs.getInt("pio_origin_id");
//        row.followed_link_deep = cs.getInt("pio_followed_link_deep");
//        row.url_received = cs.getString("pio_url_received");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(DB_ERROR_SYSTEM_FAULT,ex.getMessage());
//        }
//
//        return row;
//    }
//
//    public synchronized PseudoRow_RevisionHistory prc_revision_history(String operation, String text_kind, PseudoRow_RevisionHistory row) throws EntityTransactionException
//    {
//        int po_n_errcode = 0;
//        String po_s_errmsg = null;
//
//        try{
//        // Criar um Statement para Chamada de Procedimento
//        CallableStatement cs = connectionDB.prepareCall("CALL prc_revision_history(?,?,?,?,?,?,?,?,?,?);");
//
//        // Registrar Parâmetros de saída
//        cs.registerOutParameter("po_n_errcode", java.sql.Types.INTEGER);
//        cs.registerOutParameter("po_s_errmsg", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_date", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_date_still_valid", java.sql.Types.TIMESTAMP);
//        cs.registerOutParameter("pio_text", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_inner_link_list", java.sql.Types.VARCHAR);
//        cs.registerOutParameter("pio_origin_url", java.sql.Types.VARCHAR);
//
//        // Atribuir valores aos parâmetros de entrada
//        cs.setString("pi_ch_operation", operation);
//        cs.setString("pi_ch_text_kind", text_kind);
//        cs.setInt("pi_folksonomy_id", row.folksonomy_id);
//        SetCalendarTimestamp(cs,"pio_date",row.date);
//        SetCalendarTimestamp(cs,"pio_date_still_valid",row.date_still_valid);
//        SetNullableString(cs,"pio_text", row.text );
//        SetNullableString(cs,"pio_inner_link_list", row.inner_link_list);
//        SetNullableString(cs,"pio_origin_url", row.originUrl);
//
//        // Execute the CALL statement and ignore result sets
//        cs.executeUpdate();
//
//        // Retrieve values from INOUT parameters
//        po_n_errcode = cs.getInt("po_n_errcode");
//        po_s_errmsg = cs.getString("po_s_errmsg");
//
//        if(po_n_errcode != 0)
//        {
//            throw new EntityTransactionException(po_n_errcode,po_s_errmsg);
//        }
//        row.date = GetCalendarTimestamp(cs,"pio_date");
//        row.date_still_valid = GetCalendarTimestamp(cs,"pio_date_still_valid");
//        row.text = cs.getString("pio_text");
//        row.inner_link_list = cs.getString("pio_inner_link_list");
//        row.originUrl = cs.getString("pio_origin_url");
//
//        // Fechar o Statement
//        cs.close();
//        }
//        catch(SQLException ex)
//        {
//            throw new EntityTransactionException(DB_ERROR_SYSTEM_FAULT,ex.getMessage());
//        }
//
//        return row;
//    }
//
//    /**
//     * Atribui um valor a um parâmetro de entrada SQL DATETIME/TIMESTAMP
//     * a partir do instante presente em um objeto GregorianCalendar
//     * @param callableStatement Um objeto CallableStatement que está sendo
//     * preparado para execução
//     * @param parameter_name O Nome do parâmetro de entrada a sofrer atribuição de valor
//     * @param date O objeto GregorianCalendar com o instante de tempo a
//     * definir para o parâmetro de entrada SQL
//     * @throws java.sql.SQLException
//     */
//    private void SetCalendarTimestamp(Object callableStatement, String parameter_name, GregorianCalendar date) throws SQLException
//    {
//            if(date != null)
//            {
//                ((CallableStatement)callableStatement).setTimestamp(parameter_name, new Timestamp(date.getTimeInMillis()));
//            }
//    }
//
//    /**
//     * Recupera o valor de um parâmetro de retorno SQL DATETIME/TIMESTAMP e o
//     * transforma automaticamente para um GregorianCalendar
//     * @param callableStatement Um objeto CallableStatement que acabou de ser
//     * executado, a partir do qual será obtido o resultado de um parâmetro de
//     * saída do tipo DATETIME/TIMESTAMP
//     * @param parameter_name O Nome do Parâmetro a ser recuperado
//     * @return Um objeto GregorianCalendar com o instante de tempo definido
//     * pelo parâmetro de saída SQL
//     * @throws java.sql.SQLException
//     */
//    private GregorianCalendar GetCalendarTimestamp(Object callableStatement, String parameter_name) throws SQLException
//    {
//        Timestamp time;
//
//        time = ((CallableStatement)callableStatement).getTimestamp(parameter_name);
//        if( time == null)  {  return null; }
//        GregorianCalendar retorno = new GregorianCalendar();
//        retorno.setTimeInMillis(time.getTime());
//        return retorno;
//    }
//
//    /**
//     * Atribui uma String a um parâmetro de um CallableStatement, considerando
//     * que esta String pode ser Nula.
//     * @param callableStatement Um objeto CallableStatement que está sendo
//     * preparado para execução
//     * @param parameter_name O Nome do parâmetro de entrada a sofrer atribuição de valor
//     * @param text String a ser atribuida ao parâmetro de entrada do CallableStatement
//     * @throws java.sql.SQLException
//     */
//    private void SetNullableString(Object callableStatement, String parameter_name, String text) throws SQLException
//    {
//        if(text != null)
//        {
//            ((CallableStatement)callableStatement).setString(parameter_name, text);
//        }
//        else
//        {
//            ((CallableStatement)callableStatement).setString(parameter_name, "");
//        }
//    }
//
//}
