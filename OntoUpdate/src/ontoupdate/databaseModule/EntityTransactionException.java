/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.databaseModule;

/**
 *
 * @author David Puziol Prata
 */
public class EntityTransactionException extends Exception{
    public String errmsg;
    public int errcode;
    
    public EntityTransactionException(int errcode, String errmsg)
    {
        super(errmsg);
        this.errcode = errcode;
        this.errmsg = errmsg;
    }
}
