/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.util;

import java.util.regex.*;
import java.util.HashMap;

/**
 *
 * @David Puziol Prata
 */
public class PatternManager {
    private HashMap<String,Pattern> patternMap;
    private static PatternManager instance;
    
    public static final String REGEX_URL_NO_WHITESPACES = "http://[^\\s]*";
    
    /**
    * Obtém a Instância única do PatternManager segundo Padrão Singleton
    * @return instância única do PatternManager
    */
    public static PatternManager Instance()
    {
        if (instance == null)
        {
            instance = new PatternManager();
        }
        
        return instance;
    }
    
    /**
     * Construtor Básico PatternManager
     */
    protected PatternManager()
    {
        patternMap = new HashMap<String, Pattern>();
    }
    
    /**
     * Obtém o Pattern para a respectiva expressão regular
     * @param regex Expressão Regular para o Pattern
     * @param flags para match
     * @return Pattern compilado com a expressão regular passada
     */
    public Pattern GetPattern(String regex,int flags)
    {
        Pattern patternCompilado = patternMap.get(regex);
        if( patternCompilado == null)
        {
            patternCompilado = Pattern.compile(regex,flags);
        }
        return patternCompilado;
    }
    
    public Pattern GetPattern(String regex)
    {
        
        return GetPattern(regex,Pattern.CASE_INSENSITIVE);
    }
    
    public Matcher GetMatcher(String regex,String lookInHere)
    {
        return GetPattern(regex).matcher(lookInHere);
    }
}
