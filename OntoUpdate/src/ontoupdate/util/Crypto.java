/*
 * Foi feito o download do Jakarta Codec no dia 06/08/2008
 * Link: http://commons.apache.org/codec/
 * Versão do Codec baixado: 1.3
 * Nome da Biblioteca adicionada no projeto: commons-codec-1.3.jar
 * 
 */

package ontoupdate.util;

import java.io.UnsupportedEncodingException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 * Classe para criptografia de Strings segundo algorítmo DES e Charset UTF8
 * @David Puziol Prata
 */
public class Crypto {
        private Cipher ecipher; //criptografa
        private Cipher dcipher; //decriptografa

        // 8-byte Salt
        byte[] salt = {
            (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32,
            (byte)0x56, (byte)0x35, (byte)0xE3, (byte)0x03
        };
    
        // Contagem de Iterações
        int iterationCount = 19;
    
        /**
         * Construtor do objeto de criptografia a partir de uma SecretKey
         * @param key SecretKey a ser utilizada para criptografia
         */
        public Crypto(SecretKey key) {
            try {
                ecipher = Cipher.getInstance("DES");
                dcipher = Cipher.getInstance("DES");
                ecipher.init(Cipher.ENCRYPT_MODE, key);
                dcipher.init(Cipher.DECRYPT_MODE, key);
            } catch (javax.crypto.NoSuchPaddingException e) {
            } catch (java.security.NoSuchAlgorithmException e) {
            } catch (java.security.InvalidKeyException e) {
            }
        }
        
        /**
         * Construtor do objeto de criptografia a partir de uma String que servirá como PassPhrase
         * @param passPhrase String contendo a PassPhrase para criptografia
         */
        public Crypto(String passPhrase) {
            try {
                // Criar a chave de criptografia a partir da String passada
                KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
                SecretKey key = SecretKeyFactory.getInstance(
                    "PBEWithMD5AndDES").generateSecret(keySpec);
                ecipher = Cipher.getInstance(key.getAlgorithm());
                dcipher = Cipher.getInstance(key.getAlgorithm());
    
                // Preparar os parâmetros para os cifradores
                AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
    
                // Criar ambos os cifradores (cripto & decripto)
                ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
                dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            } catch (java.security.InvalidAlgorithmParameterException e) {
            } catch (java.security.spec.InvalidKeySpecException e) {
            } catch (javax.crypto.NoSuchPaddingException e) {
            } catch (java.security.NoSuchAlgorithmException e) {
            } catch (java.security.InvalidKeyException e) {
            }
        }
    
        /**
         * Criptografa a String passada por parâmetro
         * @param str String a criptografar
         * @return String criptografada
         */
        public String encrypt(String str) {
            if(str == null)
            {
                return null;
            }
            
            try {
                // Codificar a string segundo Charset UTF8
                byte[] utf8 = str.getBytes("UTF8");
    
                // Criptografar
                byte[] enc = ecipher.doFinal(utf8);
    
                // Codificar os bytes para base64 e obter a String em UTF8
                return new String(org.apache.commons.codec.binary.Base64.encodeBase64(enc),"UTF8");
                //return new sun.misc.BASE64Encoder().encode(enc);
                
            } catch (javax.crypto.BadPaddingException e) {
            } catch (IllegalBlockSizeException e) {
            } catch (UnsupportedEncodingException e) {
            } catch (java.io.IOException e) {
            }
            return null;
        }
    
        /**
         * Decriptografa a String passada por parâmetro
         * @param str String criptografada
         * @return String decriptografada
         */
        public String decrypt(String str) {
            if (str == null)
            {
                return null;
            }
            
            try {
                // Decodificar a partir de base64 para obter seqüência de Bytes
                byte[] dec = org.apache.commons.codec.binary.Base64.decodeBase64(str.getBytes());
                // byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
                
                // Decriptografar bytes
                byte[] utf8 = dcipher.doFinal(dec);
    
                // Decodificar sequência de bytes em string utilizando Charset utf-8
                return new String(utf8, "UTF8");
            } catch (javax.crypto.BadPaddingException e) {
            } catch (IllegalBlockSizeException e) {
            } catch (UnsupportedEncodingException e) {
            } catch (java.io.IOException e) {
            }
            return null;
        }
}
