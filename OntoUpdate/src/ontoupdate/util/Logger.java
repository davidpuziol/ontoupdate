package ontoupdate.util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @David Puziol Prata
 */
public class Logger {

    public static final int LEVEL_DETAILED = 1;
    public static final int LEVEL_NORMAL = 2;
    public static final int LEVEL_IMPORTANT = 3;
    
    public static final boolean ERROR = true;
    public static final boolean NOT_ERROR = false;
    
    private static Logger instance;

    /**
     * Obtém a Instância única do Logger segundo Padrão Singleton
     * @return instância única do Logger
     */
    public static Logger Instance() {
        if (instance == null) {
            instance = new Logger();
        }

        return instance;
    }

    /**
     * Construtor Básico Logger
     */
    protected Logger() {

    }
    
    public void PrintLine(int infoLevel,boolean error,Class<?> classe, String line)
    {
        if(infoLevel > 0)
        {
            println("["+ classe.getSimpleName() + " , " + Thread.currentThread().getId() + "] - " + line.replace("\n", "\\n"),error);
        }
    }
    public void PrintLine(int infoLevel,boolean error,Object objeto, String line)
    {
        if(infoLevel > 0)
        {
                println("[" + objeto.getClass().getSimpleName() + " , " + Thread.currentThread().getId() + "] - " + line.replace("\n", "\\n"), error);
        }
    }
    
    public void PrintBlock(int infoLevel,boolean error,Object objeto,String line)
    {
        String identificador;
        
        if(infoLevel > 0)
        {
           /* if (objeto.toString().startsWith("Thread[")) {
                identificador = "[" + objeto.getClass().getSimpleName() + " , " + ((Thread) objeto).getId() + "] - ";
            } else {*/
                identificador = "[" + objeto.getClass().getSimpleName() + " , " + Thread.currentThread().getId() + "] - ";
           // }
            PrintIdentyfiedBlock(identificador,error,line);
        }
    }
    
    public void PrintBlock(int infoLevel,boolean error,Class<?> classe, String line)
    {
        String identificador;
        if(infoLevel > 0)
        {
            identificador = "["+ classe.getSimpleName() + " , " + Thread.currentThread().getId() + "] - ";
            PrintIdentyfiedBlock(identificador,error,line);
        }
    }
    
    private void PrintIdentyfiedBlock(String identifyer, boolean error, String line)
    {
            String espacamento;
            /*
             * As demais linhas depois da primeira deverão ter a mesma tabulação relativa ao
             * tamanho da String de identificação
             * Ex: [Objeto] - linha 1
             *                linha 2
             *                linha 3
             */
            espacamento = new String();
            for(int i=0;i<identifyer.length()+2;i++)
            {
                espacamento+=" ";
            }
            
            line = line.replace("\n", "\n"+espacamento).trim();
            
            println(identifyer + line, error);
    }
    
    public void UnexpectedException(Object objeto, Exception ex){
        println("[" + objeto.getClass().getSimpleName() + " , " + Thread.currentThread().getId() + "] - Exception! --> " + ex.getMessage() , true);
        ex.printStackTrace();
    }
    
    public void UnexpectedException(Class<?> classe, Exception ex){
        println("["+ classe.getSimpleName() + " , " + Thread.currentThread().getId() + "] -  Exception! --> " + ex.getMessage() , true);
        ex.printStackTrace();
    }
    
    /**
     * Adiciona uma nova linha no log instanciado
     * @param line texto a ser adicionado
     */
    private synchronized void println(String line, boolean error) {
        if (error) {
            System.out.println("# "+line);
        } else {
            System.out.println("  "+line);
        }
    }   
}
