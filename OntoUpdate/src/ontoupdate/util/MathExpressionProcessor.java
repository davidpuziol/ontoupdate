/*
 * MathExpressionProcessor.java
 * 
 * Classe para criação dinâmica de fórmulas e cálculo de resultados a partir
 * de destas fórmulas configuradas em tempo de execução.
 * 
 */

package ontoupdate.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.Stack;

/**
 * 
 * @author David Puziol Prata
 */
public class MathExpressionProcessor {

    HashMap<String,Double> variablesMap; //Map de variáveis permitidas para a fórmula
    LinkedList<String> variablesNeeded; // Se a variável for obrigatória, deverá constar aqui
    
    Stack<String> operatorStack; 
    Stack<String> operandStack;
    LinkedList<Object> postFixedExpression;
    
    String originalInFixExpression;
    String consideredInFixExpression;
    
    public MathExpressionProcessor() {
        variablesMap = new HashMap<String,Double>();
        variablesNeeded = new LinkedList<String>();
        SetStandardConstants();
    }
    
    public void AddPossibleVariables(String... variables)
    {
        AddVariables(false,variables);
    }
    
    public void AddNecessaryVariables(String... variables)
    {
        AddVariables(true,variables);
    }
    
    private void AddVariables(boolean necessary,String... variables)
    {
        if(variables != null)
        {
            for(int i=0; i< variables.length; i++)
            {
                //remover todos os espaços e colocar todos os caracteres em minúsculo
                String var = variables[i].replaceAll(" ","").toLowerCase();
                if(! var.isEmpty())
                {
                    if(!variablesMap.containsKey(var))
                    {
                        variablesMap.put(var, (double)0);
                    }
                    
                    if(necessary && !variablesNeeded.contains(var))
                    {
                        variablesNeeded.add(var);
                    }
                    else if(!necessary && variablesNeeded.contains(var))
                    {
                        variablesNeeded.remove(var);
                    }
                }
            }
        }
    }
    
    public boolean SetVariable(String varName,double varValue)
    {
        varName = varName.replaceAll(" ","").toLowerCase();
        if(variablesMap.containsKey(varName))
        {
            variablesMap.remove(varName);
            variablesMap.put(varName, varValue);
            return true;
        }
        return false;
    }
    public boolean SetVariable(String varName, int varValue)
    {
        return this.SetVariable(varName, (double)varValue);
    }
    public boolean SetVariable(String varName, long varValue)
    {
        return this.SetVariable(varName, (double)varValue);
    }
    public boolean SetVariable(String varName, float varValue)
    {
        return this.SetVariable(varName, (double) varValue);
    }
    private void SetStandardConstants()
    {
        AddVariables(false,"pi","e");
        SetVariable("pi",Math.PI);
        SetVariable("e",Math.E);
    }
    
    final String NUMBER_TOKENS = "[\\d]+(?:\\.\\d)[\\d]*|[\\d]+";
    final String VARIABLE_TOKENS = "[a-zA-Z]+[\\d]*";
    final String OPERATOR_TOKENS = "[\\+\\(\\-\\*\\)\\/\\^]";
    final String UNARY_OPERATOR_TOKENS = "abs|log|ln|r3|r2|sin|cos|tan|asin|acos|atan|rad|deg|round|ceil|floor";
    
    public String Prepare(String expression) throws MathExpressionException
    {
        originalInFixExpression = expression;
        
        // Checar se a expressão não é nula ou vazia
        CheckEmptyExpression(expression);
        //colocar toda a expressão em letras minúsculas
        expression = expression.toLowerCase();
        //Verifica e existência de caracteres inválidos na expressão
        CheckInvalidCharacter(expression);
        //Checar se a abertura e fechamento de parênteses estão corretos (Pode lançar exception)
        CheckParentesis(expression);
        //Checar variáveis presentes na expressão
        CheckVariables(expression);
        //Normalizar a expressão in-fixa para preparar para transformação para pós fixa (Pode lançar exception)
        consideredInFixExpression = ValidateInfixExpression(expression);
        //Transforma a expressão para pós-fixa (aramazenará na estrutura postFixedExpression do presente objeto)
        InfixToPostfix(consideredInFixExpression);
        
        //Retorna a forma pós-fixa em forma de string
        return GetPostFixExpression();
    }
    
/* ======================================================================
 * Avaliação de Expressão In-Fixa
 * ====================================================================== */
    private String ValidateInfixExpression(String infixExpression) throws MathExpressionException
    {
        Matcher inFixMatcher;
        String token;
        String previousToken = "";
        String newInfixExpression = "";
        String InfixExpressionAnalyzed = "";
        
        inFixMatcher = PatternManager.Instance().GetMatcher(VARIABLE_TOKENS+"|"+NUMBER_TOKENS+"|"+OPERATOR_TOKENS, infixExpression);
        
        while(inFixMatcher.find())
        {
            token = inFixMatcher.group();
            
            if((previousToken.isEmpty()||previousToken.equals("(")) && token.equals("-"))
            {   
                newInfixExpression = newInfixExpression + "(0-1)* ";
            }
            else if((previousToken.isEmpty()||previousToken.equals("(")) && token.equals("+"))
            {
                newInfixExpression = newInfixExpression + "";
            }
            else if((previousToken.isEmpty()||previousToken.equals("(")) && IsBinaryOperator(token))
            {
                throw new MathExpressionException("Erro de Sintaxe","Operador em posição imprópria",InfixExpressionAnalyzed+"{"+token+"}");
            }
            else if(IsOperator(previousToken)&&token.equals(")"))
            {
                throw new MathExpressionException("Erro de Sintaxe","Operando esperado mas encontrado fechamento de parênteses",InfixExpressionAnalyzed+"{"+token+"}");
            }
            else if(previousToken.isEmpty())
            {
                newInfixExpression = newInfixExpression + token + " ";
            }
            else if( IsOperand(previousToken) && IsOperand(token) ||
                     IsOperand(previousToken) && token.equals("(") ||
                     previousToken.equals(")") && token.equals("(") ||
                     IsOperand(previousToken) && IsUnaryOperator(token) ||
                     previousToken.equals(")") && IsUnaryOperator(token))
            {
                newInfixExpression = newInfixExpression + "* " + token + " ";
            }
            else if(IsOperatorNotParentesis(previousToken)&&IsBinaryOperator(token))
            {
                throw new MathExpressionException("Erro de Sintaxe","Duplicidade de operadores",InfixExpressionAnalyzed+"{"+token+"}");
            }
            else if(previousToken.equals("(")&& token.equals(")"))
            {
                throw new MathExpressionException("Erro de Sintaxe", "Sub-expressão vazia",InfixExpressionAnalyzed+"{"+token+"}");
            }
            else
            {
                newInfixExpression = newInfixExpression + token + " ";
            }
            
            InfixExpressionAnalyzed = InfixExpressionAnalyzed + token + " ";
            previousToken = token;
        }
        
        if(IsOperator(previousToken) && !previousToken.equals(")"))
        {
            InfixExpressionAnalyzed = InfixExpressionAnalyzed.substring(0,InfixExpressionAnalyzed.lastIndexOf(previousToken));
            throw new MathExpressionException("Erro de Sintaxe", "Expressão finalizada incorretamente",InfixExpressionAnalyzed+"{"+previousToken+"}");
        }
        
        return newInfixExpression;
    }
    
    private void CheckEmptyExpression(String expression) throws MathExpressionException
    {
        if(expression == null||expression.trim().isEmpty())
        {
            throw new MathExpressionException("Erro de Semântica", "A expressão não pode ser nula","");
        }
    }
    
    private void CheckInvalidCharacter(String expression) throws MathExpressionException
    {
        Matcher inFixMatcher;
        String token;
        String expressionChecked="";
        
        inFixMatcher = PatternManager.Instance().GetMatcher(VARIABLE_TOKENS+"|"+NUMBER_TOKENS+"|"+OPERATOR_TOKENS+"|[\\s]+|(.)", expression);
        
        while(inFixMatcher.find())
        {
            token = inFixMatcher.group(1);
            if(token != null && !token.isEmpty())
            {
                throw new MathExpressionException("Erro de Sintaxe", "Caracter inválido",expressionChecked+"{"+token+"}");
            }
            expressionChecked = expressionChecked + inFixMatcher.group();
        }
    }
    
    private void CheckParentesis(String expression) throws MathExpressionException
    {
        char token;
        int parentesisCount = 0;
        String expressionChecked= "";
        
        for(int i=0;i<expression.length();i++)
        {
            token = expression.charAt(i);
            if(token == '(')
            {
                parentesisCount++;
            }
            else if (token == ')')
            {
                parentesisCount--;
            }
            
            if(parentesisCount < 0)
            {
                throw new MathExpressionException("Erro de Sintaxe", "Fechamento de parênteses inválido",expressionChecked+"{"+token+"}");
            }
            expressionChecked = expressionChecked + token;
        }
        
        if(parentesisCount > 0)
        {
            throw new MathExpressionException("Erro de Sintaxe", "Expressão não fecha todos os parênteses abertos",expressionChecked);
        }
    }
    
    private void CheckVariables(String expression) throws MathExpressionException
    {
        Matcher inFixMatcher;
        String token;
        LinkedList<String> existentVars = new LinkedList<String>();
        
        inFixMatcher = PatternManager.Instance().GetMatcher(VARIABLE_TOKENS, expression);
        
        // Conferir se todos os indicadores digitados foram declarados
        while(inFixMatcher.find())
        {
            token = inFixMatcher.group();
            if(!IsOperator(token))
            {
                if(variablesMap.containsKey(token) && !existentVars.contains(token))
                {
                    existentVars.add(token);
                }
                else if (!variablesMap.containsKey(token))
                {
                    throw new MathExpressionException("Erro de Semântica", "Identificador não conhecido",expression.substring(0,inFixMatcher.start())+"{"+token+"}");
                }
            }
        }
        // Conferir se todos os indicadores obrigatórios foram digitados
        Iterator<String> it = variablesNeeded.iterator();
        while(it.hasNext())
        {
            String currentVar = it.next();
            if(!existentVars.contains(currentVar))
            {
                throw new MathExpressionException("Erro de Semântica", "Identificador obrigatório ausente", currentVar);
            }
        }
    }
/* ======================================================================
 * Transformação de forma In-Fixa para Pós-Fixa
 * ====================================================================== */
    
    private void InfixToPostfix(String infixExpression)
    {
        Matcher inFixMatcher;
        String token;
        
        inFixMatcher = PatternManager.Instance().GetMatcher(VARIABLE_TOKENS+"|"+NUMBER_TOKENS+"|"+OPERATOR_TOKENS, infixExpression);
        
        //Inicializar estruturas auxiliares
        operatorStack = new Stack<String>();
        postFixedExpression = new LinkedList<Object>();
                
        while(inFixMatcher.find())
        {
            token = inFixMatcher.group();
            if(IsOperand(token))
            {
                postFixedExpression.add((Object)ConvertOperand(token));
            }
            else
            {
                ProcessOperator(token);
            }
        }
        
        while(!operatorStack.isEmpty())
        {
            postFixedExpression.add(operatorStack.pop());
        }
    }
    
    private void ProcessOperator(String value)
    {
        boolean done = false;
        String poppedValue;
        
        do{
            if(operatorStack.isEmpty() || value.equals("("))
            {
                operatorStack.push(value);
                done = true;
            }
            else if( OperatorPrecedence(value) > OperatorPrecedence(operatorStack.peek()) ||
                     IsUnaryOperator(value) && IsUnaryOperator(operatorStack.peek()))
            {
                operatorStack.push(value);
                done = true;
            }
            else
            {
                poppedValue = operatorStack.pop();
                if(poppedValue.equals("("))
                {
                    done = true;
                }
                else
                {
                    postFixedExpression.add(poppedValue);
                }
            }
        }while(!done);
    }
    
    private int OperatorPrecedence(String operator)
    {
        if(IsUnaryOperator(operator))
        {
            return 4;
        }
        if(operator.equals("^"))
        {
            return 3;
        }
        if(operator.equals("*")||
           operator.equals("/"))
        {
            return 2;
        }
        if(operator.equals("+")||
           operator.equals("-"))
        {
            return 1;
        }
        if(operator.equals("(")||
           operator.equals(")"))
        {
            return 0;
        }
        
        return -1;
    }
    
    private boolean IsOperand(String value)
    {
        return (!IsOperator(value)&&!IsParentesis(value));
    }
    
    private boolean IsOperator(String value)
    {
        if(IsBinaryOperator(value)||IsUnaryOperator(value))
        {
            return true;
        }
        return false;
    }
    
    private boolean IsUnaryOperator(String value)
    {
        if(value.matches(UNARY_OPERATOR_TOKENS))
        {
            return true;
        }
        return false;
    }
    
    private boolean IsBinaryOperator(String value)
    {
        if(value.matches(OPERATOR_TOKENS)&& !IsParentesis(value))
        {
            return true;
        }
        return false;
    }
    
    private boolean IsParentesis(String value)
    {
        if(value.equals("(") || value.equals(")"))
        {
            return true;
        }
        return false;
    }
    
    private boolean IsOperatorNotParentesis(String value)
    {
        if(IsParentesis(value))
        {
            return false;
        }
        return IsOperator(value);
    }
    
    private Object ConvertOperand(String value)
    {
        if(value.matches(NUMBER_TOKENS))
        {
            return new Double(value);
        }
        else
        {
            return value;
        }
    }
    
/* ======================================================================
 * Cálculo para a expressão Pós-Fixa
 * ====================================================================== */
    public Double ResolveExpression(){
        Stack<Double> operandValueStack = new Stack<Double>();
        Iterator<Object> posFixedIterator;
        Object token;
        
        if(postFixedExpression != null && postFixedExpression.size() != 0)
        {
            posFixedIterator = postFixedExpression.iterator();
            
            while(posFixedIterator.hasNext())
            {
                token = posFixedIterator.next();
                if(token instanceof Double)
                {
                    operandValueStack.push((Double) token);
                }
                else if (IsOperand((String)token))
                {
                    operandValueStack.push(variablesMap.get((String)token));
                }
                else
                {
                    String opr = (String)token;
                    Double op1,op2;
                    
                    if(opr.equals("+"))
                    {
                        op2 = operandValueStack.pop();
                        op1 = operandValueStack.pop();
                        operandValueStack.push(op1+op2);
                    }
                    else if(opr.equals("-"))
                    {
                        op2 = operandValueStack.pop();
                        op1 = operandValueStack.pop();
                        operandValueStack.push(op1-op2);
                    }
                    else if(opr.equals("*"))
                    {
                        op2 = operandValueStack.pop();
                        op1 = operandValueStack.pop();
                        operandValueStack.push(op1*op2);
                    }
                    else if(opr.equals("/"))
                    {
                        op2 = operandValueStack.pop();
                        op1 = operandValueStack.pop();
                        operandValueStack.push(op1/op2);
                    }
                    else if(opr.equals("^"))
                    {
                        op2 = operandValueStack.pop();
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.pow(op1, op2));
                    }
                    else if (opr.equals("log"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.log10(op1));
                    }
                    else if(opr.equals("ln"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.log(op1));
                    }
                    else if(opr.equals("abs"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.abs(op1));
                    }
                    else if(opr.equals("r2"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.sqrt(op1));
                    }
                    else if(opr.equals("r3"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.cbrt(op1));
                    }
                    else if(opr.equals("sin"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.sin(op1));
                    }
                    else if(opr.equals("cos"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.cos(op1));
                    }
                    else if(opr.equals("tan"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.tan(op1));
                    }
                    else if(opr.equals("asin"))
                    { 
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.asin(op1));
                    }
                    else if(opr.equals("acos"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.acos(op1));
                    }
                    else if(opr.equals("atan"))
                    {
                    
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.atan(op1));
                    }
                    else if(opr.equals("rad"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.toRadians(op1));
                    }
                    else if(opr.equals("deg"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push(Math.toDegrees(op1));
                    }
                    else if(opr.equals("round"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push((double)Math.round(op1));
                    }
                    else if(opr.equals("ceil"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push((double)Math.ceil(op1));
                    }
                    else if(opr.equals("floor"))
                    {
                        op1 = operandValueStack.pop();
                        operandValueStack.push((double)Math.floor(op1));
                    }
                }
            }
            
            // O resultado será o único elemento restante na pilha
            if(operandValueStack.size() == 1)
            {
                return operandValueStack.pop();
            }
        }
        
        //ERRO
        return null;
    }
    
/* ======================================================================
 * Métodos Get para a expressão corrente
 * ====================================================================== */    
public String GetOriginalInFixExpression()
{
    return originalInFixExpression;
}
public String GetConsideredInFixExpression()
{
    return consideredInFixExpression;
}
public String GetPostFixExpression()
    {
        String exp = new String();
        if(postFixedExpression != null)
        {
            Iterator it = postFixedExpression.iterator();

            while(it.hasNext())
            {
                exp = exp + String.valueOf(it.next())+" ";
            }
        }
        return exp;
    }
}
