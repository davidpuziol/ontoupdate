/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.util.notifications;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import javax.swing.ImageIcon;

/**
 *
 * @author David Puziol Prata
 */
public class QuickMessageSet {
    ImageIcon icon;
    Color background_color;
    Color border_color;
    Color foreground_color;
    Font  text_font;
    
    protected QuickMessageSet(Color background_color, Color border_color, Color foreground_color, ImageIcon icon, Font text_font)
    {
        this.icon = icon;
        this.background_color = background_color==null?Color.lightGray:background_color;
        this.border_color = border_color==null?Color.black:border_color;
        this.foreground_color = foreground_color==null?Color.black:foreground_color;
        this.text_font = text_font==null?new Font("Arial", Font.PLAIN, 12):text_font;
    }
}
