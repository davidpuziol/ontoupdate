/*
 * DragAndDropUrlForm
 * 
 * Mini-Janela com Opções Drag And Drop para arraste fácil de URLs de um
 * Browser qualquer para seu interior.
 */

package ontoupdate.util.notifications;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import ontoupdate.resources.ResourceManager;
import ontoupdate.util.Logger;
/**
 *
 * @author David Puziol Prata
 */
public class QuickMessageDisplay extends JDialog
{
        static QuickMessageDisplay instance; //Instância Singleton
        
        //Strings Padrões de Tipo
        static public final String MSG_INFORMATION = "MSG_INFORMATION";
        static public final String MSG_SUCCESS = "MSG_SUCCESS";
        static public final String MSG_WARNING = "MSG_WARNING";
        static public final String MSG_ERROR = "MSG_ERROR";
        static public final String MSG_PROCESS = "MSG_PROCESS";
        static public final String MSG_QUESTION = "MSG_QUESTION";
        
        HashMap<String,QuickMessageSet> messageSets; //HashMap para guardar os messageSets criados pelo usuário
        
        JPanel panel; //panel inserido no JFrame
        JLabel msg_label; //label inserido no JPanel
        JProgressBar progressBar; //progress Bar inserido no JPanel
        JPanel buttonsArea; //Painel em que se disponibilizarão botões em FlowLayout
        Dimension preferedButtonDimension;
        Dimension tamanhoTela; //guardará as dimensões da tela (obtido do Toolkit)
        
        Integer progressBarMax; //guardará o máximo estipulado valor para a ProgressBar
        
        TimeoutThread timeout_thread; //Thread que contabiliza tempo de exibição das mensagens
        
        /**
         * Classe interna de Thread contabilizadora do tempo de exibição das mensagens
         */
        class TimeoutThread extends Thread {
             int period_in_milis;
             
             /**
              * Contrutor da thread contabilizadora do tempo de exibição das mensagens
              * @param period_in_milis tempo, em mili segundos, ao longo do qual
              * a mensagem deverá permanecer visível ao usuário
              */
             TimeoutThread(int period_in_milis) {
                 this.period_in_milis = period_in_milis;
             }

             /**
              * Método de execução da Thread
              */
            @Override
             public void run() {
                try {
                    //Dorme pelo tempo determinado (aproximadamente)
                    Thread.sleep(period_in_milis);
                    //Ao despertar, esconde a mensagem, e depois finaliza a thread
                    QuickMessageDisplay.instance.setVisible(false);
                } catch (InterruptedException ex) {
                    //Esta thread poderá ser interrompida normalmente, por motivo de
                    //chegada de outra mensagem, por exemplo
                    return;
                }
             }
         }

        
        /**
        * Obtém a Instância única do QuickMessageDisplay segundo Padrão Singleton
        * @return instância única do QuickMessageDisplay
        */
        public static QuickMessageDisplay Instance() {
            if (instance == null) {
                instance = new QuickMessageDisplay();
            }

            return instance;
        }
 
        /**
         * Construtor privado da classe Singleton QuickMessageDisplay
         */
	private QuickMessageDisplay() 
        {
            //Obter o container do Form e criar um novo Layout para ele
            Container c = getContentPane(); 
            c.setLayout(new BorderLayout());
        
            //Obter o tamanho da tela (Ex: 800x600, 1024x780, etc)
            tamanhoTela = Toolkit.getDefaultToolkit().getScreenSize();
        
            //Tornar o Fomulário UNDECORATED
            getRootPane().setWindowDecorationStyle(JRootPane.NONE);
            
            //Obter dimensões da tela e estabelecer limites mínimos ao formulário de exibição
            Dimension tamanhoMinimo = new Dimension(tamanhoTela.width - 100,0);
            c.setMinimumSize(tamanhoMinimo);
            this.setMinimumSize(tamanhoMinimo);
            this.setResizable(false);
            this.setUndecorated(true);
            
            //Instanciar JPanel e ajustar exibição genérica
            panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.setOpaque(false);
            
            
            //Instanciar JLabel e ajustar exibição genérica
            msg_label = new JLabel("", JLabel.CENTER);
            msg_label.setOpaque(false);
            msg_label.setBorder(new javax.swing.border.EmptyBorder(5,5,5,5));
            msg_label.setVerticalTextPosition(JLabel.BOTTOM);
            msg_label.setHorizontalTextPosition(JLabel.CENTER);
            msg_label.setIconTextGap(6);
            
            //Instancia JProgressBar e ajusta exibição genérica
            progressBar = new JProgressBar(0, 100);
            progressBar.setBorderPainted(false);
            progressBar.setValue(0);
            progressBar.setStringPainted(false);
        
            //Instancia o painel em que aparecerão os botões, se for o caso de um painel de confirmação
            buttonsArea = new JPanel(new FlowLayout());
            buttonsArea.setOpaque(false);   

            //Adicionar JLabel ao JPanel
            panel.add(BorderLayout.NORTH,msg_label);
            //Adicionar JProgressBar ao JPanel
            panel.add(BorderLayout.CENTER,progressBar);
            panel.add(BorderLayout.SOUTH,buttonsArea);
            //Adicionar JPanel ao JFrame
            c.add("Center",panel);
            //Empacotar o formulário
            this.pack();

            //sempre no topo e localizado no canto inferior direito da tela
            //this.setAlwaysOnTop(true);
            this.setLocation(tamanhoTela.width/2 - this.getSize().width/2, tamanhoTela.height/2 - this.getSize().height/2 );
            
            //Inicializar variáveis de HashMap e TimeoutThread
            messageSets = new HashMap<String, QuickMessageSet>();
            timeout_thread = new TimeoutThread(0);
            
            //Adicionar um evento ao panel para que, ao ser clicado, o dialog deixe de ser exibido,
            //ao não ser que a mensagem que está sendo exibida seja uma Mensagem de Progresso (com barra de progresso)
            panel.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelMouseClicked(evt);
            }
            });
	}
        
        private void PanelMouseClicked(java.awt.event.MouseEvent evt) {
            if(!progressBar.isVisible() && !buttonsArea.isVisible())
            {
                this.setVisible(false);
            }
        }
        
        
        /**
         * Procedimento setVisible original sobrescrito apenas para
         * estabelecer controle de Concorrência, pois este atributo 
         * será gerenciado pelo formulário e Threads de Timeout de Mensagens
         * @param b true para visível e false para invisível
         */
        @Override
        public synchronized void setVisible(boolean b)
        {
            super.setVisible(b);
        }
        
        /**
         * Cria um novo Esquema de Mensagem para ser utilizado ao longo do programa.
         * Os esquemas aqui criados serão utilizados no momento da chamada 'ShowMessage'.
         * @param name Nome para o novo esquema de Mensagem Rápida
         * @param background_color Cor de fundo para a Mensagem
         * @param border_color Cor da Borda da Caixa de Mensagem
         * @param foreground_color Cor do texto a ser exibido na Mensagem
         * @param icon Ícone a ser exibido na Mensagem
         * @param text_font Fonte a ser utilizada no texto da Mensagem
         * @return true caso o esquema tenha sido criado com sucesso
         * ou false caso tenha ocorrido algum erro (ex: nome já existente)
         */
        public boolean CreateMessageSet(String name,Color background_color, Color border_color, Color foreground_color, ImageIcon icon, Font text_font)
        {
            //Se o nome não é nulo, não está vazio e ainda não está cadastrado, criar novo Esquema de Mensagem
            if(name != null && !name.isEmpty() && !messageSets.containsKey(name))
            {
                messageSets.put(name, new QuickMessageSet(background_color, border_color, foreground_color, icon, text_font));
                return true;
            }
            return false;
        }
        
        /**
         * Exibe uma mensagem segundo o esquema previamente cadastrado através do método
         * 'CreateMessageSet' contendo o texto pasado por parâmetro, durante um período
         * de tempo estipulado em mili-segundos, ao não ser que outra mensagem venha a
         * sobrepor a atual.
         * @param messageSetName Nome do Esquema de Mensagem previamente cadastrado
         * @param text Texto da Mensagem
         * @param period_visible Tempo em mili-segundos para exibição
         */
        public void ShowMessage(String messageSetName,String text, int period_visible)
        {
            progressBar.setVisible(false);
            buttonsArea.setVisible(false);
            ShowGenericMessage(messageSetName,text);
            // Criar thread para timeout de mensagem
            timeout_thread = new TimeoutThread(period_visible);
            timeout_thread.start();
        }
        public void StartProgressMessage(String messageSetName,String text, int minValue, int maxValue, int currentValue)
        {
            progressBar.setMinimum(minValue);
            progressBar.setMaximum(maxValue);
            progressBar.setValue(currentValue);
            progressBar.setVisible(true);
            buttonsArea.setVisible(false);
            ShowGenericMessage(messageSetName,text);
        }
        public void UpdateProgressMessage(int currentValue)
        {
            progressBar.setValue(currentValue);
        }
        public void IncrementProgressMessage(int incrementValue)
        {
            progressBar.setValue(progressBar.getValue()+incrementValue);
        }
        public void UpdateProgressMessage(int currentValue, String text)
        {
            progressBar.setValue(currentValue);
            text = text.replaceAll("\n", "<br>");
            msg_label.setText("<html>"+text+"</html>");
        }
        public void IncrementProgressMessage(int incrementValue,String text)
        {
            progressBar.setValue(progressBar.getValue()+incrementValue);
            text = text.replaceAll("\n", "<br>");
            msg_label.setText("<html>"+text+"</html>");
        }
        public void FinishProgressMessage(int period_still_visible)
        {
            if(period_still_visible > 0)
            {
                // Criar thread para timeout de mensagem
                timeout_thread = new TimeoutThread(period_still_visible);
                timeout_thread.start();
            }
            else
            {
                //Opção para esconder a mensagem imediatamente
                QuickMessageDisplay.instance.setVisible(false);
            }
        }
        Component callerComponent;
        String callbackMethod;
        public void ShowButtonsMessage(Component callerComponent,String messageSetName,String text,String callbackMethod, String... button_names)
        {
            if (callerComponent != null && callerComponent != this)
            {
                callerComponent.setEnabled(false);
            }
            
            this.callerComponent = callerComponent;
            this.callbackMethod = callbackMethod;
            
            if(button_names == null || button_names.length ==0)
            {
                button_names = new String[]{"Ok"};
            }
            
            buttonsArea.removeAll();
            for(int i=0; i < button_names.length; i++)
            {
                JButton newButton = new JButton(button_names[i]);
                newButton.setFocusable(false);
                newButton.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        class MyClickThread extends Thread{
                            java.awt.event.MouseEvent evt;
                            public MyClickThread(java.awt.event.MouseEvent evt)
                            {
                                this.evt = evt;
                            }
                            @Override
                            public void run(){
                              CallBackIt(evt);
                          }
                          
                        }
                        MyClickThread myClickThread = new MyClickThread(evt);
                        myClickThread.start();       
                    }
                    });
                buttonsArea.add(newButton);
            }
            
            progressBar.setVisible(false);
            buttonsArea.setVisible(true);
            
            ShowGenericMessage(messageSetName,text);

        }
        public void CallBackIt(java.awt.event.MouseEvent evt)
        {
            int returnValue = 0;
            JButton buttonClicked = (JButton) evt.getSource();
            
            if( !(callerComponent == null) && !(callbackMethod == null) && !callbackMethod.trim().isEmpty())
            {
                Component[] buttons = buttonsArea.getComponents();
                for(int i=0; i< buttons.length; i++)
                {
                    if(((JButton)buttons[i]) == buttonClicked)
                    {
                        returnValue = i+1;
                    }
                }

                try {
                    Class[] params = {int.class};
                    Method method = callerComponent.getClass().getDeclaredMethod(callbackMethod, params);
                    method.invoke(callerComponent,returnValue);
                } catch (NoSuchMethodException ex) {
                    Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "O callback falhou, pois não foi encontrado o método "+callbackMethod+"(Integer i) para a classe "+callerComponent.getClass());
                } catch (SecurityException ex) {
                    Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "O callback falhou para o método "+callbackMethod+"(Integer i) para a classe "+callerComponent.getClass()+" por motivo de Segurança da origem!");
                } catch (IllegalAccessException ex){
                    Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "O callback falhou para o método "+callbackMethod+"(Integer i) para a classe "+callerComponent.getClass()+" por motivo de Acesso Ilegal!"); 
                } catch (IllegalArgumentException ex){
                    Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "O callback falhou para o método "+callbackMethod+"(Integer i) para a classe "+callerComponent.getClass()+" por motivo de Argumentos Incorretos!");
                } catch (InvocationTargetException ex){
                    Logger.Instance().PrintLine(Logger.LEVEL_IMPORTANT, Logger.ERROR, this, "O callback falhou para o método "+callbackMethod+"(Integer i) para a classe "+callerComponent.getClass()+" por motivo de Falha na Invocação!");
                }
            }
            
            this.setVisible(false);
            if(callerComponent != null)
            {
                callerComponent.setEnabled(true);
                callerComponent.setVisible(true);
            }
        }
        private void ShowGenericMessage(String messageSetName, String text)
        {
            QuickMessageSet ms = messageSets.get(messageSetName);
            
            if(ms != null)
            {
                //Se existe uma thread de timeout ativa, significa que há uma mensagem sendo exibida
                //Esta thread deverá ser morta e a mensagem será sobreposta
                if(timeout_thread.isAlive())
                {
                    try {
                        timeout_thread.interrupt();
                        timeout_thread.join();
                    } catch (InterruptedException ex) {
                        // Simplesmente ignorar erro (possibilidade remota e sem impactos)
                        ex.toString();
                    }
                }
                // esconder o formulário de mensagem
                this.setVisible(false);
                // aplicar alterações segundo MessageSet especificado por parâmetro
                this.getContentPane().setBackground(ms.background_color);
                panel.setBorder(new javax.swing.border.LineBorder(ms.border_color,2));
                msg_label.setForeground(ms.foreground_color);
                msg_label.setFont(ms.text_font);
                msg_label.setIcon(ms.icon);
                text = text.replaceAll("\n", "<br>");
                msg_label.setText("<html>"+text+"</html>");
                progressBar.setBackground(ms.background_color);
                progressBar.setForeground(ms.foreground_color);
                // Empacotar o formulário e exibí-lo no centro da tela
                this.pack();
                this.repaint();
                this.setLocation(tamanhoTela.width/2 - this.getSize().width/2, tamanhoTela.height/2 - this.getSize().height/2 );
                //Ajustar o tamanho dos botões para que todos fiquem iguais:
                if(buttonsArea.isVisible())
                {
                    preferedButtonDimension = new Dimension(80,0);
                    for(int i=0; i<buttonsArea.getComponentCount(); i++)
                    {
                        if(preferedButtonDimension.width < buttonsArea.getComponent(i).getSize().width)
                        {
                            preferedButtonDimension.width = buttonsArea.getComponent(i).getSize().width;
                        }
                        if(preferedButtonDimension.height < buttonsArea.getComponent(i).getSize().height)
                        {
                            preferedButtonDimension.height = buttonsArea.getComponent(i).getSize().height;
                        }
                    }
                    for(int i=0; i<buttonsArea.getComponentCount(); i++)
                    {
                        buttonsArea.getComponent(i).setPreferredSize(preferedButtonDimension);
                        buttonsArea.getComponent(i).setMinimumSize(preferedButtonDimension);
                        buttonsArea.getComponent(i).setSize(preferedButtonDimension);
                    }
                }
                // Fim do ajuste do tamanho dos botões
                this.setVisible(true);
                this.setAlwaysOnTop(true);
            }
        }
        
        /**
         * Método-Atalho para 
         * QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_INFORMATION, text, period_visible);
         * @param text
         * @param period_visible
         */
        public static void ShowInformationMsg(String text,int period_visible)
        {
            QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_INFORMATION, text, period_visible);
        }
        /**
         * Método-Atalho para 
         * QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_SUCCESS, text, period_visible);
         * @param text
         * @param period_visible
         */
        public static void ShowSuccessMsg(String text,int period_visible)
        {
            QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_SUCCESS, text, period_visible);
        }
        /**
         * Método-Atalho para 
         * QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_ERROR, text, period_visible);
         * @param text
         * @param period_visible
         */
        public static void ShowErrorMsg(String text,int period_visible)
        {
            QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_ERROR, text, period_visible);
        }
        /**
         * Método-Atalho para 
         * QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_WARNING, text, period_visible);
         * @param text
         * @param period_visible
         */
        public static void ShowWarningMsg(String text,int period_visible)
        {
            QuickMessageDisplay.Instance().ShowMessage(QuickMessageDisplay.MSG_WARNING, text, period_visible);
        }
        
        /**
         * Método Atalho para
         * QuickMessageDisplay.Instance().StartProcessProgressMessage(QuickMessageDisplay.MSG_PROCESS,text,minValue,maxValue,currentValue);
         * @param messageSetName
         * @param text
         * @param minValue
         * @param maxValue
         * @param currentValue
         */
        public void StartProcessProgressMessage(String text, int minValue, int maxValue, int currentValue)
        {
            QuickMessageDisplay.Instance().StartProgressMessage(QuickMessageDisplay.MSG_PROCESS,text,minValue,maxValue,currentValue);
        }
        
        public static void ShowOkDialog(String messageSetName, String text)
        {
            QuickMessageDisplay.Instance().ShowButtonsMessage(null, messageSetName, text, null, "Ok");
        }
        
        public static void ShowButtonCallBackDialog(Component callerComponent, String messageSetName, String text, String callBackMethodName, String... buttonNames )
        {
            QuickMessageDisplay.Instance().ShowButtonsMessage(callerComponent, messageSetName, text, callBackMethodName, buttonNames);
        }
        
        /**
         * Método principal para teste unitário
         * @param args vazio
         */
      
        
        private void ContinueTests(int buttonIndex)
        {
            try{
                if(buttonIndex == 1)
                {
                    QuickMessageDisplay.Instance().ShowMessage("information", "Esta é uma mensagem de Informação.", 4000);
                    Thread.sleep(2000);
                    QuickMessageDisplay.Instance().ShowMessage("success", "Esta é uma mensagem de Sucesso.", 4000);
                    Thread.sleep(2000);
                    QuickMessageDisplay.Instance().ShowMessage("error", "Esta é uma mensagem de Erro.", 4000);
                    Thread.sleep(2000);
                    QuickMessageDisplay.Instance().ShowMessage("warning", "Esta é uma mensagem de Aviso.", 2000);
                    Thread.sleep(2000);
                    QuickMessageDisplay.Instance().StartProgressMessage("process", "Iniciando Mensagem de Progresso",0,100,0);
                    Thread.sleep(1000);
                    QuickMessageDisplay.Instance().IncrementProgressMessage(25, "Primeira Atualização de Progresso");
                    Thread.sleep(1000);
                    QuickMessageDisplay.Instance().IncrementProgressMessage(25, "Segunda Atualização de Progresso");
                    Thread.sleep(1000);
                    QuickMessageDisplay.Instance().IncrementProgressMessage(25, "Terceira Atualização de Progresso");
                    Thread.sleep(1000);            
                    QuickMessageDisplay.Instance().UpdateProgressMessage(100, "Finalizando Mensagem de Progresso");
                    QuickMessageDisplay.Instance().FinishProgressMessage(2000);
                    Thread.sleep(3000);
                    QuickMessageDisplay.Instance().ShowMessage("success", "Teste Finalizado!", 4000);
                    Thread.sleep(2000);
                }
                else
                {
                    QuickMessageDisplay.Instance().ShowMessage("error", "Teste Cancelado!", 4000);
                    Thread.sleep(2000);
                }
                System.exit(0);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

}
