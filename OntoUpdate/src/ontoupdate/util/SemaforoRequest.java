package ontoupdate.util;
/**
 *
 * @author David Puziol Prata
 */

public class SemaforoRequest {

    int count = 1;
    private static Semaforo instance;

    public static Semaforo Instance() {
        if (instance == null) {
            instance = new Semaforo();
        }
        return instance;
    }

    public synchronized void P() {
        while (count <= 0) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        count--;
    }

    public void V() {
        count++;
        try{
            notify();
        }catch(Exception e){

        }
    }
}
