/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

/**
 *
 * @author Alessandro Germer
 */
public class DisplayUtils {

    public static Point GetPointToCenterForm (Dimension form_size)
    {
        // Obter o tamanho da tela (Ex: 800x600, 1024x780, etc)
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension screenCenter = new Dimension(screenSize.width/2,screenSize.height/2);
        Dimension formHalfLength = new Dimension(form_size.width/2,form_size.height/2);
        Point pointToCenterForm = new Point(screenCenter.width - formHalfLength.width, screenCenter.height - formHalfLength.height);
        
        return pointToCenterForm;
    }
    
    public static void CenterGuiComponent(java.awt.Component form)
    {
        form.setLocation(GetPointToCenterForm(form.getSize()));
    }
}
