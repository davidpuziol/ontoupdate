package ontoupdate.util;

/*
package ontoupdate.util;

/**
 *
 * @author David Puziol Prata
 */
public class LoadOntologyException extends Exception{
    
    public LoadOntologyException( String message )
    {
        super(message);
    }
}