/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ontoupdate.util;

/**
 *
 * @author David Puziol Prata
 */
public class MathExpressionException extends Exception{
    
    String errDetail;
    String errTrace;
    String errTitle;
    
    public MathExpressionException( String errTitle, String errDetail, String errTrace )
    {
        super(errTitle+" - "+errDetail+": "+errTrace);
        this.errDetail = errDetail;
        this.errTrace = errTrace;
        this.errTitle = errTitle;
    }

    public String getErrDetail() {
        return errDetail;
    }
    
    public String getErrTrace() {
        return errTrace;
    }
    
    public String getErrTitle() {
        return errTitle;
    }
}

