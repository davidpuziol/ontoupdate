# OntoUpdate Using Clustering

![logopuc](./logo-puc.png)

Esse projeto, desenvolvido durante o trabalho de conclusão do curso, consiste em carregar uma ontologia pré desenvolvida (sobre ataque de redes) para filtrar os páginas as páginas navegadas e arquivos de texto e conferir se tem ou não relevância com a ontologia carregada. Durante a filtragem a ontologia poderá ser atualizada com a intervenção do usuário, mas o programa sugere novos conceitos. O OntoUpdate funcionará como um proxy para o navegador intermediando todos as requisições.

A arquitetura do projeto é a seguinte
![arquitetura](./pics/DiagramaDeArquitetura.png)

A [monografia](./Monografia/) está na pasta incluída neste repositório.

O diretório [OntoUpdate](./OntoUpdate/) contém todo o código do sistema e pode ser aberto no NetBeans JAVA IDE <http://www.netbeans.org/>. A versão utilizada durante o desenvolvimento do projeto foi a 7.0.

Caso seja necessário corrigir o caminho para as bibliotecas (pacotes JAR) no aplicativo,
todas estãoo disponníveis no diretório .\OntoUpdate\libs\

## Manual

Dentro da pasta [pics](./pics/) possue os prints de todos os passos para das configurações necessárias.

## Propostas de melhoria

Dentro da pasta [Propostas Futuras](./Propostas%20Futuras/) possue tudo que achei que poderia ser melhorado na ferramenta durante o projeto de tcc, mas não foi possível implementar devido a complexidade que já existe no projeto e o tempo não estava a favor.
