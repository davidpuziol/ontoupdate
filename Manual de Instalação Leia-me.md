# Executar o aplicativo

É necessário ter o java 7 para rodar o jar do aplicativo. Faça a devida instalação de acordo com o seu sistema operacional

Para executar o OntoUpdate clique duas vezes em OntoUpdate.jar.

Se o computador não reconhecer o arquivo como sendo executável java, clique com o botão direito, vá em "abrir com" e procure dentro de C:\Program Files\Java\jre7\bin e utilize o executável "javaw".

## Instalação do Servidor Apache Contendo alguns textos utilizados nos testes

- Defina duas v�riaveis de ambiente a primeiro sendo JAVA_HOME e a segunda sendo JRE_HOME com o caminho da pasta "bin" do java.

JAVA_HOME = C:\Program Files\Java\jre7\bin
JRE_HOME = C:\Program Files\Java\jre7\bin

Para definir essas variáveis é necessário ir em painel de controle > sistema e segurança > sistema > configurações avançadas do sistema > variáveis de ambiente > variáveis do sistema e criar essas duas váriaveis sendo o valor igual ao path.

- Abra o prompt de comando navegue até pasta "servidor" dentro TCC David Puziol Prata\Instalação OntoUpdate\Servidor Apache\apache-tomcat-7.0.29\bin e digite catalina.bat

- Logo ap�s digitar o comando acima, digite startup.bat

- Para manter o servidor rodando é necessário manter a janela do prompt de comando minimizada. se fechar o servidor para de executar.

## Como navegar nos textos

- Os textos estão na página <http://localhost:8080/textos/> , mas para acessar cada textos é necessário especificar o nome do texto <http://localhost:8080/textos/t1.txt>

- Os textos são chamadados de t1, t2, t3, t4 e assim por diante até o t15 e as extesões são pdf, txt, rtf e doc.

## Configurar navegador

É necessário criar um proxy da conexão do navegador para o programa OntoUpte, assim o OntoUpdate processará os textos. Se não for definido, o navegador acessa os textos mas não processa.
Dentro do programa é escolhida uma porta e designado um ip na tela de navegação.Utilize o IP dado e a porta escolhida.

Para o Internet Explorer e o Google Chrome é necessário configurar o proxy dentro de opções de internet no painel de controle. Para o Firefox basta ir em opções > redes > proxy e definir o IP e porta do proxy.
