�            Processing Took 00:00:00.00
001: Slash (/) must be followed by a parameter.$002: Illegal parameter: $003: Only one /X switch is allowed.$004: /P and /C are incompatible.$005: Record len must be between 1 and 32,750 in: $006: Pascal string key only allowed in fixed len record: $007: /P only allowed for fixed length records.$008: Binary number key (F,I,M,T or U) only allowed in fixed len record: $009: /X switch not allowed for fixed length records.$010: One and only one temp drive letter may be entered: $011: Non-existent drive: $012: Invalid character for the drive: $013: Start column must be between 1 and 32,750: $014: Start column must not exceed record len: $015: Only one start column allowed: $016: Error in sort key: $017: Key len must be between 1 and 32,750: $018: Key len is too big to fit in record: $019: Only one key length allowed: $020: Length for 80x87 floating point number must be 4, 8 or 10: $021: Length for GWBASIC/BASICA floating point number must be 4 or 8: $022: Length for Turbo Pascal floating point number must be 6: $023: Length for Pascal strings must be between 2 and 256: $024: Length for Pascal strings must be between 2 and 256.$025: "P" and "C" attributes are incompatible: $026: C attribute conflicts with /P: $027: Sort key cannot be both a binary number and a string: $028: Only one binary key type allowed in a sort key: $029: Only one list of input files and a single output file may be given.
            Found additional file spec: $030: Multiple files not allowed in output spec: $031: Misplaced plus sign in input file list: $032: Input is redirected to the standard input, so output must go to
  the standard output.  The following file spec is illegal: $033: You must specify an input file.$034: No name specified for error file: $037: RPSORT requires MS DOS version 2.00 or later.$040: Not enough memory.  RPSORT requires 30,000 bytes.$041: Not enough memory to hold at least two records/lines at a time.$043: Line exceeds max length of 32750 bytes.$044: Found Pascal string whose length byte exceeds specified key length.$046: No data in input file(s) so nothing to do.$047: Input file not found: 048: Error reading input file.$049: No room on disk to write sorted output file.$050: No room on disk to write temp file.$051: Unable to create temp file.$052: Unable to create output file.$053: Unable to create error file: $
 ERROR 054: Ran out of space on disk attempting to write error file.
            Redirecting error messages to the screen.
059: Error allocating memory.  $060: Unknown error accessing disk.$
Sort successfully completed.

Enter the RPSORT command with no parameters to get a description of the syntax.

Found short record at end of input file.  Will delete it from output file.

Added carriage return and line feed which was missing for last line in file.

Do you wish to quit RPSORT?  Press "Esc" to quit, any other key to continue.
Continuing...
� 	
 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~CUEAAAACEEEIIIAAEAAOOOUUYOU$$$$$AIOUNN��?����!""�������������������������������������������������S���������������������������������������@����� ������ ,�!�.��6	�
���� 0�!<s��3<r���|' 
�~' .� �I�!� �!A��'�_F���� ;�w��� D�  �!�����G�>'��iD�>
'���>'iDu�>�' u���Y�8�>�' t�;�S �3�
3�>'�� ������n�l�>�' u�E!���'
�'t�U!s�>�' u�-	��2��'��$��3�>�' t�>�' t�H'�
�> 'w�&�C(�>�' t��'��v �>�' t�뫀>�' t�+�> u	��  ��23�P�>&'t	�&'� >�!�3��'�!� X�L�!� ,�!�.��6��´d�(*
�7 ��<�%	�( ��<�"� ����
 �� �2Ü:�w2�*�*��2�
���	 ���� ���0<9~þ� 3Ɋ� �6'�'���!I�< t�</u�I��<Qu���6'�'úz�Q �2ø 3�!��'�3��!�#%��!�>�' t�=��'�!�ع � F�!�PSQRVWU�>�' u)��tN�>�' tG�,�P �1��!<t5�|� �1�>�' u"�B3ɋѻ �!r�ʋЃ��� r� B� �!����0�]_^ZY[Xː
RPSORT v1.02  Dec. 15, 1992, Copyright 1991 by Bob Pirko, All rights reserved

Usage: RPSORT [/Q] [/Eerrfile] [/]? [inputfile[+inputfile]] [outputfile] [/A]
          [/B] [/C] [/D] [/Fnnnn] [/N] [/P] [/R] [/Td] [/Z] [sort key defin...]

Sort key defin syntax:  /+ [col] [:len] [A] [C] [F] [I] [M] [P] [R] [T] [U]
-------------------------------------------------------------------------------
   (Press PageUp or PageDown to see other syntax screens or Esc to exit)
RPSORT greatly improves upon the features and the performance of the sort
utility distributed with Microsoft DOS.  First, RPSORT does everything that
the DOS SORT does.  Virtually any command that works with the DOS SORT will
work with RPSORT and produce the same result.

But RPSORT does much more.  It can sort very large files and supports multiple
sort keys.  It is extremely fast.  I know of no sort utility that outspeeds it.

RPSORT sorts text files.  These consist of lines each ended by CRLF (i.e. a
carriage return and a line feed).  It also sorts files of fixed length records
such as those produced by many BASIC, Pascal and C language programs.

RPSORT supports numerous sort key types including regular text keys, C language
strings, Turbo Pascal strings, signed and unsigned binary integers of any
length and several types of binary floating point numbers.

RPSORT can delete null lines (consisting only of CRLF).  It can also delete
records/lines whose sort keys duplicate those in a previous record/line.

   (Press 
Parameters may be entered in any order except as noted below.

RPSORT may be run as a filter using redirection.

  For example:    RPSORT   <ipfile   >opfile
           or:    DIR | RPSORT | MORE

Input and output may be specified directly.  Input is one or more filespecs
separated by plus signs.  Output must be a single file.  Input filespec(s) must
precede output filespec.  Filespecs may include a path.  Wildcard characters
are allowed.  Input files are sorted together into the single output file.

  For example:    RPSORT ipfile*.txt+c:\mydir\ip??file.txt   opfile

By default, RPSORT assumes the input is a text file and that the entire line is
the sort key.  The sort is case insensitive (lower equals upper case) and just
like the DOS SORT it equates foreign letters, punctuation, and currency symbols
to their English equivalents.  The following screens describe other options.

/Q if it is the first parameter suppresses copyright and success messages.
/Eerrfile directs error messages to a file.  Should precede all but /Q.
/? or ? produces these syntax screens.  RPSORT with no parameters also does.
/A does an ASCII sort.  This is case sensitive (lower not equal upper case).
/B tells RPSORT to ignore any control break entered from the keyboard.
/C says that text keys are terminated by a binary zero (C language strings).
/D deletes any record whose sortkeys duplicate those in a previous record.
/Fnnnn says that the input consists of fixed length records of nnnn bytes.
/N deletes any null lines (those consisting only of a CRLF sequence).
/P uses the first byte of text keys as the key length (Turbo Pascal strings).
/R specifies a reverse (descending order) sort.
/Td designates drive to be used for temp files instead of default drive.
/Z says to ignore any Ctrl-Z in a text file and use the entire file.  Normally
   RPSORT (just like MS-DOS) treats Ctrl-Z as the end of a text file.

/R applies to all sort key definitions while /A, /C and /P apply to all text
sort keys.  Sort key definitions can't over-ride them.  To sort some keys one
way and some another way, use the sort key attributes on the next screen.

A sort key definition has the above form with no spaces between the attributes.

  col  is starting column of this key.  Col 1 is the first col in the record.
  len  is the length of this key.
  A    does an ASCII (case sensitive) sort for the key.
  C    treats a binary zero as the end of the key (i.e. a C type string).
       Len should equal max C string length (e.g. if "char mystr[8]", len = 8).
  F    sorts this key as 80x87 binary floating point number. Len is 4, 8 or 10.
  I    sorts this key as a signed binary integer.  This may be any length.
  M    sorts this key as a binary floating point number defined by BASICA,
       GWBASIC or older versions of Microsoft QuickBASIC.  Len is 4 or 8.
  P    uses the first byte of this key as the key length (a Pascal string).
       Len must equal max Pascal string length + 1 (e.g. if string[8] len = 9).
  R    does a reverse (descending) sort for this key.
  T    sorts this key as a Turbo Pascal number of type "real".  Len must be 6.
  U    sorts this key as an unsigned binary integer.  This may be any length.

Attributes F, I, M, P, T and U are only allowed for fixed length records.

1. RPSORT as a filter.  Input is a text file.  Sort key is the entire line.

      RPSORT   <DATAINP.DAT   >SORT.DAT

2. Combine two files.  Do an ASCII (case sensitive) sort.  Write the output to
   SORT.DAT.  Put temp files on the C drive.  Key is 5 bytes at column 4.

      RPSORT   DATA1.DAT+C:\MYDIR\DATA2.DAT  SORT.DAT  /TC /A /+4:5

3. Sort a file of 90 byte records.  The first key is a Turbo Pascal real number
   at column 15.  The second key is a Turbo Pascal string of type string[11].

      RPSORT  PASFILE.DAT  SORTFILE.DAT  /F90 /+15:6T /+1:12P

4. Sort a file of 70 byte records.  Both keys are C type strings.  The first
   key is ASCII (case sensitive) the second is case insensitive by default.

      RPSORT  CFILE.DAT  SORT.DAT  /F70 /C  /+5:10A /+23:7

   (Press PageUp�@U 5 �@s�J �@�J �@��J �@�#.), �&�&�&�&�&�&          iD        �                              

���    �#        ������  ��                                    * ERROR  "
                   �           ��                   NUL �>�'r=�>�' u� D�  �!
�y����\����&x'� '�6�&IF�"�I< t</u�NA���&x'��'�6'�'��㦉6�&� '�I< t�<?u�{</t�>�'t���'�� ���	I�<?u�Y�,���'�< t�</uAN��<+t
<0r<:wAN�>�'t�u��;딀>�'t�f�׋���x= w	'�R�'����6���<Fu7�>' t�F4�r��I��a���w��tщ'�g �'BB�r'�'  �.�<Zu��'��H ��<Du��'���<Bu��'���<Nu��'���<Eu�, ����� u�Ⱥ\�����&w�u����</t�< t��À>�' t�E7����'��9�6^'I�< t/</t+��I< t</u�NA� �PVQ� <�^'3��!r�&'Y^X�ú��D�Y^X��6
�9���&  ��&��&�X ��!
�t<t5� �!�� �!<It<Qu�>�&��t���&��>�&�� t���&� �2����>�&����&�u�=�4��M����;�r��<TuK�>�' t�T6���� ��'��A�!@��2���I�< t</t<Ar<Zw:�w �Ƣ�'����xu�AN3�ú&�l����f���`�<Ar0<Zw,S�K�[<�t"� <�r,���= t= w	��'�u
�= ��������'�����' ��' ��' ��' ��' �I��< u�`</t�����y�
= tR= v� �>�' t��'u��'���= u8��'��>�' u#�' u�� ��5��C��P���� ���� = w=t� ��'��>�' u��' t��6��P��C���J�>' uf���=�^�>�' t��'uP��'���"�C�>' u��'u��'�U��>�' ��'�t�'�����u��'u��'�0��	'�����<:t#<0rU<9wQ����w?�t;�>' uP�'���PI������wE�tA�>' uC�'��P�X�< tANúM�z�}���'�t�s����g�j����^�a����U�X��B�L�O��cQV���'�'�'�t+'H�'�'�'  �'  �'  �>'�'� t�>�'u�>' u�&�X� t���M�'�>�' t�?�h'  ��iDt�>p'�6
'��iD� �>h'�6
'��qD� �>f'�>';>'r� ^Yú8����t�NA�>�' u9��'���'��6Z'�^�I<+tX��'��I< t
</t<+u���NA�6\'��'��'À>�' u"��'��6`'��I< t</t<+u��NA�6b'úf�'����!��
����
��܃����J�!r�H����!= uߴH�!rف� s�;�[��������  �|�z  ؉�K������   �   �'�K��s�8'��:'�������3�����+ډ8'�����:'�&r' �� ���ر�� �@'��� ��.�4'�  ����= Hs/3�P�	 ��F'��= s� �H'X= $s3ҹ ��B'3���D'+D'��KK���s���';'r8��+|'r.� ��>' t�؋�3ҋr'����'�����ӣ����.�>'�r���#�r��>' t������  �<'  Ë><'�����|+ñ����.��z+�.��؋�$�+�.����.|.����3�.�|���s��.�<'  .�4'&�  À>�' u��u
��  �'ú���>�' ub�>�' t� O�!r�� �E��' �>�' u����'�>Z'�׋\'+���+�tG�E� �>Z'�R s��'�� N� �!r� � =�!r��� r
��� >�!��R��}�]	��Z��+�I��w.��'/����+�I�u���<?t<*t<\t<:t�����It�<\t<:t���t�������W�t�>_����g�>� �t��< �����'��> 'w� '  �.��>���'��' �Ë�.�>�' t.��' .�.�.�>�� �B3ɋ��!PR� B3ɋ��!]_�>�'uC�>�' u<�~'9�r3�ǋ�+��s%�|')�)�))���   �   �'� ��3������+��� �tUW���N �P_]���u�.��.����.��+�.+z'.;@'v.�8'�>.;B'r2.+D'.�>' u����� K�ù R��.�6r'.�&'Z��.;'s���+�+��� s�3���.��' �.��'�.��� ?�!rZ;�uV��.�>' uO�W���+����_u<K.��'�.�>�' t�+�� ��+�u� .�.��' ����������.z'�S� .�.��' .�>' u����������.z'�R��3�.�6'������.z'Z���tUW.��' �q _]���.��'.�>' u@ڋG�.;4't/��.;4't&C.:5't.:4'tC.�4'�G�S���P ��[.��� �úx	�$WQ�|���s��Y_��.�6<'��.����$�+�.������.�.��.�>�' t.��' .��' �.�>�' t.��' Ë�.+�|� ��Ń� .+'�� t.�>�' u��v(.��'�� .;�zw�.;8's.�>�' u.�>�' u�.�8'P.��G��+߁��v��+ˎ�.���������KK.���.ļzY.'+ϋ�.�>' t�� ����������.)z's.�z'  ��IGIG���w.�4'I��Zu&8%t��@&8e�uOA�&8%u�AO+�u.�>�' u�&�/��.;.'v.;.'w.�.'��J�NN�u&8e�uOA���z�+���.;.'w�.��'���OO�ƌˎێË6<'��;��tD��z@@��:$�)�:)������������FF�6<'�ދ�$�+���z����x��|�(�Ë�3�.�6'��������.)z's.�z'  �ǌێË�.�.'��������r��
 ������ 3�����0�F���.�>�' u.��'.��'.��'�:��+�:��  �% � '�('�-� � �$'�*'�<� � �,'��V� ,�!������������.B��PB�2��ZJJ3ɸ <�!rú�	�i.�>b'.� .�`'3ɸ <�!r.��'�.� '.�"'ú
�;�.�<'��NN.�6>'.�>�' t�� .�R'����3��Z�@@��.��@.��.��  .��+Ё� s	���.�R'3�V^.;6>'t}FFV��.�>�' t.��ZV��.Ŵz�.��ZS��.ŷ��4.��|MMx@��.�>�' t.��ZW��.Ľz�.��ZS��.Ŀ�&�=.��|V�s	^_.��\뿃�^.��\�{��.�6p'.�D��.�D���.�T'���qAA������.�.ZU��.Ŷ��4.��|.��Z��.ľ�&�=.��|Q�������Ys�������:�w���������:�w�����Z�u�Ȏ����]�-.�Z��.ŷ��4.��|�%r�Ȏ؎��Z������II;�:r�����.>'x�>'��A�Z�u�밋>'��6�� .� '.�>�' t&.�>�' t�Ȏغ�'� �@�!� '��tF� >�!�?�B3ɋ��!rv.�6N'.���T.�J'��s.�.�L'��.�6N'.�.���Ȏ؎��6p'.�D9�.�D��.��.���3�.�X'��.� '�@�!r;�u.��  ø61��62��
.��
���t.�>�' u��	���	�.�>�' t`.�>T'.�6T'.�V'���u@.�>X'���tB.��.�>�' t).�>' u!.��+�IIOO&�QW���U���_Y&�u�Ë��D���u��.�'�u�@@.�R'.�>�+�+ȑs�tE��.�T'����.�>X'.�.�>�' u.+6'�.�>' t	��s������Is��.�4'���.�>�' u.+6'������Ë�.�>' t������.�4'�D�P��X�D����V�.�>�' te� AA������.�6ZV��.Ŵz.��Z��.ĽzQ����j
���Ys�������:�w���������:�wɋ���Z�u�Ȏ����]�-.�6Z��.Ŵz�~�r�Ȏ؎��Z����z�'��z;�zs/�#��|��.��z.;�zs��r.;�zw�Ɏ�.�>'��<��.�6p'.�D��.�D�.��'�.�.��.� .� .��H.�.��.+�.+F'.�'�����.�H';�s��3���= s�t�����;�v� ��%�.��.�.J';�v���@v�@ .�.0'.� '.�$'.� '.�>�' u�G.��' .��' .��' .�J'3�.�60'.�.��u� = u�B.�2'.�J'+�.�.� '.�.� '.�>�' u��.��' �"�Y��.��'��B.� '3ɋ��!s�73���.�.�.� '.�.� '.�6N��.�$'�.�  .�0'.�2'��.�>uC.��'�.� '�.;('t�+.�*'  �.�('  �Ȏ��q.�>�' t���.� ' .�> t.��
.�0'.�.�IQ�u".�.;2't	.�2'Q�CY.�>�' t.��'�.�H+�.�&0'.�.2'H.�
�Q��.�>�' u=�B.� '3ɋ��!s�74���.�6.���T��s.�.���.�6YIx�m�.�.�N'.�.�L'.�.�.�.�J'= t���Ȏ؀>�' t��'� � '�@�!� '��t� >�!�>�' t1�$'�;('t�+�*'  ��('  �4��  �:�$�3ɋѸ B�!r�@3��!r�uø63����64����65���.�.�L'����+ދ�O�.��.+�.+F'3�.�62'.�:';�r.�8'����.�'�3�����.���.�2'��.�<'.��.��.�2'H.�>�' tH.�3�.��' Q.�.�L'������.�����+�+ߋ�W�w�o�_������.���.ǅ�  ��A.;v�.�>�' tK��W��.�>.'��.�.�3���.�$'.�.�$'�o.�$'.�.�$'_����.���.ǅ�  .�T'�����.��'�.�Z.��z��.��z+�t3�.��|�؎���s��.��z�.Ǉz  .���.���.+��.��r���uG.��z u?.�>�' t��.;.'u.��' .�.<'t�Ȏ؎��Z�<'��Z�u��b�� .�z�� �u.;�v.����.��zP.�$'.� .�>�' t.;.'u.�.� � B��.���.���.��.��S.� �!r4[Y.��z.Ǉz  +�.��.��� .��|.� � ?�!r;�u���ø66���67���68��+��.���.���.���.���u.;�r.����.��z.����.��|.ǅz  �������.>�RP� B�֋�.�$'�!rY.�$'� ?3��!r
;�t�69�	��70����R�>�' u��F�%���
��'�"�Z��'���'���� �$�O��+��	�QVSP��Q��&�� '�I��</t< t���N��+�X =P v-P �=O rQR��'� �� ZYQR��'� � ZY� ��'� � �	QVSP�T�� .��' X[^Y����� .��'���('�('  ��8 �*'�*'  �+�( �,'�,'  �:� �>�' t�"'��v�`'� ��t� >�!r� A�!rø71����72���.��'���'� ��6'� � �&'�@�!r;�t6�>&'t/� >�!�&' � RQ�Y
� �@�!.�>�' t	��.��' YZ���3ҿ
 <0r<9w% P���Zp�r	�I���NAú����I�<0r�<9w���������Ӌ�+��k�Ȏ؎����&d'�<'�>'.�.>'x�0�!.�6>'��.��|.��.���.��.��:.�����+��"^[��MMCC��+ʃ��t���+�r;�s�ڇ��SV��u.;&d'r�]Z��+���RU�����z��+�����z����+����&�v &�5&�v ��&�=V.���?.��^wBB;�u�&�v �|���&�=&�~ �����&�=V.���.��^rJJ;�u�&�v �L���&�=&�~ ���                      �_ .�|C.�~CV���M ^.��C.9|Cu�u�
 �VW.�~C��C�\ ^V.��C��C�M �Ȏ��ؾ�C��C� �_^�
 �V��� 3ۭ�xu�����C��s�^��@u3�ËD�%�u@+�@ ÌȎ��ñ�����+����˳*�Rt�����t�����ƪ������u���ƪ��2��ZÁ�������3��ج&�GI��;�s��:�v�bB��+ٞ��&�GG��;�s�ٟ���w��+�+��	��w�Ð;�s��+���@3��R�
�t�&�G:���u

�t3���@Z�&�%GI:�u�
�u�PS����.׆�&�G.�:���[XRS���
�t�.׆�&�G.�:���u

�t3���@[ZS���.׆�&�GI.�:�u�
�u�[��&�%OI��Ā:�u����������Ɋ&�%��r#��r5�D��t&�E��t��$��C�����C�����s�D��t�&�E��t��t�����Ɋ&�%��r��r��	��s�t������D�&�]�
�t��r��r��	��s�t������D�&�e�
�t0�&�%��r��r#�D�&�e�:�u����s�D�&�e�:�u�t����t� Ð�t� Ð˴@FG;���;��QV�>' t�>�'u�>�' t��4��N�����>' t�'�'�u��+'��>' u�'� t�' ��'�w�~���+'s�>' u'������'�t^�''H�'�&'��'�t�'� t�>'ty������q�e���i�����a�>'tZ�>'tS�'@ u�>'
u��B�' t:�> t�>' t,�>'r�>' v�f�����>'r�>' v�+�T��'�>' uBB�'�>�' t���>'�6
'�>' t �>j'�l'  ��tD�' u� ��% ��ƙD�= �j'  �>l'��'�u�' t�' ug��D� �t�z�' t�`E� �E�'  t�uE� �5�' t�>'
t��E�  ��{E�H ��'@ t��E�$ ��F�? 6
'��_�>' t��D� ���D� 6
'��B�' u�>' t�' t�E� ��E� ��>' t�EE� ��E�( 6
'�6
'��FF� �>' t�' t�6
'��LF�	 �>n'�>';>'r���>h' t
�'�>h'�E�>f'�'�E�>j' t%�>j'�'�E�' tO�' u9�>n'�E����<�>l'�'�E�E(�E�'�E�n'+�-& �E%�' t�E-�Ë>n'�E����E��Ë>'�>p'�UF� 6
'�>';>'r���^Y�