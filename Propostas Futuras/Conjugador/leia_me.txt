FORMA vers�o 1.1
Ferramenta de etiquetagem e lematiza��o
=======================================

A) Procedimentos para preparacao de pastas e arquivos

1) Em uma pasta (ser� chamada aqui de "pai") copiar o arquivo forma_1_1.bat (ou forma_1_1_dos.bat)

2) Criar uma pasta abaixo da pasta "pai" com o nome de "forma_1_1"

3) Na pasta "forma_1_1" copiar os arquivos
"acentos.let", "forma_1_1.c", "locucoes.let", "sufixato.let" e "sufixos.let"

4) Compilar (linguagem C) o programa-fonte "forma_1_1.c" gerando o executavel "forma_1_1.exe" na pasta "forma_1_1"

B) Procedimento para execu��o

B.1) No LINUX
Em linha de comando, na pasta "pai" executar
./forma_1_1.bat ARQ1 ARQ2
sendo
ARQ1 = o arquivo que cont�m o texto a ser etiquetado e lematizado
ARQ2 = o arquivo que conter� a saida da etiquetagem e da lematiza��o

B.2) No DOS
Em linha de comando, na pasta "pai" executar
forma_1_1_dos.bat ARQ1 ARQ2
sendo
ARQ1 = o arquivo que cont�m o texto a ser etiquetado e lematizado
ARQ2 = o arquivo que conter� a saida da etiquetagem e da lematiza��o

Observa��o: O arquivo texto.txt � um exemplo de ARQ1 e o arquivo saida.txt � um exemplo de ARQ2.

Refer�ncia (favor citar):
Gonzalez, M.; Lima, V. L. S. de; Lima, J. V. de. Tools for Nominalization: an Alternative for Lexical Normalization. In: WORKSHOP ON COMP. PROC. OF THE PORTUGUESE LANG. - WRITTEN AND SPOKEN, 7; PROPOR, 2006. Proceedings� [S.l.]: Springer-Verlag, 2006. p.100-109. (Lectures Notes in AI, 3960).